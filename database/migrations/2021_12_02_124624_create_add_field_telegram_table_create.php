<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddFieldTelegramTableCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->unsignedBigInteger('telegram_access_id')->index('telegram_access_id')->after('support_project_id')->nullable();
            $table->foreign('telegram_access_id')->references('id')->on('telegram_accesses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropForeign('order_items_telegram_access_id_foreign');
            $table->dropColumn('telegram_access_id');
        });
    }
}
