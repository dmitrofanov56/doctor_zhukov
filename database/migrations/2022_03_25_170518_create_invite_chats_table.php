<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInviteChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invite_chats', function (Blueprint $table) {
            $table->id();
            $table->string('telegram_id');
            $table->string('telegram_from_id');
            $table->string('telegram_from_username');
            $table->string('telegram_join_id');
            $table->string('telegram_join_username');
            $table->float('scope')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invite_chats');
    }
}
