<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('chat_joins', function (Blueprint $table) {
            $table->id();
            $table->string('username');
            $table->string('chat_id');
            $table->string('user_chat_id');
            $table->string('status')->default('not_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('chat_joins');
    }
};
