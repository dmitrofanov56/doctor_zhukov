<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class SeederCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([
            ['title' => 'Блог', 'slug' => 'blog'],
            ['title' => 'Видео', 'slug' => 'video'],
            ['title' => 'Авторские посты', 'slug' => 'author-post'],
        ]);
    }
}
