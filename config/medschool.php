<?php

return [
    'telegram_channels' => [
        'Клуб' => [
            'chat_id' => '-1001539682289'
        ],
        'Канал медскула' => [
            'chat_id' => '-1001671604165'
        ],
        'Тестовый'      => [
            'chat_id' => '-1001685063147'
        ]
    ],
    'invite_telegram' => env('INVITE_TELEGRAM')
];
