<?php
/**
 * @see https://github.com/artesaos/seotools
 */

return [
    'meta' => [
        'defaults'       => [
            'title'        => "школа пациента",
            'titleBefore'  => false,
            'description'  => "Вам необходима скорейшая консультация онлайн кардиолога? Сергей Юрьевич Жуков, готов вас принять онлайн и дать рекомендации по вашему здоровью, доступны авторские курсы в личном кабинете.",
            'separator'    => " - ",
            'keywords'     => [],
            'canonical'    => "full", // Set to null or 'full' to use Url::full(), set to 'current' to use Url::current(), set false to total remove
            'robots'       => false // Set to 'all', 'none' or any combination of index/noindex and follow/nofollow
        ],
        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
            'norton'    => null
        ],

        'add_notranslate_class' => false,
    ],
    'opengraph' => [
        'defaults' => [
            'title'       => "школа пациента", // set false to total remove
            'description' => "Вам необходима скорейшая консультация онлайн кардиолога? Сергей Юрьевич Жуков, готов вас принять онлайн и дать рекомендации по вашему здоровью, доступны авторские курсы в личном кабинете.", // set false to total remove
            'url'         => false, // Set null for using Url::current(), set false to total remove
            'type'        => "WebSite",
            'site_name'   => "Medschool35",
            'images'      => [
                //
            ]
        ],
    ],
    'twitter' => [
        'defaults' => [
            //'card'        => 'summary',
            //'site'        => '@LuizVinicius73',
        ],
    ],
    'json-ld' => [
        'defaults' => [
            'title'       => "школа пациента", // set false to total remove
            'description' => "Вам необходима скорейшая консультация онлайн кардиолога? Сергей Юрьевич Жуков, готов вас принять онлайн и дать рекомендации по вашему здоровью, доступны авторские курсы в личном кабинете.",
            'url'         => null, // Set null for using Url::current(), set false to total remove
            'type'        => "WebSite",
            'images'      => [
                //
            ]
        ],
    ],
];
