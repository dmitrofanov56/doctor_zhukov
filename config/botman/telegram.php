<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Telegram Token
    |--------------------------------------------------------------------------
    |
    | Your Telegram bot token you received after creating
    | the chatbot through Telegram.
    |
    */
    'token' => env('TELEGRAM_TOKEN'),

    'conversation_cache_time' => 2,

    'hideInlineKeyboard' => false,


    'site_words' => [
        'сайт',
        'сайте',
        'сайтом'
    ]
];
