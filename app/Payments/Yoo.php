<?php

namespace App\Payments;

use App\Interfaces\Pay;
use App\Models\Order;
use App\Traits\YooKassaGateway;
use Kenvel\Tinkoff;

class Yoo implements Pay
{
    use YooKassaGateway;

    protected string $title;
    protected string $description;
    protected int $amount;

    /**
     * Сумма
     * @param int $amount
     *
     * @return $this
     */

    public function amount(int $amount) : self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Описание
     * @param string $description
     *
     * @return $this
     */

    public function description(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Название
     * @param string $title
     *
     * @return $this
     */

    public function title(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function payUrl(Order $order, string $description, $data)
    {
        return $this->getPayUrl($order->id, $order->order , $this->amount, $description, $data);
    }

    public function cancelPayment(string $paymentId)
    {
        return $this->client->cancelPayment($paymentId);
    }
}
