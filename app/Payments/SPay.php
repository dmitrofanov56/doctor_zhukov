<?php

namespace App\Payments;

use App\Interfaces\Pay;
use App\Models\Order;
use Kenvel\Tinkoff;

class SPay implements Pay
{
    protected string $order;
    protected string $title;
    protected string $description;
    protected int $amount;
    protected Tinkoff $payment;

    public function __construct()
    {
        $this->payment = new Tinkoff(
            env('TINKOFF_URL'),
            env('TINKOFF_TERMINAL'),
            env('TINKOFF_SECRET')
        );
    }

    /**
     * Сумма
     * @param int $amount
     *
     * @return $this
     */

    public function amount(int $amount) : self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Описание
     * @param string $description
     *
     * @return $this
     */

    public function description(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Название
     * @param string $title
     *
     * @return $this
     */

    public function title(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Номер платежа
     * @param string $order
     *
     * @return $this
     */

    public function orderId(string $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function payUrl(Order $order)
    {
        $payment = [
            'OrderId'     => $order->order,
            'Amount'      => $this->amount,
            'Language'    => 'ru',
            'Description' => $this->description ?: 'Оплата заказа # ' . $order->id,
            'Email'       => $order->user->email,
            'Phone'       => $order->user->phone,
            'Name'        => $order->user->username,
            'Taxation'    => 'usn_income'
        ];

        $items[] = [
            'Name'  => $this->title ?: 'Оплата заказа # ' . $order->id,
            'Price' => $this->amount,
            'NDS'   => '0',
        ];

        return $this->payment->paymentURL($payment, $items);
    }

    /**
     * Подтверждение платежа
     * @param string $paymentId
     */

    public function confirmPayment(string $paymentId)
    {
        $this->payment->confirmPayment($paymentId);
    }

    /**
     * Отмена платежа
     * @param string $paymentId
     */

    public function cancelPayment(string $paymentId)
    {
        $this->payment->cencelPayment($paymentId);
    }

    /**
     * Получить информацию о платеже
     * @param string $paymentId
     *
     * @return string
     */

    public function getState(string $paymentId) : string
    {
        return $this->payment->getState($paymentId);
    }
}
