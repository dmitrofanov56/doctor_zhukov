<?php

namespace App\Payments;

use App\Interfaces\Pay;
use App\Models\Order;
use Illuminate\Http\Request;
use Kenvel\Tinkoff;

class TPay implements Pay
{

    protected string  $order;
    protected string  $title;
    protected string  $description;
    protected int     $amount;
    protected Tinkoff $payment;

    protected const STATUS_SUCCESS  = 'CONFIRMED';
    protected const STATUS_REJECTED = 'REJECTED';
    protected const STATUS_CANCELED = 'CANCELED';
    protected const STATUS_PAY      = 1;

    public function __construct()
    {
        $this->payment = new Tinkoff(
            env('TINKOFF_URL'),
            env('TINKOFF_TERMINAL'),
            env('TINKOFF_SECRET')
        );
    }

    /**
     * Сумма
     *
     * @param int $amount
     *
     * @return $this
     */

    public function amount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Описание
     *
     * @param string $description
     *
     * @return $this
     */

    public function description(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Название
     *
     * @param string $title
     *
     * @return $this
     */

    public function title(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Номер платежа
     *
     * @param string $order
     *
     * @return $this
     */

    public function orderId(string $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function payUrl(Order $order)
    {
        $payment = [
            'OrderId'     => $order->order,
            'Amount'      => $this->amount,
            'Language'    => 'ru',
            'Description' => $this->description ?: 'Оплата заказа # ' . $order->id,
            'Email'       => $order->user->email,
            'Phone'       => $order->user->phone,
            'Name'        => $order->user->username,
            'Taxation'    => 'usn_income',
        ];

        $items[] = [
            'Name'  => $this->title ?: 'Оплата заказа # ' . $order->id,
            'Price' => $this->amount,
            'NDS'   => '0',  //НДС
        ];

        if (!$this->payment->paymentURL($payment, $items)) {
            return false;
        }

        $order->update(['payment_id' => $this->payment->payment_id]);

        return $this->payment->payment_url;
    }

    /**
     * Подтвердить платёж
     * @param Order   $order
     * @param Request $request
     */

    public function checkPay(Order $order, Request $request)
    {
        $payment_id = $request->post('PaymentId');

        $order->update(['payment_id' => $payment_id]);

        $status     = $request->post('Status');

        if ($status == self::STATUS_SUCCESS) :

            // Обновим статус

            $order->update([
                'status' => self::STATUS_PAY,
            ]);

            // Если применён бонус

            if ($order->discount && $order->discount->used_count > 0) :
                $order->discount()->decrement('used_count');
            endif;

            // Подтвердим платёж

            $this->confirmPayment($payment_id);

        // Если платёж не прошёл

        elseif ($status == self::STATUS_REJECTED || $status == self::STATUS_CANCELED) :
            $this->cancelPayment($payment_id);
        endif;
    }

    /**
     * Подтверждение платежа
     *
     * @param string $paymentId
     */

    public function confirmPayment(string $paymentId)
    {
        $this->payment->confirmPayment($paymentId);
    }

    /**
     * Отмена платежа
     *
     * @param string $paymentId
     */

    public function cancelPayment(string $paymentId)
    {
        $this->payment->cencelPayment($paymentId);
    }

    /**
     * Получить информацию о платеже
     *
     * @param string $paymentId
     *
     * @return string
     */

    public function getState(string $paymentId): string
    {
        return $this->payment->getState($paymentId);
    }

}
