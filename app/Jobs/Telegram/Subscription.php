<?php

namespace App\Jobs\Telegram;

use App\Payments\Yoo;
use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\Extensions\Keyboard;
use BotMan\Drivers\Telegram\Extensions\KeyboardButton;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldBeUniqueUntilProcessing;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use YooKassa\Model\PaymentStatus;

class Subscription implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The product instance.
     *
     * @var \App\Models\Subscription
     */

    public $subscription;

    /**
     * The number of seconds after which the job's unique lock will be released.
     *
     * @var int
     */

    public $uniqueFor = 3600;

    public $timeout = 30;

    public $tries = 2;


    public function __construct(\App\Models\Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * The unique ID of the job.
     *
     * @return string
     */

    public function uniqueId() : string
    {
        return $this->subscription->id;
    }

    public function handle()
    {
        if ($this->job->maxTries()) {

            info('Исчерпали все попытки оплаты' . $this->subscription->user);

            $this->subscription->update(['status' => 'not_active']);

            return $this->delete();
        }

        $pay = (new Yoo)->autoPay($this->subscription->amount, $this->subscription->payment_method_id);

        if ($pay['status'] == PaymentStatus::SUCCEEDED) {

            $this->delete();

            $this->subscription->update([
                'expired_at' => now()->addDays($this->subscription->days),
                'status'     => 'active'
            ]);

            $keyboard = Keyboard::create(Keyboard::TYPE_INLINE)
                ->oneTimeKeyboard()
                ->addRow(KeyboardButton::create('Перейти в канал')->url(env('INVITE_TELEGRAM')))
                ->toArray();

            $data = array_merge([
                'parse_mode' => 'html'
            ], $keyboard);

            return BotMan::say('Спасибо, вы успешно продлили подписку в канале', $this->subscription->user->telegram_id , TelegramDriver::class, $data);
        }

        if ($pay['status'] == PaymentStatus::CANCELED) {

            $reason = $pay['cancellation_details']['reason'];

            $this->subscription->update(['status' => $reason]);

            $this->release(now()->addHours(5));

            $data = array_merge([
                'parse_mode' => 'html'
            ]);

            BotMan::say('Не удалось <b>продлить подписку</b>, повторная попытка списания средств будет через <b>5</b> часов.', $this->subscription->user->telegram_id , TelegramDriver::class, $data);

            return false;
        }
    }
}
