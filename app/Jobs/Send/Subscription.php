<?php

namespace App\Jobs\Send;


use App\Actions\Materials\RandomOnSubscription;
use App\Mail\Subscriptions\SendMaterial;
use App\Models\User;

use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Facades\Mail;

class Subscription implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    public $material;

    public function __construct(User $user, $material)
    {
        $this->user         = $user;
        $this->material     = $material;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->user->email)->send(new SendMaterial($this->user, $this->material));

        BotMan::say("Спасибо, вы оформили подписку на ежемесячный материал, вся информация отправлена на вашу почту которую вы указали.",
            $this->user->telegram_id,
            TelegramDriver::class
        );
    }
}
