<?php

namespace App\Jobs\NewsLetter;

use App\Models\NewsLetter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendToUserNotify implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public $users;

    public function __construct(NewsLetter $news_letter, $users)
    {
        $this->data = $news_letter;
        $this->users = $users;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle($users)
    {
        foreach ($this->users as $user) {
            $user->notify(new \App\Notifications\Admin\NewsLetter($this->data));
        }
    }
}
