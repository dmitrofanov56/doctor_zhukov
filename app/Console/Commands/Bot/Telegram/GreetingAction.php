<?php

namespace App\Console\Commands\Bot\Telegram;

use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Incoming\IncomingMessage;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class GreetingAction extends Conversation
{
    public function greeting()
    {
        $question = Question::create("Меню чата")
            ->fallback('unable')
            ->callbackId('commands')
            ->addButtons([
                Button::create('Мне нужна консультация')->value('consult'),
                Button::create('Мне нужна повторная консультация')->value('consult_two'),
                Button::create('Купить обучающие материалы')->value('learning'),
            ]);

        $this->ask($question, function (Answer $answer) use ($question) {

            if ($answer->isInteractiveMessageReply()) {

                $value = $answer->getText();

                switch ($value) {
                    case 'consult':
                        $value = "Вам необходима консультация доктора?\n\nДля платной консультации необходимо оплатить 2000 руб на номер <b>8 (911) 829-07-40</b> (СБЕРБАНК), и написать в WhatsApp что-бы я смог назначить время.";
                        break;
                    case 'consult_two':
                        $value = "Вам необходима повторная консультация доктора?\n\nДля повторной консультации необходимо оплатить 1500 руб на номер <b>8 (911) 829-07-40</b> (СБЕРБАНК), и написать в WhatsApp что-бы я смог назначить время.";
                        break;
                    case 'learning':
                        $value = "В скором времени заработает .... ";
                        break;
                    default:
                }

                $this->say($value, [
                    'parse_mode' => 'html'
                ]);
            }

        });
    }

    public function run()
    {
        $this->greeting();
    }
}
