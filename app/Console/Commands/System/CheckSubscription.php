<?php

namespace App\Console\Commands\System;

use App\Models\Subscription;
use App\Payments\Yoo;
use App\Repository\Telegram\SubscriptionRepository;
use App\Traits\YooKassaGateway;
use BotMan\BotMan\Facades\BotMan;
use Doctrine\DBAL\Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use YooKassa\Model\PaymentStatus;

class CheckSubscription extends Command
{
   // use YooKassaGateway;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:sub';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверка подписок';

    public function handle()
    {
        (new SubscriptionRepository)->subscriptionsActive()->each(function (Subscription $subscription) {

            // если текущая дата больше чем в подписке то подписку ставим не активную

            if ($subscription->expired_at->diffInDays() > 1 ) {
                $subscription->update(['status' => 'active']);
            }

            if ($subscription->expired_at->diffInDays() <= 1) {

                info('Меньше 1 дня ' . $subscription->user);

                \App\Jobs\Telegram\Subscription::dispatch($subscription)
                    ->onQueue('subscription')->delay(
                        now()->addMinutes(rand(1,500))
                    );
            }
        });
    }
}
