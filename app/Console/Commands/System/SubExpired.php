<?php

namespace App\Console\Commands\System;

use App\Models\ChatJoin;
use App\Models\Subscription;
use App\Repository\Telegram\SubscriptionRepository;
use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Console\Command;

class SubExpired extends Command
{
    protected $signature = 'expired:run';

    protected $description = 'Expired Subscriptions';

    public function handle()
    {
        (new SubscriptionRepository)->subscriptionsNotActive()->each(function (Subscription $subscription) {

            $telegram_id = $subscription->user->telegram_id;

            \Illuminate\Support\Facades\Http::post('https://api.telegram.org/bot' . config('botman.telegram.token') . '/banChatMember', [
                'chat_id' => -1001539682289,
                'user_id' => $telegram_id
            ])->json();

            $bot = \Illuminate\Support\Facades\Http::post('https://api.telegram.org/bot' . config('botman.telegram.token') . '/unbanChatMember', [
                'chat_id' => -1001539682289,
                'user_id' => $telegram_id,
                'only_if_banned' => true
            ])->json('ok');

            $data = [
                'parse_mode' => 'html'
            ];

            BotMan::say('К сожалению вы больше не участник <b>закрытого канала</b>, вы вовремя не оплатили подписку и <b>были исключены</b>, для того что бы попасть в канал нажмите /start и оплатите подписку повторно', $telegram_id, TelegramDriver::class, $data);

            ChatJoin::where('user_chat_id', $telegram_id)->update(['status' => 'decline']);

            $subscription->update(['status' => 'banned']);
        });
    }
}
