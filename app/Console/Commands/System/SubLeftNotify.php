<?php

namespace App\Console\Commands\System;

use App\Models\Subscription;
use App\Repository\Telegram\SubscriptionRepository;
use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Console\Command;

class SubLeftNotify extends Command
{
    protected $signature = 'subLeftNotify:run';

    protected $description = 'Command description';

    public function handle()
    {
        (new SubscriptionRepository)->subscriptionsActive()->each(function (Subscription $subscription) {
            if ($subscription->expired_at->diffInDays() < 10 ) {
                BotMan::say("Здравствуйте, до окончания подписки осталось <b>{$subscription->expired_at->diffInDays()} дней</b>, не забудьте сохранить <b>сумму</b> до окончания срока, чтобы остаться в канале и получать информацию дальше." , $subscription->user->telegram_id, TelegramDriver::class, [
                    'parse_mode' => 'html'
                ] );
            }
        });
    }
}
