<?php

namespace App\Console\Commands;

use App\Repository\ItemOrderRepository;
use App\Models\OrderItem;
use App\Notifications\Customer\Webinar;
use Illuminate\Console\Command;

class BeforeWebinar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webinar:run';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'Оповещение за 5 минут до вебинара';

    public function handle()
    {
        (new ItemOrderRepository)->subscribesWebinars()->each(function (OrderItem $item) {
            if ($item->webinar->status) {
                $item->order->user->notify(new Webinar($item->webinar));
            }
        });
    }
}
