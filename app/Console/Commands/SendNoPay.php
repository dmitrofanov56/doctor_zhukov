<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\User;
use Illuminate\Console\Command;

class SendNoPay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'noPay:run';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'Запуск неоплаченных счетов';

    public function handle()
    {
        return Order::where('status', 0)->each(function (Order $order) {
            $order->user->noPayNotify();
        });

      /*  User::query()->each(function (User $user) {

            $orders = $user->orders()->where('status', 0)->count();

            if (! $orders) return false;

            $user->noPayNotify();
        });*/
    }
}
