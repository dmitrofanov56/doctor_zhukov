<?php

namespace App\Console\Commands\Telegram;

use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\Extensions\Keyboard;
use BotMan\Drivers\Telegram\Extensions\KeyboardButton;
use Illuminate\Console\Command;

class RelatedProductsForChannel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'relatedProducts:run';

    public function handle()
    {
        $pdf = (new \App\Repository\PdfRepository)->bestWeeks();

        $video = (new \App\Repository\VideoRepository)->bestWeeks();

        $keyboard = Keyboard::create(Keyboard::TYPE_INLINE)
            ->oneTimeKeyboard()
            ->addRow(KeyboardButton::create('Купить ' . $pdf->title)->url('https://medschool35.ru'))
            ->addRow(KeyboardButton::create('Купить ' . $video->title)->url('https://medschool35.ru'))
            ->toArray();

        $data = array_merge([
            'parse_mode' => 'html',
        ], $keyboard);

        $message = sprintf("Уважаемые Друзья!\n\nЗа <b>эту неделю</b> статистика показала что пользователям нашего проекта нравиться читать эти материалы\n\n📋 %s (купили {$pdf->totalPay} чел)\n\n🎥 %s (купили {$video->totalPay} чел)\n\nЕсли у вас еще нет такого материала, вы можете приобрести данный материал на сайте. я стараюсь делать только доступный и понятный материал для каждого. ❤" , $pdf->title, $video->title);

        BotMan::say($message , env('CHAT_ID'), \BotMan\Drivers\Telegram\TelegramDriver::class , $data);
    }
}
