<?php

namespace App\Console\Commands\Telegram;

use App\Models\Subscription;
use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\Extensions\Keyboard;
use BotMan\Drivers\Telegram\Extensions\KeyboardButton;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Console\Command;

class UnbanActive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chat:unban';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        (new \App\Repository\Telegram\SubscriptionRepository)->subscriptionsAll()->each(function (Subscription $subscription) {

            if ($subscription->user->telegram_id) {

                $telegram_id = $subscription->user->telegram_id;

                /*$bot = \Illuminate\Support\Facades\Http::post('https://api.telegram.org/bot' . config('botman.telegram.token') . '/unbanChatMember', [
                    'chat_id' => env('SUBS_CHAT_ID'),
                    'user_id' => $subscription->user->telegram_id,
                    'only_if_banned' => true
                ])->json('ok');*/

                $keyboard = Keyboard::create(Keyboard::TYPE_INLINE)
                    ->oneTimeKeyboard()
                    ->addRow(KeyboardButton::create('Перейти в клуб')->url(env('INVITE_TELEGRAM')))
                    ->toArray();

                $data = array_merge([
                    'parse_mode' => 'html'
                ], $keyboard);

                BotMan::say("Здравствуйте, заходите в канал была перенастройка, и мы завершили все работы, для того чтобы перейти в клуб нажмите <b>Перейти в клуб</b>, если вас не пускает значит у вас нет подписки\n\nБудем рады вас видеть снова! ", $telegram_id, TelegramDriver::class, $data);
            }
        });
    }
}
