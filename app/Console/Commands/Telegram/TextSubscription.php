<?php

namespace App\Console\Commands\Telegram;

use App\Models\Post;
use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\Extensions\Keyboard;
use BotMan\Drivers\Telegram\Extensions\KeyboardButton;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Console\Command;

class TextSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instruction:subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    public function handle()
    {
        $keyboard = Keyboard::create(Keyboard::TYPE_INLINE)
            ->oneTimeKeyboard()
            ->addRow(KeyboardButton::create('Перейти к оформлению подписки')->url('https://t.me/medshool35_bot'))
            ->toArray();

        $data = array_merge([
            'parse_mode' => 'html',
            'disable_web_page_preview' => true
        ], $keyboard);

        $post = Post::find(25);

        $format = str_ireplace('&nbsp;', '', $post->content);
        $format = strip_tags($format, ['<strong>']);

        BotMan::say("{$format}", env('CHAT_ID') , TelegramDriver::class, $data);
    }
}
