<?php

namespace App\Console\Commands\Telegram;

use App\Models\Post;
use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\Extensions\Keyboard;
use BotMan\Drivers\Telegram\Extensions\KeyboardButton;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Console\Command;

class InstructionForChannel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instruction:telegram';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    public function handle()
    {
        $keyboard = Keyboard::create(Keyboard::TYPE_INLINE)
            ->oneTimeKeyboard()
            ->addRow(KeyboardButton::create('Купить материалы')->url('https://medschool35.ru/cabinet'))
            ->addRow(KeyboardButton::create('Личный кабинет')->url('https://medschool35.ru/cabinet'))
            ->addRow(KeyboardButton::create('Мои видео')->url('https://medschool35.ru/cabinet/videos'))
            ->addRow(KeyboardButton::create('Мои материалы для чтения')->url('https://medschool35.ru/cabinet/pdfs'))
            ->addRow(KeyboardButton::create('Мои вебинары')->url('https://medschool35.ru/cabinet/webinars/my'))
            ->addRow(KeyboardButton::create('Услуги по платной консультации')->url('https://medschool35.ru/cabinet/service'))
            ->toArray();

        $data = array_merge([
            'parse_mode' => 'html',
            'disable_web_page_preview' => true
        ], $keyboard);

        $post = Post::find(27);

        $format = str_ireplace('&nbsp;', '', $post->content);
        $format = strip_tags($format, ['<strong>']);

        BotMan::say("{$format}", env('CHAT_ID') , TelegramDriver::class , $data );
    }
}
