<?php

namespace App\Console\Commands\Telegram;

use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\Extensions\Keyboard;
use BotMan\Drivers\Telegram\Extensions\KeyboardButton;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class GreetingsForChannel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'greetings:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public function handle()
    {
        $day  = Carbon::now()->isoFormat('dddd');

        $data = array_merge([
            'parse_mode' => 'html',
        ]);

        BotMan::say("Доброе утро! ☀️\n\nВсем хорошего дня и отличного самочувствия\n\n<b>Сегодня:</b> " . $day, env('CHAT_ID') , TelegramDriver::class , $data );
    }
}
