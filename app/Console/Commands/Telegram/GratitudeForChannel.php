<?php

namespace App\Console\Commands\Telegram;

use App\Models\Order;
use App\Models\User;
use App\Repository\StatsRepository;
use BotMan\BotMan\Facades\BotMan;
use Illuminate\Console\Command;

class GratitudeForChannel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gratitude:run';

    public function handle()
    {
        $users = (new StatsRepository(new User))->todayCount();

        $successOrders = (new StatsRepository(new Order))->successPayCount();

        $message = sprintf("❤ Мы очень ценим за то что вы с нами,и благодарим каждого пользователя за финансовую поддержку, ведь каждый купленный продукт вами, помогает нам развивать сайт в лучшую сторону для вас, а это значит что проект <b>МЕДСКУЛ</b> двигается в правильном направлении!\n\nЗа сегодня зарегистрировалось: %s чел. \nПоддержали проект: %s чел.\n\nСпасибо вам за доверие!" , $users, $successOrders);

        BotMan::say($message , env('CHAT_ID'), \BotMan\Drivers\Telegram\TelegramDriver::class , [
            'parse_mode' => 'html'
        ]);
    }
}
