<?php

namespace App\Console\Commands\Telegram;

use BotMan\BotMan\Facades\BotMan;
use Illuminate\Console\Command;

class CommandsForChannel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'botCommands:run';

    public function handle()
    {
        BotMan::say("Команды для чата, это поможет вам быстрее получить консультацию или быстрый ответ на интересующие вас вопросы.\n\nЧтобы вызвать данное меню введите в чате !команды",
            env('CHAT_ID'),
            \BotMan\Drivers\Telegram\TelegramDriver::class
        );
    }
}
