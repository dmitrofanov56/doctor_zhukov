<?php

namespace App\Console\Commands\Telegram;

use App\Models\Post;
use App\Repository\Telegram\SubscriptionRepository;
use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\Extensions\Keyboard;
use BotMan\Drivers\Telegram\Extensions\KeyboardButton;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Console\Command;


class GiftOfMonth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gift:month';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    public function handle()
    {
        $date = now()->subDays(60);

        $sub = (new SubscriptionRepository)->winMonthRandomUser(60);

        $post = Post::find(26);

        $format = str_ireplace('&nbsp;', '', $post->content);
        $format = str_ireplace('{user}', $sub->user->username, $format);
        $format = str_ireplace('{telegram_id}', $sub->user->telegram_id, $format);
        $format = str_ireplace('{telegram_username}', $sub->user->username, $format);
        $format = str_ireplace('{days}', rand(150, 200), $format);

        $format = str_ireplace('<strong>', '<b>', $format);
        $format = str_ireplace('</strong>', '</b>', $format);
        $format = str_ireplace('<p>', '', $format);
        $format = str_ireplace('</p>', '', $format);
        $format = str_ireplace('<br>', '', $format);

        BotMan::say("{$format}", env('CHAT_ID') , TelegramDriver::class, [
            'parse_mode' => 'html'
        ]);
    }
}
