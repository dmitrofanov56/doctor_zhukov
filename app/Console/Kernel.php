<?php

namespace App\Console;

use App\Jobs\NewsLetter\SendToUserNotify;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Создать очередь неоплаченных товаров

        $schedule->command('noPay:run')->daily();

       // $schedule->command('webinar:run')->withoutOverlapping()->dailyAt("10:00");
       // $schedule->command('telescope:prune')->daily();

        $schedule->command('queue:work --queue=sending --stop-when-empty --tries=5 --sleep=3')->withoutOverlapping()->everyMinute();
        $schedule->command('queue:work --queue=subscription --stop-when-empty --timeout=30 --tries=2 --sleep=3')->withoutOverlapping()->everyMinute();

        $schedule->command('greetings:run')->dailyAt("09:30");
        $schedule->command('instruction:telegram')->weeklyOn(1, '07:00');
        $schedule->command('gratitude:run')->weeklyOn(1, '10:00');
        $schedule->command('instruction:subscription')->dailyAt("12:00");
        $schedule->command('relatedProducts:run')->weeklyOn(1, '12:00');
        $schedule->command('gift:month')->monthlyOn(15, '15:00');

        // исключать из платной группы если кончилась подписка и нет продления
        $schedule->command('check:sub')->withoutOverlapping()->everyMinute();
       // $schedule->command('chat:unban')->withoutOverlapping()->everyMinute();
        $schedule->command('subLeftNotify:run')->daily();
        $schedule->command('expired:run')->withoutOverlapping()->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
