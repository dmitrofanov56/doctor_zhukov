<?php

namespace App\Listeners;

use App\Events\CustomRegister;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendRegister
{
    public function handle(CustomRegister $event)
    {
        Mail::to($event->user)->send(new \App\Mail\User\CustomRegister($event->user, $event->password));
    }
}
