<?php

namespace App\Traits;

use App\Notifications\Customer\Invoice;
use App\Repository\DiscountRepository;
use App\Repository\OrderRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

trait Order
{
    public $textPay;

    public function payToPayment($data = [])
    {
        $discount = session()->has('discount') ? (new DiscountRepository())->findByCode(session('discount')) : [];

        if (\Cart::isEmpty()) {
            return false;
        }

        $order = (new OrderRepository)->create('Yoo', $discount);

        foreach (\Cart::getContent()->toArray() as $item)
        {
            if (preg_match('/(telegram)/', $item['id']))
            {
                $telegram = $order->telegram()->create([
                    'join_url' => 'https://t.me/joinchat/aJb7cqj9YME0Y2U6',
                    'sum_pay'  => $item['price'],
                ]);

                $order->items()->create([
                    'telegram_access_id' => $telegram->id,
                    'type'               => 'telegram',
                    'price'              => $telegram->sum_pay,
                ]);
            }

            if (preg_match('/(service)/', $item['id']))
            {
                $id = (string) Str::of($item['id'])->replaceFirst('service_', '');

                $order->items()->create([
                    'service_id' => (int) $id,
                    'price'      => $item['price'],
                    'type'       => 'service',
                ]);
            }

            if (preg_match('/(video)/', $item['id']))
            {
                $id = (string) Str::of($item['id'])->replaceFirst('video_', '');

                $order->items()->create([
                    'video_id' => (int) $id,
                    'price'    => $item['price'],
                    'type'     => 'video',
                ]);
            }

            if (preg_match('/(pdf)/', $item['id']))
            {
                $id = (string) Str::of($item['id'])->replaceFirst('pdf_', '');

                $order->items()->create([
                    'pdf_id' => (int) $id,
                    'price'  => $item['price'],
                    'type'   => 'pdf',
                ]);
            }

            if (preg_match('/(webinar)/', $item['id']))
            {
                $id = (string) Str::of($item['id'])->replaceFirst('webinar_', '');

                $order->items()->create([
                    'webinar_id' => (int) $id,
                    'price'      => $item['price'],
                    'type'       => 'webinar',
                ]);
            }
        }

        // Создаём платёж

        $payUrl =  $order->payYoo();

        // Платежная система

        auth()->user()->sendInvoice();

        // редирект на оплату

        return redirect($payUrl);
    }

}
