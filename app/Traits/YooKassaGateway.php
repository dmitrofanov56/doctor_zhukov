<?php

namespace App\Traits;

use App\Models\User;
use YooKassa\Client;
use YooKassa\Model\NotificationEventType;

trait YooKassaGateway
{
    public $client;
    public $builder;

    public function __construct()
    {
        $this->builder = \YooKassa\Request\Payments\CreatePaymentRequest::builder();
        $this->client = (new Client())->setAuth('848919' , env('YOOKASSA_KEY'));
    }

    public function getCustomPayUrl($order_id, $amount, $description, $data)
    {
        $data = collect([
            'cms_name'       => 'МЕДСКУЛ',
            'order_id'       => $order_id,
            'user_id'        => auth()->id(),
            'language'       => 'ru',
            'transaction_id' => $order_id,
        ])->merge($data);

        $this->builder->setAmount($amount)
            ->setCurrency(\YooKassa\Model\CurrencyCode::RUB)
            ->setCapture(true)
            ->setDescription($description . ' ' . $order_id)
            ->setMetadata($data->toArray());

        $this->builder->setConfirmation(array(
            'type'      => \YooKassa\Model\ConfirmationType::REDIRECT,
            'returnUrl' => route('customer.orders')
        ));

        $this->builder->setCapture(true);
        $this->builder->setSavePaymentMethod(true);

        $this->builder->setReceiptEmail($data['email']);

        $request = $this->builder->build();

        return $this->client->createPayment($request)->confirmation->getConfirmationUrl();
    }

    public function getPayUrl($orderId, $order, $amount, $description, $data)
    {
        $data = collect([
            'cms_name'       => 'МЕДСКУЛ',
            'order_id'       => $orderId,
            'user_id'        => auth()->id(),
            'language'       => 'ru',
            'transaction_id' => $order,
        ])->merge($data);

        $this->builder->setAmount($amount)
                ->setCurrency(\YooKassa\Model\CurrencyCode::RUB)
                ->setCapture(true)
                ->setDescription($description . ' ' . $orderId)
                ->setMetadata($data->toArray());

        $this->builder->setConfirmation(array(
            'type'      => \YooKassa\Model\ConfirmationType::REDIRECT,
            'returnUrl' => route('customer.orders')
        ));

        $this->builder->setReceiptEmail(auth()->user()->email);
        $this->builder->setReceiptPhone(auth()->user()->phone);

        $request = $this->builder->build();

        return $this->client->createPayment($request)->confirmation->getConfirmationUrl();
    }

    /**
     * @param float $amount
     * @param $autoPayId
     * @return void
     */

    public function autoPay(float $amount, $autoPayId)
    {
        return $this->client->createPayment(
            array(
                'amount' => array(
                    'value' => $amount,
                    'currency' => 'RUB',
                ),
                'capture' => true,
                'payment_method_id' => $autoPayId,
                'description' => 'Продление подписки на 1 месяц',
            ),
            uniqid('', true)
        );
    }

    public function getState($payment_id)
    {
        return $this->client->getPaymentInfo($payment_id);
    }

}
