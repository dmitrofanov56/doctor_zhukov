<?php

namespace App\Traits;

use App\Actions\Telegram\Converstation\Buy\Webinar;
use App\Actions\Telegram\Converstation\User;
use App\Repository\UserRepository;

trait HasUserTelegram
{
    public $user;

    public function checkUser($converstation)
    {
        $this->user = (new UserRepository)->checkTelegramId($this->bot->getUser()->getId());

        if (! $this->user) {
            $this->bot->startConversation(new User($converstation));
        }
    }

}
