<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property                     $user_id     int
 * @property                     $order       string
 * @property                     $status      int
 * @property                     $discount_id int
 * @property                     $pay_system  string
 * @property                     $payment_id  string
 * @property                     $classPay    string
 * @property-read Discount       $discount
 * @property-read User           $user
 * @property-read OrderItem      $items
 * @property-read SupportProject $support
 */
class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'order',
        'pay_system',
        'payment_id',
        'discount_id',
        'telegram_access',
        'status'
    ];

    public $with = ['items' , 'discount' , 'user' , 'support' , 'telegram'];


    protected $appends = [
        'classPay',
    ];

    /**
     * Товары
     *
     * @return HasMany
     */

    public function items(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    /**
     * Пользователь
     *
     * @return BelongsTo
     */

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Поддержка проекта
     *
     * @return HasOne
     */

    public function support(): HasOne
    {
        return $this->hasOne(SupportProject::class);
    }

    /**
     * Поддержка проекта
     *
     * @return HasOne
     */

    public function telegram(): HasOne
    {
        return $this->hasOne(TelegramAccess::class);
    }

    /**
     * Промокод
     *
     * @return BelongsTo
     */

    public function discount(): BelongsTo
    {
        return $this->belongsTo(Discount::class);
    }

    /**
     * Формирование ссылки оплаты
     *
     * @return string
     */

    public function payUrl(): string
    {
        return $this->classPay
            ->amount($this->items()->sum('price'))
            ->title('Оплата заказа')
            ->description('Покупка обучаещего материала')
            ->payUrl($this);
    }

    public function payYoo($data = [])
    {
        if ($this->status == 2 ) return false;

        $price = auth()->user()->is_admin ? 1 : $this->items()->sum('price');

        $desc = 'Оплата обучающего материала или подписок на платные услуги';

        return $this->classPay
            ->amount($price)
            ->title('Оплата заказа')
            ->description($desc)
            ->payUrl($this, $desc, $data);
    }

    public function payYooCustom($description = 'Оплата обучающего материала или подписок на платные услуги', $data = []): string
    {
        return $this->classPay
            ->amount($this->items()->sum('price'))
            ->title('Оплата заказа')
            ->description($description)
            ->payUrl($this, $description, $data);
    }


    public function supportPay($data = [])
    {
        $desc = 'Поддержка проекта и пожертвования в развитие проекта';

        return $this->classPay
            ->amount($this->items()->sum('price'))
            ->title('Поодержка проекта')
            ->description($desc)
            ->payUrl($this, $desc, $data);
    }

    /**
     * Класс платежной системы
     */

    public function getClassPayAttribute()
    {
        $class = "\App\Payments\\" . $this->pay_system;

        return new $class;
    }

}
