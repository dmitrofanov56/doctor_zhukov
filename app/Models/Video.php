<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property $price int
 */

class Video extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'videoUrl',
        'price'
    ];

    protected $casts = [
        'type'
    ];

    public function getTypeAttribute()
    {
        return 'video';
    }

    public function items() : HasMany
    {
        return $this->hasMany(OrderItem::class);
    }
}
