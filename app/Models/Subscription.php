<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * @property $user_id integer
 * @property $days integer
 * @property $options string
 * @property $payment_method_id string
 * @property $amount float
 * @property $expired_at
 * @property $status string
 */
class Subscription extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'days',
        'options',
        'payment_method_id',
        'amount',
        'expired_at',
        'status'
    ];

    protected $casts = [
        'expired_at' => 'datetime',
        'is_paid'    => 'boolean',
        'options'    => 'collection'
    ];

    /**
     * @param $q
     * @return mixed
     */

    public function scopeActive($q)
    {
        return $q->where('status', 'active');
    }

    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
