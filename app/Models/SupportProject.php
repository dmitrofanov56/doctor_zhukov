<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class SupportProject extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'user_id',
        'name',
        'email',
        'text_comment',
        'sum_pay'
    ];

    public function order() : HasOne
    {
        return $this->hasOne(Order::class , 'id' , 'order_id');
    }
}
