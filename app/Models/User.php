<?php

namespace App\Models;

use App\Actions\Materials\RandomOnSubscription;
use App\Actions\Materials\Telegram\SubscriptionsMaterial;
use App\Mail\Subscriptions\SendMaterial;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Support\Facades\Mail;

/**
 * @property $username string
 * @property $phone string
 * @property $email string
 * @property $telegram_id string
 * @property $password string
 * @property-read Order $orders
 */
class User extends Authenticatable
{

    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'phone',
        'email',
        'password',
        'telegram_id',
        'is_admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

    protected $casts = [
        'email_verified_at'  => 'datetime',
        'is_admin'           => 'boolean'
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function orders() : HasMany
    {
        return $this->hasMany(Order::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function subscription()
    {
        return $this->hasOne(Subscription::class);
    }

    /**
     * Неоплаченные счета
     * @return void
     */

    public function noPayNotify()
    {
        dispatch(new \App\Jobs\Send\OrderReminder($this))->onQueue('sending');
    }

    /**
     * Оповестить о созданном заказе
     * @return void
     */

    public function sendInvoice()
    {
        $order =  $this->orders()->latest()->first();

        dispatch(new \App\Jobs\Send\Invoice($order))->onQueue('sending');
    }

    /**
     * @return void
     */

    public function sendSubscription()
    {
        $material = (new RandomOnSubscription)->material;

        Mail::to($this)->send(new SendMaterial($this, $material));

        (new SubscriptionsMaterial)->send($this->telegram_id, $material);
    }
}
