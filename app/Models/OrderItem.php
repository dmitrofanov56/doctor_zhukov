<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * @property-read Order $order
 * @property-read Video $video
 * @property-read Pdf $pdf
 * @property-read Webinar $webinar
 */

class OrderItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'pdf_id',
        'webinar_id',
        'support_project_id',
        'telegram_access_id',
        'service_id',
        'video_id',
        'price',
        'type'
    ];

    public $with = ['video' , 'pdf' , 'webinar' , 'telegram' , 'service'];

    /**
     * Связь ордера оплаты
     * @return BelongsTo
     */

    public function order() : BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @return BelongsTo
     */

    public function video() : BelongsTo
    {
        return $this->belongsTo(Video::class);
    }

    /**
     * @return BelongsTo
     */

    public function pdf() : BelongsTo
    {
        return $this->belongsTo(Pdf::class);
    }

    /**
     * @return BelongsTo
     */

    public function webinar() : BelongsTo
    {
        return $this->belongsTo(Webinar::class);
    }

    /**
     * @return BelongsTo
     */

    public function telegram() : BelongsTo
    {
        return $this->belongsTo(TelegramAccess::class, 'telegram_access_id');
    }

    /**
     * @return BelongsTo
     */

    public function service() : BelongsTo
    {
        return $this->belongsTo(Service::class);
    }

    /**
     * @return BelongsTo
     */

    public function support() : BelongsTo
    {
        return $this->belongsTo(SupportProject::class, 'support_project_id');
    }

    public function scopeTypeVideo($q)
    {
        return $q->where('type' , 'video')->get();
    }
}
