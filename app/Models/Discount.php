<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property $code string
 * @property $used_count int
 * @property $percent int
 */
class Discount extends Model
{
    use HasFactory;

    protected $fillable = [
      'code',
      'used_count',
      'percent'
    ];

}
