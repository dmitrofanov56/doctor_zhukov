<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TelegramAccess extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'join_url',
        'sum_pay'
    ];
}
