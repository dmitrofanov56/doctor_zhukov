<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * @property \Illuminate\Support\Carbon $date_send_telegram
 * @property string $content
 * @property string $title
 * @property int $status
 * @property int $views
 */
class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'content',
        'date_send_telegram',
        'status',
        'views'
    ];

    /**
     * @return bool
     */

    public function leftPostHas() : bool
    {
       return Carbon::parse($this->date_send_telegram)->diffInDays(now()) >= 1;
    }
}
