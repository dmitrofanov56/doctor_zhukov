<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property $id int
 * @property $title string
 * @property $preview string
 * @property $live_url string
 * @property $description string
 * @property $price int
 * @property $status boolean
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read \Illuminate\Support\Carbon $start_date
 */

class Webinar extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'preview_video',
        'preview_image',
        'live_url',
        'description',
        'price',
        'start_date',
        'status',
    ];

    protected $casts = [
        'price' => 'decimal:0'
    ];

    protected $dates = [
        'start_date'
    ];

    public function orders()
    {
        return $this->hasMany(OrderItem::class)->whereHas('order', function (Builder $builder) {
            $builder->where('status', 1);
        })->orderByDesc('created_at');
    }

    /**
     * @return bool
     */

    public function isExpire() : bool
    {
        return now() > $this->start_date;
    }

    /**
     * Стоимость вебинара
     * @return int
     */

    public function price() : int
    {
        return $this->price;
    }
}
