<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatJoin extends Model
{
    use HasFactory;

    protected $fillable = [
        'username',
        'chat_id',
        'user_chat_id',
        'status'
    ];
}
