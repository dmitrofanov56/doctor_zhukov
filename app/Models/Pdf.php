<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * @property $price int
 */

class Pdf extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'pdfUrl',
        'price'
    ];

    protected $casts = [
        'type'
    ];

    public function getTypeAttribute()
    {
        return 'pdf';
    }

    public $cacheFor = 2592000;

    public $cacheTags = ['pdfs'];

    public $cachePrefix = 'pdfs_';

    protected static $flushCacheOnUpdate = true;

    /**
     * Set the base cache tags that will be present
     * on all queries.
     *
     * @return array
     */
    protected function getCacheBaseTags(): array
    {
        return [
            'pdfs',
        ];
    }

    public function items() : HasMany
    {
        return $this->hasMany(OrderItem::class);
    }
}
