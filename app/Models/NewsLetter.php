<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Rennokki\QueryCache\Traits\QueryCacheable;

/**
 * @property $subject string
 * @property $message string
 */
class NewsLetter extends Model
{
    use HasFactory;

    public $fillable  = [
        'subject',
        'message',
        'active'
    ];
}
