<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Service extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'type',
        'image',
        'description',
        'attributes',
        'price',
        'status'
    ];

    protected $casts = [
        'status'     => 'boolean',
        'attributes' => 'collection'
    ];

    public int $cacheFor = 84600;

    public function setTitleAttribute($value)
    {
        $this->attributes['slug']  = Str::slug($value, '-');
        $this->attributes['title'] = $value;
    }
}
