<?php

namespace App\Models\Telegram;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InviteChat extends Model
{
    use HasFactory;

    protected $fillable = [
        'telegram_id',
        'telegram_from_id',
        'telegram_from_username',
        'telegram_join_id',
        'telegram_join_username',
        'scope'
    ];

    public function invite()
    {
        return $this->hasOne(InviteChat::class , 'telegram_from_id' , 'telegram_from_id');
    }
}
