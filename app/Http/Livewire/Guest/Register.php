<?php

namespace App\Http\Livewire\Guest;

use App\Models\User;
use App\Notifications\User\Register\LightToEmail;
use App\Providers\RouteServiceProvider;
use App\Repository\UserRepository;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Register extends Component
{
    public $email;

    public $phone;

    public $existsUserForEmail = false;

    protected $rules = [
        'email' => 'required|email|unique:users',
        'phone' => 'required',
    ];

    public function register()
    {
        $this->validate();

        $login =  preg_replace('/[@].*/', '', $this->email);

        $checkEmail = (new UserRepository)->checkEmail($this->email);

        if ($checkEmail) {
            return $this->existsUserForEmail = true;
        }

        $user = User::create([
            'username' => $login,
            'phone'    => $this->phone,
            'email'    => $this->email,
            'password' => Hash::make($login),
        ]);

        event(new Registered($user));

        Auth::login($user);

        $user->notify(new LightToEmail($login));

        return redirect(RouteServiceProvider::HOME);
    }

    public function render()
    {
        return view('livewire.guest.register');
    }
}
