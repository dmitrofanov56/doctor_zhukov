<?php

namespace App\Http\Livewire\Dashboard\Customer;

use App\Repository\DiscountRepository;
use App\Repository\PdfRepository;
use App\Repository\ServiceRepository;
use App\Repository\VideoRepository;
use App\Repository\WebinarRepository;
use App\Traits\Order;
use Livewire\Component;

class ChatBuy extends Component
{
    use Order;

    // Скидочный код
    public $discount = null;

    // Массив с сообщениями

    public $messages;

    // Общая стоимость

    public $total = 0;

    public string $type = 'video';

    public function mount()
    {
        $this->total = \Cart::getTotal();

        $this->discount = session()->has('discount') ? session('discount') : null;

        $this->menu();
    }

    public function menu()
    {
        $this->messages = collect()->push([
            'avatar'     => asset('images/robot-chat.png'),
            'type'       => 'new_message',
            'role'       => 'bot',
            'username'   => 'Бот',
            'content'    => 'Какой материал вас интересует? ',
            'delay'      => 100,
            'created_at' => now()->toDateTimeString(),
            'actions'    => [
                'video'    => '<button wire:click="videos" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-primary mb-2 w-100"><i class="fal fa-tv-alt"></i> Видеоматериалы</button>',
                'pdf'      => '<button wire:click="pdfs" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-danger mb-2 w-100"><i class="fal fa-file-signature"></i> PDF материалы</button>',
                'webinars' => '<button wire:click="webinars" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-secondary mb-2 w-100"><i class="fal fa-film-alt"></i> Вебинары / Видео</button>',
                'telegram' => auth()->user()->telegram_id ? '<button wire:click="telegram" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-info mb-2 w-100"><i class="fab fa-telegram"></i> Доступ в телеграм</button>' : '',
                'service'  => '<button wire:click="service" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-warning mb-2 w-100"><i class="fal fa-user-md"></i> Консультация</button>',
                'clear'    => '<button wire:click="clearSelect" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-dark mb-2 w-100"><i class="fal fa-trash-alt"></i> Очистить</button>',
            ],
            'feature'    => [
                'isDate' => false
            ]
        ]);
    }

    public function videos()
    {
        $this->messages->where('type', '=', 'new_message')->each(function ($item, $i) {
            $this->messages->forget($i);
        });

        $this->type = 'video';

        $this->messages->push([
            'avatar'        => asset('images/robot-chat.png'),
            'type'          => 'reply_action',
            'role'          => 'user',
            'username'      => 'Бот',
            'content'       => 'Вот материал который на данный момент есть в продаже по запросу видеоматериалы, что-бы выбрать нажмите на него',
            'delay'         => 100,
            'created_at'    => now()->toDateTimeString(),
            'type_material' => 'video',
            'materials'     => (new VideoRepository())->getAll(),
            'class'         => 'btn btn-sm btn-outline-primary mb-2',
            'actions'       => [
                'other' => '<button wire:click="other" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-primary mb-2 w-100"><i class="fal fa-network-wired"></i> Выбрать другие материалы</button>',
                'pay'   => '<button wire:click="pay" wire:loading.attr="disabled" class="d-block btn btn-sm btn-success mb-2 w-100"><i class="fal fa-ruble-sign"></i> Оплатить </button>',
                'clear' => '<button wire:click="clearSelect" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-dark mb-2 w-100"><i class="fal fa-trash-alt"></i> Очистить</button>',
            ],
            'feature'       => [
                'getTotal' => true,
                'isDate'   => true
            ]
        ]);
    }

    public function pdfs()
    {
        $this->messages->where('type', '=', 'new_message')->each(function ($item, $i) {
            $this->messages->forget($i);
        });

        $this->type = 'pdf';

        $this->messages->push([
            'avatar'        => asset('images/robot-chat.png'),
            'type'          => 'reply_action',
            'role'          => 'user',
            'username'      => 'Бот',
            'content'       => 'Вот материал который на данный момент есть в продаже по запросу PDF, что-бы выбрать нажмите на него',
            'delay'         => 100,
            'created_at'    => now()->toDateTimeString(),
            'type_material' => 'pdf',
            'materials'     => (new PdfRepository())->getAll(),
            'class'         => 'btn btn-sm btn-outline-danger mb-2',
            'actions'       => [
                'other' => '<button wire:click="other" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-primary mb-2 w-100"><i class="fal fa-network-wired"></i> Выбрать другие материалы</button>',
                'pay'   => '<button wire:click="pay" wire:loading.attr="disabled" class="d-block btn btn-sm btn-success mb-2 w-100"><i class="fal fa-ruble-sign"></i> Оплатить </button>',
                'clear' => '<button wire:click="clearSelect" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-dark mb-2 w-100"><i class="fal fa-trash-alt"></i> Очистить</button>',
            ],
            'feature'       => [
                'getTotal' => true,
                'isDate'   => true
            ]
        ]);
    }

    public function webinars()
    {
        $this->messages->where('type', '=', 'new_message')->each(function ($item, $i) {
            $this->messages->forget($i);
        });

        $this->type = 'webinar';

        $this->messages->push([
            'avatar'        => asset('images/robot-chat.png'),
            'type'          => 'reply_action',
            'role'          => 'user',
            'username'      => 'Бот',
            'content'       => 'Вот материал который на данный момент есть в продаже по запросу вебинары / видео, что-бы выбрать нажмите на него',
            'delay'         => 100,
            'type_material' => 'webinar',
            'created_at'    => now()->toDateTimeString(),
            'materials'     => (new WebinarRepository())->getAll(),
            'class'         => 'btn btn-sm btn-outline-secondary mb-2',
            'actions'       => [
                'other' => '<button wire:click="other" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-primary mb-2 w-100"><i class="fal fa-network-wired"></i> Выбрать другие материалы</button>',
                'pay'   => '<button wire:click="pay" wire:loading.attr="disabled" wire:loading.attr="disabled" class="d-block btn btn-sm btn-success mb-2 w-100"><i class="fal fa-ruble-sign"></i> Оплатить </button>',
                'clear' => '<button wire:click="clearSelect" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-dark mb-2 w-100"><i class="fal fa-trash-alt"></i> Очистить</button>',
            ],
            'feature'       => [
                'getTotal' => true,
                'isDate'   => true
            ]
        ]);
    }

    public function telegram()
    {
        $this->messages->where('type', '=', 'new_message')->each(function ($item, $i) {
            $this->messages->forget($i);
        });

        $this->type = 'telegram';

        $this->messages->push([
            'avatar'        => asset('images/robot-chat.png'),
            'type'          => 'reply_action',
            'role'          => 'user',
            'username'      => 'Бот',
            'content'       => 'Доступ в платный телеграм канал вам даёт возможность более развёрнуто общаться с доктором и получать развёрнутые ответы на ваш вопрос',
            'delay'         => 100,
            'type_material' => 'telegram',
            'created_at'    => now()->toDateTimeString(),
            'materials'     => [
                [
                    'id'    => 1,
                    'title' => 'Платный канал',
                    'price' => 299,
                ]
            ],
            'class'         => 'btn btn-sm btn-outline-secondary mb-2',
            'actions'       => [
                'other' => '<button wire:click="other" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-primary mb-2 w-100"><i class="fal fa-network-wired"></i> Выбрать другие материалы</button>',
                'pay'   => '<button wire:click="pay" wire:loading.attr="disabled" wire:loading.attr="disabled" class="d-block btn btn-sm btn-success mb-2 w-100"><i class="fal fa-ruble-sign"></i> Оплатить </button>',
                'clear' => '<button wire:click="clearSelect" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-dark mb-2 w-100"><i class="fal fa-trash-alt"></i> Очистить</button>',
            ],
            'feature'       => [
                'getTotal' => true,
                'isDate'   => true
            ]
        ]);
    }

    public function service()
    {
        $this->messages->where('type', '=', 'new_message')->each(function ($item, $i) {
            $this->messages->forget($i);
        });

        $this->type = 'service';

        $this->messages->push([
            'avatar'        => asset('images/robot-chat.png'),
            'type'          => 'reply_action',
            'role'          => 'user',
            'username'      => 'Бот',
            'content'       => 'Платные услуги Доктора Жукова, которые будут полезны многим пациентам, вы сможете диагностировать ваше здоровье и показать результаты анализов для дальнейших рекомендаций.',
            'delay'         => 100,
            'type_material' => 'service',
            'created_at'    => now()->toDateTimeString(),
            'materials'     => (new ServiceRepository)->all(),
            'class'         => 'btn btn-sm btn-outline-secondary mb-2',
            'actions'       => [
                'other' => '<button wire:click="other" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-primary mb-2 w-100"><i class="fal fa-network-wired"></i> Выбрать другие материалы</button>',
                'pay'   => '<button wire:click="pay" wire:loading.attr="disabled" wire:loading.attr="disabled" class="d-block btn btn-sm btn-success mb-2 w-100"><i class="fal fa-ruble-sign"></i> Оплатить </button>',
                'clear' => '<button wire:click="clearSelect" wire:loading.attr="disabled" class="d-block btn btn-sm btn-outline-dark mb-2 w-100"><i class="fal fa-trash-alt"></i> Очистить</button>',
            ],
            'feature'       => [
                'getTotal' => true,
                'isDate'   => true
            ]
        ]);
    }

    public function other()
    {
        $this->menu();
    }

    public function clearSelect()
    {
        \Cart::clear();

        $this->total = 0;
    }

    public function add($model, $type)
    {
        $this->type = $type;

        $discount = $this->discount ? (new DiscountRepository())->findByCode($this->discount) : [];

        if ($type == 'video') {
            $product = (new VideoRepository())->findById($model);
        }

        if ($type == 'pdf') {
            $product = (new PdfRepository())->findById($model);
        }

        if ($type == 'webinar') {
            $product = (new WebinarRepository)->findById($model);
        }

        if ($type == 'telegram') {
            $product = (object)[
                'id'    => 1,
                'title' => 'Телеграм канал',
                'price' => 299,
            ];
        }

        if ($type == 'service') {
            $product = (new ServiceRepository)->findById($model);
        }

        if ($discount) {
            $price = $product->price - ($product->price * ($discount->percent / 100));
        } else {
            $price = $product->price;
        }

        if (\Cart::has($type . '_' . $model)) :
            \Cart::remove($type . '_' . $model);
        else:
            \Cart::add(
                $type . '_' . $product->id,
                $product->title,
                $price,
                1
            );
        endif;


        $this->total = \Cart::getTotal();
    }

    public function pay()
    {
        if (!\Cart::getTotal()) {

            $this->messages->each(function ($item, $i) {
                $this->messages->forget($i);
            });

            $this->messages->push([
                'avatar'     => asset('images/robot-chat.png'),
                'type'       => 'reply_action',
                'role'       => 'bot',
                'username'   => 'Бот',
                'content'    => 'Не удалось оплатить, причина в том что в вашей корзине нет добавленных товаров, пожалуйста выберите товары и попробуйте снова.',
                'delay'      => 100,
                'created_at' => now()->toDateTimeString(),
                'class'      => 'btn btn-sm btn-outline-primary mb-2',
                'actions'    => [
                    'other' => '<button wire:click="other" class="d-block btn btn-sm btn-outline-primary mb-2 w-100"><i class="fal fa-redo"></i> Повторить попытку</button>',
                ],
                'feature'    => [
                    'isDate' => false
                ]
            ]);
        }

        $this->payToPayment();

    }

    public function delete(int $id)
    {
        $this->messages->forget($id);
    }

    public function render()
    {
        return view('livewire.dashboard.customer.chat-buy');
    }

}
