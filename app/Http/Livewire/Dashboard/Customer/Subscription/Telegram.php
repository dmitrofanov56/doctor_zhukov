<?php

namespace App\Http\Livewire\Dashboard\Customer\Subscription;

use App\Models\Subscription;
use App\Repository\Telegram\SubscriptionRepository;
use Livewire\Component;

class Telegram extends Component
{
    public $contact;

    public $subscriptions;

    protected $listeners = ['render'];

    protected $rules = [
        'contact.email'    => 'required',
        'contact.telegram' => 'required',
    ];

    /**
     * Отписаться
     * @param Subscription $subscription
     * @return void
     */

    public function unsubscribe(Subscription $subscription)
    {
        $subscription->update(['status' => 'not_active']);

        $this->emit('render');
    }

    /**
     * Подписаться если остались дни
     * @param Subscription $subscription
     * @return void
     */

    public function reSubscribe(Subscription $subscription)
    {
        $subscription->update(['status' => 'active']);

        $this->emit('render');
    }

    public function render()
    {
        $this->subscriptions = auth()->user()->subscriptions;

        return view('livewire.dashboard.customer.subscription.telegram');
    }
}
