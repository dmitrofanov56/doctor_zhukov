<?php

namespace App\Http\Livewire\Admin;

use Artesaos\SEOTools\Traits\SEOTools;
use Livewire\Component;

class Index extends Component
{
    use SEOTools;

    public function render()
    {
        $this->seo()->setTitle('Панель управления');

        return view('livewire.admin.index')->extends('layouts.admin')->slot('content');
    }
}
