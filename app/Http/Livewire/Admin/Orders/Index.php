<?php

namespace App\Http\Livewire\Admin\Orders;

use App\Repository\OrderRepository;
use App\Repository\UserRepository;
use Artesaos\SEOTools\Traits\SEOTools;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use SEOTools, WithPagination;

    public $searchEmail;

    public $userId;

    protected $paginationTheme = 'bootstrap';

    public function updatedSearchEmail()
    {
        $find = (new UserRepository)->checkEmail($this->searchEmail);

        $this->userId = $find ? $find->id : null;

        $this->resetPage();
    }

    public function render()
    {
        $this->seo()->setTitle('Просмотр счетов');

        return view('livewire.admin.orders.index', [
            'orders' => (new OrderRepository)->getAll($this->userId),
        ])
            ->extends('layouts.admin')
            ->slot('content');
    }
}
