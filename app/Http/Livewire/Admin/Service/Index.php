<?php

namespace App\Http\Livewire\Admin\Service;

use App\Models\Service;
use App\Repository\ServiceRepository;
use Illuminate\Support\Collection;
use Livewire\Component;

class Index extends Component
{
    public bool $isNew = false;
    public bool $isEdit = false;

    public Collection $services;
    public Service $service;

    protected $rules = [
        'service.title'       => 'required',
        'service.description' => 'required|min:25',
        'service.type'        => 'required',
        'service.image'       => 'nullable',
        'service.price'       => 'required|numeric',
    ];

    protected $messages = [
        'service.title.required'       => 'Название услуги должно быть заполнено',

        'service.description.required' => 'Описание услуги должно быть заполнено',
        'service.description.min'      => 'Описание услуги должно быть не мение :min символов',

        'service.type.required'        => 'Тип услуги не выбрано',
        'service.price.required'       => 'Стоимость должна быть заполнена',
        'service.price.numeric'        => 'Стоимость должна быть числом',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function mount()
    {
        $this->services = (new ServiceRepository)->all();

        $this->service = new Service([
            'type' => 'call'
        ]);
    }

    public function deleteService(Service $service)
    {
        $service->delete();

        $this->services = (new ServiceRepository)->all();
    }

    public function addService()
    {
        $this->validate();

        $this->service->save();

        $this->services = (new ServiceRepository)->all();
    }

    public function render()
    {
        return view('livewire.admin.service.index')->extends('layouts.admin')->slot('content');
    }
}
