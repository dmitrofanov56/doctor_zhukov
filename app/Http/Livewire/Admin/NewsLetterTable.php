<?php

namespace App\Http\Livewire\Admin;

use App\Jobs\NewsLetter\SendToUserNotify;
use App\Mail\Admin\NewsLetter\TemplateSend;
use App\Models\User;
use App\Repository\UserRepository;
use Artesaos\SEOTools\Traits\SEOTools;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

use App\Models\NewsLetter as ModelMail;


class NewsLetterTable extends Component
{
    use SEOTools;

    public string $editSubject;
    public string $editMessage;
    public bool $isEditModal = false;

    public $templates;

    public bool $is_create = false;

    public function changeStatus(ModelMail $news_letter, $status)
    {
        $news_letter->update(['active' => $status]);
    }

    public function edit(ModelMail $news_letter)
    {
        $this->isEditModal = true;
        $this->editMessage = $news_letter->message;
        $this->editSubject = $news_letter->subject;
    }

    public function update(ModelMail $news_letter)
    {
        (new \App\Repository\NewsLetter())->updateByFind(
            $news_letter,
            $this->editSubject,
            $this->editMessage
        );

        $this->isEditModal = false;

        session()->flash('success' , 'Шаблон обновлён');
    }

    public function delete(ModelMail $news_letter)
    {
        $news_letter->delete();

        return redirect()->route('newsletter')->with('success' , 'Шаблон удалён');
    }

    public function send(ModelMail $news_letter)
    {
        $users = (new UserRepository())->all();

        foreach ($users as $user) {
            Mail::to($user)->send(new TemplateSend($news_letter));
        }
    }

    public function render()
    {
        $this->seo()->setTitle('Шаблоны рассылок');

        $this->templates = (new \App\Repository\NewsLetter())->getAll();

        return view('livewire.admin.news-letter.table')
            ->extends('layouts.admin');
    }
}
