<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class NewsLetter extends Component
{
    public $subject;

    public $message;

    public $changeStatus;

    protected $rules = [
        'subject' => 'required|string|min:6',
        'message' => 'required',
    ];

    public function updated()
    {
        $this->dispatchBrowserEvent('ckeditor');
    }

    public function add()
    {
        $this->validate();

        (new \App\Repository\NewsLetter())->create(
            $this->subject,
            $this->message
        );

        return redirect()->route('newsletter')->with('success' , 'Шаблон создан');
    }

    public function render()
    {
        return view('livewire.admin.news-letter.index')
            ->extends('layouts.admin');
    }
}
