<?php

namespace App\Http\Livewire\Admin\Webinar;

use App\Models\Order;
use App\Repository\ItemOrderRepository;
use App\Repository\OrderRepository;
use App\Repository\WebinarRepository;
use Illuminate\Support\Collection;
use Livewire\Component;

class Index extends Component
{
    public Collection $webinars;

    public bool $isNewWebinar = false;

    public array $new = [];

    protected $listeners = ['update', 'render'];

    public function addNew()
    {
        $validate = $this->validate([
            'new.title'         => 'required',
            'new.price'         => 'required|numeric',
            'new.preview_video' => 'required',
            'new.preview_image' => 'required',
            'new.live_url'      => 'required',
            'new.description'   => 'required',
            'new.start_date'    => 'required',
        ]);

        (new WebinarRepository)->create($validate['new']);

        $this->emit('update');
    }

    public function render()
    {
        $this->webinars = (new WebinarRepository)->getAll();

        $totalPayMonth = (new ItemOrderRepository)->subscribesWebinarsMonth()->sum('price');
        $payMonth      = (new ItemOrderRepository)->subscribesWebinarsMonth();

        return view('livewire.admin.webinar.index', compact('totalPayMonth' , 'payMonth'))->extends('layouts.admin')->slot('content');
    }
}
