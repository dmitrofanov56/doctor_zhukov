<?php

namespace App\Http\Livewire\Admin\Users;

use App\Models\Subscription;
use App\Models\User;
use App\Repository\UserRepository;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Support\Collection;
use Livewire\Component;

class Index extends Component
{
    use SEOTools;

    public $search = '';

    public function unsub(User $user)
    {
        $user->subscriptions()->update([
            'status'     => 'not_active'
        ]);
    }

    public function subOn(User $user)
    {
        if (! $user->telegram_id) return false;

        $user->subscription->update([
            'status' => 'active'
        ]);
    }

    public function render()
    {
        $this->seo()->setTitle('Пользователи');

        $users = (new UserRepository)->getAllWithPagination($this->search);

        return view('livewire.admin.users.index', compact('users'))
            ->extends('layouts.admin')
            ->slot('content');
    }
}
