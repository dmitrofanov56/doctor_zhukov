<?php

namespace App\Http\Livewire\Admin\Users;

use App\Models\User;
use Artesaos\SEOTools\Traits\SEOTools;
use Livewire\Component;

class Show extends Component
{
    use SEOTools;

    public User $user;

    public function render()
    {
        $this->seo()->setTitle('Просмотр пользователя - '  . $this->user->email);

        return view('livewire.admin.users.show')
            ->extends('layouts.admin')
            ->slot('content');
    }
}
