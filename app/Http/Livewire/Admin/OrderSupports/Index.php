<?php

namespace App\Http\Livewire\Admin\OrderSupports;

use App\Models\SupportProject;
use Artesaos\SEOTools\Traits\SEOTools;
use Livewire\Component;

class Index extends Component
{
    use SEOTools;

    public function render()
    {
        $this->seo()->setTitle('Поддержка проекта');

        $supports = SupportProject::orderByDesc('created_at')->paginate(10);

        return view('livewire.admin.order-supports.index', compact('supports'))
            ->extends('layouts.admin')
            ->slot('content');
    }
}
