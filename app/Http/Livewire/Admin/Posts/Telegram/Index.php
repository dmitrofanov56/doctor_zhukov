<?php

namespace App\Http\Livewire\Admin\Posts\Telegram;

use App\Http\Livewire\Shared\FlashMessage;
use App\Models\Post;
use App\Repository\PostsRepository;
use BotMan\BotMan\Facades\BotMan;
use BotMan\BotMan\Messages\Attachments\Image;
use Illuminate\Support\Collection;
use Livewire\Component;

class Index extends Component
{
    public Collection $posts;

    public Post $post;

    public $newPost;

    public bool $isEdit = false;
    public bool $isNew = false;
    public $chat_id;

    protected $rules = [

        'post.title'   => 'required',
        'post.content' => 'required',

        'newPost.title'   => 'required',
        'newPost.content' => 'required',

        'chat_id' => 'required'

    ];

    protected $listeners = ['updatePosts' => 'render'];

    public function setEditPost(Post $post)
    {
        $this->isEdit = true;
        $this->isNew = false;

        $this->post = $post;
    }

    public function setNewPost()
    {
        $this->isEdit = false;
        $this->isNew = true;
    }

    /**
     * Обновить пост
     * @return void
     */

    public function save()
    {
        $this->post->update();


        $this->isEdit = false;
    }

    /**
     * Добавить новый пост
     * @return void
     */

    public function newPostSave()
    {
        (new PostsRepository)->newPost($this->newPost);

        $this->isNew = false;

        $this->posts = (new PostsRepository)->getPosts();
    }

    /**
     * Удалить пост
     * @param Post $post
     * @return void
     */

    public function deletePost(Post $post)
    {
        $post->delete();

        $this->posts = (new PostsRepository)->getPosts();
    }

    public function sendPostWithTelegram(Post $post)
    {
        if (is_null($post->date_send_telegram) || $post->leftPostHas()) :
            $format = str_ireplace('&nbsp;', '', $post->content);
            $format = strip_tags($format, ['<strong>']);
            BotMan::say("{$format}", $this->chat_id[$post->id], \BotMan\Drivers\Telegram\TelegramDriver::class, [
                'parse_mode'               => 'html',
                'disable_web_page_preview' => true
            ]);
            return $post->update(['date_send_telegram' => now()]);
        else:
            $this->emit('flash', [
                'type'    => FlashMessage::DANGER,
                'message' => 'С момента последней публикации не прошло 24 часа, публиковать повторно можно спустя сутки.'
            ]);
            return redirect()->back();
        endif;
    }

    public function mount()
    {
        $this->chats = config('medschool.telegram_channels');

        $this->posts = (new PostsRepository)->getPosts();
    }

    public function render()
    {
        return view('livewire.admin.posts.telegram.index')->extends('layouts.admin')->slot('content');
    }
}
