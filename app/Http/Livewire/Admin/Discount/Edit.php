<?php

namespace App\Http\Livewire\Admin\Discount;

use App\Http\Livewire\Shared\FlashMessage;
use App\Models\Discount;
use Artesaos\SEOTools\Traits\SEOTools;
use Livewire\Component;

class Edit extends Component
{
    use SEOTools;

    public Discount $discount;

    protected $rules = [
        'discount.code'       => 'required',
        'discount.used_count' => 'required',
        'discount.percent'    => 'required',
    ];

    public function update()
    {
        $this->discount->update();

        $this->emit('flash', [
            'type'    => FlashMessage::SUCCESS,
            'message' => 'Промокод обновлён!'
        ]);
    }

    public function render()
    {
        $this->seo()->setTitle('Промокоды');

        return view('livewire.admin.discount.edit')
            ->extends('layouts.admin');
    }
}
