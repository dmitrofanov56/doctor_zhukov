<?php

namespace App\Http\Livewire\Admin\Discount;

use Livewire\Component;

class Create extends Component
{
    public function render()
    {
        return view('livewire.admin.discount.create');
    }
}
