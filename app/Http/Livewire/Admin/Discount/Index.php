<?php

namespace App\Http\Livewire\Admin\Discount;

use App\Repository\DiscountRepository;
use Artesaos\SEOTools\Traits\SEOTools;
use Livewire\Component;

class Index extends Component
{
    use SEOTools;

    public function render()
    {
        $this->seo()->setTitle('Промокоды');

        $discounts = app(DiscountRepository::class)->getAll();

        return view('livewire.admin.discount.index', compact('discounts'))
            ->extends('layouts.admin')
            ->slot('content');
    }
}
