<?php

namespace App\Http\Livewire\Admin\Products;

use App\Repository\PdfRepository;
use App\Repository\VideoRepository;
use Illuminate\Http\RedirectResponse;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use WithFileUploads;

    public string $type = 'video';

    public $product;

    protected $rules = [
        'product.title' => 'required',
        'product.description' => 'required',
        'product.pdfUrl'   => 'nullable',
        'product.videoUrl' => 'nullable',
        'product.price' => 'required|numeric',
    ];


    /**
     * @return RedirectResponse
     */

    public function addProduct() : RedirectResponse
    {
        $this->validate();

        // Добавление ВИДЕО

        if ($this->type == 'video') {

            $product =  collect($this->product)->toArray();

        }

        // Добавление PDF

        if ($this->type == 'pdf') {

            $file = isset($this->product['pdfUrl']) ? $this->product['pdfUrl']->store('pdf') : null;

           $product =  collect($this->product)->except(['pdfUrl'])->merge([
               'pdfUrl' => $file
           ])->toArray();

           $hasPdfCreate = (new  PdfRepository)->store($product);

           if ($hasPdfCreate) {
               session()->flash('success' , 'Добавлен новый PDF');
           }
        }

        return redirect()->back();
    }

    public function render()
    {
        return view('livewire.admin.products.create')
            ->extends('layouts.admin')
            ->slot('content');
    }
}
