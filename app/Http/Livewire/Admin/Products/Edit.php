<?php

namespace App\Http\Livewire\Admin\Products;

use App\Models\Pdf;
use App\Models\Video;
use Artesaos\SEOTools\Traits\SEOTools;
use Livewire\Component;

class Edit extends Component
{
    use SEOTools;

    public Video $video;
    public Pdf $pdf;

    public function render()
    {
        $this->seo()->setTitle('Редактировать продукт');

        return view('livewire.admin.products.edit')
            ->extends('layouts.admin')
            ->slot('content');
    }
}
