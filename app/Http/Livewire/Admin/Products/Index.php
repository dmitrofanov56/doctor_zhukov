<?php

namespace App\Http\Livewire\Admin\Products;

use App\Repository\PdfRepository;
use App\Repository\VideoRepository;
use Artesaos\SEOTools\Traits\SEOTools;
use Livewire\Component;

class Index extends Component
{
    use SEOTools;

    public function render()
    {
        $this->seo()->setTitle('Продукция');

        $pdfs = (new PdfRepository)->getAll();

        $videos = (new VideoRepository)->getAll();

        return view('livewire.admin.products.index', compact('pdfs' , 'videos'))
            ->extends('layouts.admin')
            ->slot('content');
    }
}
