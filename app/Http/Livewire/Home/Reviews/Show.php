<?php

namespace App\Http\Livewire\Home\Reviews;

use Livewire\Component;

class Show extends Component
{
    public string $type = 'no';

    public function type($type)
    {
        $this->type = $type;
    }

    public function render()
    {
        return view('livewire.home.reviews.show');
    }
}
