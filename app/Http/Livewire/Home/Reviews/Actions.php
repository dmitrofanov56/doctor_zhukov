<?php

namespace App\Http\Livewire\Home\Reviews;

use Livewire\Component;

class Actions extends Component
{
    public $isModal = false;

    public function add()
    {
        $this->isModal = true;
    }

    public function render()
    {
        return view('livewire.home.reviews.actions');
    }
}
