<?php

namespace App\Http\Livewire\Home\Telegram;

use App\Notifications\Customer\Invoice;
use App\Repository\OrderRepository;
use Livewire\Component;

class Buy extends Component
{
    public $isFormActive = false;

    public $amount = 1500;
    public $joinUrl = 'https://t.me/joinchat/aJb7cqj9YME0Y2U6';

    public function pay()
    {
        $order = (new OrderRepository)->create('Yoo');

        $telegram = $order->telegram()->create([
            'join_url' => $this->joinUrl,
            'sum_pay'  => $this->amount
        ]);

        $order->items()->create([
            'telegram_access_id' => $telegram->id,
            'type'               => 'telegram',
            'price'              => $telegram->sum_pay,
        ]);

        $payUrl = $order->payYooCustom('Подписка на закрытую группу телеграм', 1500, [
            'telegramAccess' => true
        ]);

        auth()->user()->notify(new Invoice($order));

        return redirect($payUrl);

    }

    public function render()
    {
        $isAuth =  auth()->user();

        return view('livewire.home.telegram.buy', compact('isAuth'));
    }
}
