<?php

namespace App\Http\Livewire\Home\Pages\Blog;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.home.pages.blog.index');
    }
}
