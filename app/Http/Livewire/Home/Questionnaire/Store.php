<?php

namespace App\Http\Livewire\Home\Questionnaire;

use App\Mail\Home\Questionnaire;
use App\Models\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;
use Livewire\WithFileUploads;

class Store extends Component
{

    use WithFileUploads;

    public $fio;
    public $age;
    public $sex    = 'Женский';
    public $complaints;
    public $diagnoses;
    public $tablets;
    public $allergy;
    public $period = 3;
    public $file;
    public $isAuth = false;

    protected $rules = [
        'fio'  => 'required',
        'age'  => 'required|numeric',
        'sex'  => 'required',
        'file' => 'required',
    ];

    protected $messages = [
        'fio.required' => 'Ф.И.О не указано',

        'age.required' => 'Возраст не указан',
        'age.numeric'  => 'Возраст должен быть числом',

        'sex.required' => 'Пол не указан',

    ];

    public function mount()
    {
        if (auth()->user())
            $this->isAuth = true;
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function save()
    {
        $validatedData = $this->validate();

        Mail::to(env('MAIL_ADMIN'))->send(new Questionnaire($this));

        session()->flash('success', 'Анкета успешно отправлена.');

        $this->reset('fio', 'age', 'sex' , 'complaints', 'diagnoses', 'tablets', 'allergy', 'file',  'period');

    }

    public function render()
    {
        return view('livewire.home.questionnaire.store');
    }

}
