<?php

namespace App\Http\Livewire\Home\Products;

use App\Repository\DiscountRepository;
use App\Repository\PdfRepository;
use App\Repository\VideoRepository;
use App\Repository\WebinarRepository;
use Livewire\Component;

class ListTable extends Component
{
    public $videos;
    public $pdfs;
    public $discount;

    public $cart;

    public function mount()
    {
        $this->videos = (new VideoRepository)->getAll();
        $this->pdfs   = (new PdfRepository)->getAll();
        $this->discount = session()->has('discount') ? session('discount') : null;
    }

    public function updatedDiscount($value)
    {
        $discount = (new DiscountRepository())->findByCode($value);

        if ($discount && $discount->used_count > 0) {
            session()->put('discount', $value);
        }
    }

    public function render()
    {
        $this->cart = \Cart::getContent();

        return view('livewire.home.products.list-table');
    }

    public function addToCart($type, $id)
    {
        $discount = $this->discount ? (new DiscountRepository())->findByCode($this->discount) : null;

        if ($type == 'video') {
            $product = (new VideoRepository())->findById($id);
        }

        if ($type == 'pdf') {
            $product = (new PdfRepository())->findById($id);
        }

        if ($type == 'webinar') {
            $product = (new WebinarRepository)->findById($id);
        }

        if ($discount && $discount->used_count > 0) {
           $price =  $product->price - ($product->price * ($discount->percent / 100));
        } else {
            $price = $product->price;
        }

        if (\Cart::has($type . '_' . $product->id)) :
            \Cart::remove($type . '_' . $product->id);
        else:
            \Cart::add(
                $type . '_' . $product->id,
                $product->title,
                $price,
                1
            );
        endif;

        $this->emit('cart_updated');

        $this->emit('reloadPayAction');
    }

}
