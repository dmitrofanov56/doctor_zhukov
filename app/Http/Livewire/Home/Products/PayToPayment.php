<?php

namespace App\Http\Livewire\Home\Products;

use App\Repository\OrderRepository;
use App\Repository\UserRepository;
use App\Traits\Order;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class PayToPayment extends Component
{

    use Order;

    protected $listeners = ['reloadPayAction' => 'render'];

    // Данные для авторизации

    public $account;

    # Тип оплаты

    public $typePay = 1;

    # Если гость, то просим указать Email и телефон

    public $guest;

    public $payBtn = false;

    protected $messages = [
        'guest.email.required' => 'Вы не ввели почту',
        'guest.email.email'    => 'Не верный формат почты',
        'guest.email.unique'   => 'Данная почта уже занята другим пользователем',
        'guest.phone.required' => 'Вы не ввели номер телефона',
        'guest.email.exists'   => 'Данная почта уже занята'
    ];

    protected $validationAttributes = [
        'account.email'    => 'Почта',
        'account.password' => 'Пароль',
    ];

    /**
     * Авторизовать аккаунт
     */

    public function setAuth()
    {
        $validatedData = $this->validate([
            'account.email'    => 'required',
            'account.password' => 'required',
        ]);

        $email = (new UserRepository())->checkEmail($validatedData['account']['email']);

        if ($email)
        {
            if (Auth::attempt($validatedData['account']))
            {
                $this->emitSelf('reload');
            }
        }
    }


    public function pay()
    {
        if (!\Cart::getTotal() > 0) :
            session()->flash('error', 'В вашей корзине нет товаров, добавьте в корзину товары и попробуйте снова.');
        else:
            if ($this->typePay && auth()->id()) :
                return $this->payToPayment();
            elseif($this->typePay == 0) :

                $validated = $this->validate([
                    'guest.email' => 'required|unique:users,email',
                    'guest.phone' => 'required'
                ]);

                $user = (new UserRepository())->createForEmail(
                    $validated['guest']['email'],  $validated['guest']['phone']
                );

                Auth::login($user, true);

                return $this->payToPayment();
            endif;
        endif;

        return false;
    }

    public function render()
    {
        $cart_count = \Cart::getTotal();

        $isOrderIsset = (new OrderRepository)->existsForUser();

        return view('livewire.home.products.pay-to-payment', compact('cart_count', 'isOrderIsset'));
    }

}
