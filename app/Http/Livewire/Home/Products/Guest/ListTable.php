<?php

namespace App\Http\Livewire\Home\Products\Guest;

use Livewire\Component;

class ListTable extends Component
{
    public function render()
    {
        return view('livewire.home.products.guest.list-table');
    }
}
