<?php

namespace App\Http\Livewire\Home\Products;

use Livewire\Component;

class CardCounter extends Component
{
    protected $listeners = ['cart_updated' => 'render'];

    public function render()
    {
        $cart_count = \Cart::getTotal();

        return view('livewire.home.products.card-counter', compact('cart_count'));
    }
}
