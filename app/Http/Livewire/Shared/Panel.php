<?php

namespace App\Http\Livewire\Shared;

use Artesaos\SEOTools\Traits\SEOTools;
use Livewire\Component;

class Panel extends Component
{

    use SEOTools;

    public $title;
    public $description;

    public function mount()
    {
        $this->title       = $this->seo()->metatags()->getTitleSession();
        $this->description = $this->seo()->metatags()->getDescription();
    }

    public function render()
    {
        return view('__shared.dashboard.panel');
    }

}
