<?php

namespace App\Http\Livewire\Shared;

use Livewire\Component;

class FlashMessage extends Component
{
    protected $listeners = ['flash' => 'message'];

    public string $type     = 'success';
    public string $message = 'success';
    public bool $isMessage = false;

    const DANGER = 'danger';
    const SUCCESS = 'success';

    public function message($data)
    {
        $this->isMessage = true;
        $this->type    = $data['type'];
        $this->message = $data['message'];
    }

    public function render()
    {
        return view('livewire.shared.flash-message');
    }
}
