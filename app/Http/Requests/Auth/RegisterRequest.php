<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{

    /**
     * @return bool
     */

    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */

    public function rules(): array
    {
        return [
            'username'              => 'required',
            'phone'                 => 'required',
            'email'                 => 'required|string|email|unique:users',
            'password'              => 'required|string',
            'password_confirmation' => 'required|same:password',
        ];
    }

    /**
     * @return array
     */

    public function messages(): array
    {
        return [
            'username.required'              => 'Имя пользователя не может быть пустым',
            'phone.required'                 => 'Номер телефона не может быть пустым',
            'email.required'                 => 'Почта не может быть пустая',
            'email.unique'                   => 'Эта почта занята другим пользователем',
            'password.required'              => 'Пароль не может быть пустым',
            'password_confirmation.required' => 'Повторный пароль не заполнен',
        ];
    }

}
