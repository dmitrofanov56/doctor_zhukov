<?php

namespace App\Http\Requests\Home;

use App\Repository\DiscountRepository;
use Illuminate\Foundation\Http\FormRequest;

class CreatePayProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'discount' => ['nullable' , 'string']
        ];
    }

    public function discount()
    {
        if ($this->isEmptyString('discount')) return null;

        $discount = app(DiscountRepository::class)->findByCode($this->input('discount'));

        return collect($discount)->only(['id', 'percent' , 'used_count'])->toArray();
    }
}
