<?php

namespace App\Http\Requests\Home\Finance;

use Illuminate\Foundation\Http\FormRequest;

class SupportRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => ['required'],
            'email'        => ['required'],
            'text_comment' => ['nullable'],
            'sum_pay'      => ['required', 'min:1'],
        ];
    }

    public function messages()
    {
        return [
            'name.required'    => 'Имя не указано',
            'email.required'   => 'Почта не указана',
            'sum_pay.required' => 'Сумма пополнения не указана',
        ];
    }

}
