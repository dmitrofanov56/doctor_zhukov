<?php

namespace App\Http\Requests\Admin\Video;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => ['required'],
            'description' => ['nullable'],
            'videoUrl'    => ['required'],
            'price'       => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'title.required'    => 'Название видео не заполнено',
            'videoUrl.required' => 'Ссылка на видео не заполнено',
            'price.required'    => 'Стоимость не указана',
        ];
    }

    public function title() : string
    {
        return (string) $this->input('title');
    }

    public function description() : string
    {
        return (string) $this->input('description');
    }

    public function videoUrl() : string
    {
        return (string) $this->input('videoUrl');
    }

    public function price() : int
    {
        return (int) $this->input('price');
    }
}
