<?php

namespace App\Http\Requests\Admin\Pdf;

use App\Models\Pdf;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;

class UpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => ['required'],
            'description' => ['nullable'],
            'pdfUrl'      => ['nullable'],
            'price'       => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'title.required'  => 'Название видео не заполнено',
            'pdfUrl.required' => 'Файл не выбран',
            'price.required'  => 'Стоимость не указана',
        ];
    }

    public function title(): string
    {
        return (string) $this->input('title');
    }

    public function description(): string
    {
        return (string) $this->input('description');
    }

    public function pdfUrl(Pdf $pdf): string
    {
        $pdfUrl = $pdf->pdfUrl;

        if ($this->hasFile('pdfUrl')) :
            Storage::disk('public')->delete($pdf->pdfUrl);
            $pdfUrl = $this->file('pdfUrl')->store('pdf', 'public');
        endif;

        return (string) $pdfUrl;
    }

    public function price(): int
    {
        return (int) $this->input('price');
    }

}
