<?php

namespace App\Http\Middleware\Home\Discount;

use App\Repository\DiscountRepository;
use Closure;
use Illuminate\Http\Request;

class WithCode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // Если есть промокод в строке

        if ($request->has('withCode'))
        {
            $discount = (new DiscountRepository())->findByCode($request->query('withCode'));

            if ($discount ) {
                session()->put('discount' , $request->query('withCode'));
            }

            return redirect($request->url());
        }

        // Обнуление промокода если кол-во активаций исчерпано

        if (session()->has('discount'))
        {
            $discount = (new DiscountRepository())->findByCode(session('discount'));

            if (! $discount) {
                session()->forget('discount');
                session()->reflash();
            }
        }

        return $next($request);
    }
}
