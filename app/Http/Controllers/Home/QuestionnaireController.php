<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\View\View;

class QuestionnaireController extends Controller
{

    /**
     * Страница заполнение анкеты
     * @return View
     */

    public function index() : View
    {
        $this->seo()
             ->setTitle('Получить рекомендации кардиолога онлайн WhatsApp')
             ->setDescription('Получить рекомендации кардиолога на долгосрочный период по снижению (холестерина, лечению гипертонии, или любого другого заболевания), на долгосрочной основе от 3 до 6 месяцев ведения пациента');

        return view('home.pages.questionnaire');
    }
}
