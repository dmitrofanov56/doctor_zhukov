<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Repository\WebinarRepository;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Http\Request;
use Illuminate\View\View;

class WebinarController extends Controller
{
    /**
     * Список вебинаров
     * @param Request $request
     *
     * @return View
     */

    public function webinarsList(Request $request) : View
    {
        $this->seo()
             ->setTitle('Вебинары и доступные уроки про лечение гипертонии')
             ->setDescription('Всегда доступные вебинары и видео уроки на любую тему по вашему здоровью, вы узнаете как правильно лечить и избавиться от холестерина, гипертонии, ИБС сердца, снизить давление, и сможете задавать свои вопросы Доктору Жукову в прямом эфире, ваш вопрос 100% будет замечен.');

        $webinars = (new WebinarRepository)->getAll();

        return view('home.webinar.index', compact('webinars'));
    }
}
