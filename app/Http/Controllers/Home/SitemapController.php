<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Laravelium\Sitemap\Sitemap;

class SitemapController extends Controller
{
    protected $sitemap;

    public function __construct()
    {
        $this->sitemap = app()->make('sitemap');
    }

    public function generate()
    {
        $this->sitemap->setCache('laravel.sitemap', 3600);

        if (! $this->sitemap->isCached() ) {

            $images = [
                ['url' => URL::to('assets/images/dc.jpeg'), 'title' => 'Доктор жуков', 'caption' => 'Купить обучающие видео уроки для улучшения своего здоровья', 'geo_location' => 'Russia, Cherepovets']
            ];

            $this->sitemap->add(route('home'), now()->subMonth()->toW3cString(), '1.0', 'daily', $images);
            $this->sitemap->add(route('customer') , now()->subMonth()->toW3cString(), '0.1', 'monthly');

            $this->sitemap->add(route('page.questionnaire') , now()->subMonth()->toW3cString(), '1.0', 'daily');
            $this->sitemap->add(route('finance.support') , now()->subMonth()->toW3cString(), '0.1', 'monthly');
            $this->sitemap->add(route('home.webinars') , now()->subMonth()->toW3cString(), '1.0', 'daily');
            $this->sitemap->add(route('home.posts') , now()->subMonth()->toW3cString(), '1.0', 'daily');
        }

        $this->sitemap->store('xml');
    }
}
