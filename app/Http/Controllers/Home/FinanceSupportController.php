<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Http\Requests\Home\Finance\SupportRequest;

use App\Notifications\Customer\Invoice;
use App\Repository\OrderRepository;

use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\View\View;

class FinanceSupportController extends Controller
{
    use SEOTools;

    public function index() : View
    {
        $this->seo()
             ->setTitle('Финансовая поддержка')
             ->setDescription('Если у вас есть желание помочь в развитии моего проекта, вы всегда можете пожертвовать любую сумму, данная сумма пойдёт на развитие и улучшение проекта, я стараюсь для вас, Уважаемые друзья!');

        return view('home.supportFin');
    }

    public function store(SupportRequest $request)
    {
        if (! auth()->user()) {
            return response()->json([
                'notAuth' => true,
            ], 403);
        }

        $order = (new OrderRepository)->create('Yoo');

        $support = $order->support()->create($request->all());

        $order->items()->create([
            'support_project_id' => $support->id,
            'type'               => 'support',
            'price'              => $support->sum_pay,
        ]);

        auth()->user()->notify(new Invoice($order));

        return $order->supportPay();
    }

}
