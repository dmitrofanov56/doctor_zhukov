<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Notifications\Customer\Order\BuyDone;
use App\Repository\OrderRepository;
use App\Repository\PostsRepository;
use Illuminate\View\View;

class HomeController extends Controller
{
    public function index(): View
    {
       // $order = (new OrderRepository)->orderFindTransaction('f4e9bdd9-b9f1-4e41-a082-0a534ad8462e');

      //  $order->user->notify(new BuyDone($order));

        $this->seo()
             ->metatags()
             ->setTitleDefault('Сергей Юрьевич Жуков')
             ->setTitle('Получить рекомендации кардиолога')
             ->setDescription('Вам необходима скорейшая консультация онлайн кардиолога? Сергей Юрьевич Жуков, готов вас принять онлайн и дать рекомендации по вашему здоровью, доступны авторские курсы в личном кабинете.');

        $this->seo()
            ->opengraph()
            ->setType('Profile')
            ->setUrl(route('home'))
            ->setTitle("Рекомендации кардиолога по ватсап")
            ->setSiteName('Medschool35')
            ->setDescription('Кардиолог онлайн, рекомендации пациентам, а так же продажа авторского материала по лечению гипертонии и снижению холестерина, обучайтесь по курсам и сохраните ваше здоровье')
            ->setType('Profile')
            ->setProfile([
                'first_name' => 'Сергей',
                'last_name' => 'Юрьевич',
                'username' => 'Жуков',
                'gender' => 'male'
            ])
            ->addImage(asset('assets/images/dc.jpeg') , ['height' => 150, 'width' => 150 , 'alt' => 'Сергей Юрьевич Жуков' , 'type' => 'image/jpeg' , 'secure_url' => asset('assets/images/dc.jpeg')])
            ->addImage(asset('images/snippets/images/products.png') , ['height' => 150, 'width' => 150 , 'alt' => 'Сергей Юрьевич Жуков' , 'type' => 'image/jpeg' , 'secure_url' => asset('images/snippets/images/products.png')]);


        return view('home.index');
    }

    /**
     * Акции и предложения
     * @return View
     */

    public function posts(): View
    {
        $this->seo()
             ->setTitle('Скидки и акции')
             ->setDescription('Вы всегда сможете ознакомиться с предстоящими акциями и предложениями в школе пациента, и приобрести любой обучающий материал по выгодной и низкой цене.');

        $posts = (new PostsRepository())->getPosts();

        return view('home.posts', compact('posts'));
    }

    /**
     * Страница с материалами
     * @return View
     */

    public function products() : View
    {
        $this->seo()->setTitle('Как избавиться от гипертонии купить курс видео уроков')
                    ->setDescription('Вы хотите снизить высокое артериальное давление? но в то же время не хотите себе навредить, вы всегда сможете купить мои авторские методички и видео курсы, и узнать как это можно сделать безопасно и какие лекарства можно принимать.');

        return view('home.products');
    }
}
