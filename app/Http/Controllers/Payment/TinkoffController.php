<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Repository\OrderRepository;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TinkoffController extends Controller
{

    public function handle(Request $request)
    {
        $order      = (new OrderRepository)
            ->orderFindTransaction($request->post('OrderId'));

        $order->classPay->checkPay($order, $request);

        echo "OK";
    }

    /**
     * Страница успешного платежа
     * @param Request $request
     *
     * @return View
     */

    public function success(Request $request) : View
    {
        $order = (new OrderRepository)
                 ->orderFindTransaction($request->get('OrderId'));

        return view('customer.payment.success', compact('order'));
    }

    /**
     * Страница неуспешного платежа
     * @return View
     */

    public function error() : View
    {
        return view('customer.payment.error');
    }

}
