<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Notifications\Customer\Order\BuyDone;
use App\Repository\OrderRepository;
use App\Repository\UserRepository;
use App\Repository\WebinarRepository;
use App\Traits\YooKassaGateway;
use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;


class YooController extends Controller
{
    use YooKassaGateway;

    public function handle(Request $request)
    {
        if ($request->post('object')['status'] !== 'succeeded')  {
            return false;
        }

        // Подписка

        if (! empty($request->post()['object']['metadata']['telegram_subs']) ) {

            $user = (new UserRepository)->checkEmail($request->post()['object']['metadata']['email']);

            $user->subscriptions()->updateOrCreate(['user_id' => $user->id], [
                'amount'            => $request->post()['object']['metadata']['amount'],
                'days'              => $request->post()['object']['metadata']['period'],
                'expired_at'        => now()->addDays($request->post()['object']['metadata']['period']),
                'options'           => $request->post()['object']['payment_method'],
                'payment_method_id' => $request->post()['object']['payment_method']['id'],
                'status'            => 'active',
            ]);

            $user->sendSubscription();
        }

        elseif (! empty($request->post()['object']['metadata']['webinar']) ) {

            // Получаем вебинар

            $webinar = (new WebinarRepository)->findById($request->post()['object']['metadata']['webinar']);

            // Получаем пользователя

            $user = (new UserRepository)->checkEmail($request->post()['object']['metadata']['email']);

            // Авторизовываем

            Auth::login($user);

            // Создаем счет

            $order = (new OrderRepository)->create('Yoo');

            // Добавляем купленный продукт

            $order->items()->create([
                'webinar_id' => $webinar->id,
                'price'      => $webinar->price,
                'type'       => 'webinar',
            ]);

            $order->update(['status' => 1]);

            BotMan::say("Вы приобрели доступ на вебинар {$webinar->title} ссылка доступа {$webinar->live_url}, спасибо что доверяете мне."  , $user->telegram_id , TelegramDriver::class);
        }
        elseif (! empty($request->post()['object']['metadata']['transaction_id']) ) {

            $transactionId = $request->post()['object']['metadata']['transaction_id'];

            try {
                $order = (new OrderRepository)->orderFindTransaction($transactionId);

                $order->update(['status' => 1]);

                $order->user->notify(new BuyDone($order));

                if ($order->discount && $order->discount->used_count > 0) :
                    $order->discount->decrement('used_count');
                endif;

            } catch (\Exception $exception) {}
        }

        return true;
    }
}
