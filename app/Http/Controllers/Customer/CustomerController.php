<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Order;

use App\Repository\ItemOrderRepository;
use App\Repository\OrderRepository;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class CustomerController extends Controller
{

    /**
     * Кабинет клиента
     * @return View
     */

    public function index(): View
    {
        return view('customer.index');
    }

    /**
     * Вывод видео материалов
     * @return View
     */

    public function videos(): View
    {
        $videos = (new ItemOrderRepository)->videosForUser();

        return view('customer.videos.index', compact('videos'));
    }

    /**
     * Вывод PDF материалов
     * @return View
     */

    public function pdfs() : View
    {
        $pdfs = (new ItemOrderRepository)->pdfsForUser();

        return view('customer.pdfs.index', compact('pdfs'));
    }

    /**
     * Вывод заказов клиента
     * @return View
     */

    public function orders() : View
    {
        $orders = (new OrderRepository)->ordersByUser('desc');

        return view('customer.orders.index', compact('orders'));
    }

    /**
     * Оплата заказа
     * @param Order $order
     * @return RedirectResponse
     */

    public function pay(Order $order) : RedirectResponse
    {
        if ($order->status == 0) :
            return redirect($order->payYoo());
        else:
            return redirect()->back();
        endif;
    }

    /**
     * Отмена оплаты счёта
     * @param Order $order
     * @return RedirectResponse
     */

    public function cancelOrder(Order $order) : RedirectResponse
    {
        (new OrderRepository)->setCancelOrderStatus($order->order);

        return redirect()->route('customer');
    }

    /**
     * Доступ к телеграм каналу
     * @return View
     */

    public function tgAccess() : View
    {
        $myAccess = (new ItemOrderRepository)->tgForUser();

        return view('customer.orders.subscribeTelegram', compact('myAccess'));
    }

    /**
     * Купленные услуги
     * @return View
     */

    public function myService() : View
    {
        $orders = (new ItemOrderRepository)->userService();

        return view('customer.orders.service', compact('orders'));
    }

    public function subscription()
    {
        return view('customer.subscriptions.telegram');
    }

}
