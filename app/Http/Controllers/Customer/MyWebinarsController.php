<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Repository\ItemOrderRepository;

class MyWebinarsController extends Controller
{
    public function mySubscribe()
    {
        $orderWebinars = (new ItemOrderRepository())->forUserWebinars();

        return view('customer.webinars.index', compact('orderWebinars'));
    }
}
