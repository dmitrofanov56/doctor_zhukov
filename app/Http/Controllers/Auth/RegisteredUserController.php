<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\View\View;

class RegisteredUserController extends Controller
{

    /**
     * @return View
     */

    public function create(): View
    {
        return view('auth.register2');
    }

    /**
     * @return View
     */

    public function v2(): View
    {
        return view('auth.register2');
    }

    /**
     * @param LoginRequest $request
     *
     * @return RedirectResponse
     */

    public function store(RegisterRequest $request): RedirectResponse
    {
        $user = User::create([
            'username' => $request->input('username'),
            'phone'    => $request->input('phone'),
            'email'    => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }

}
