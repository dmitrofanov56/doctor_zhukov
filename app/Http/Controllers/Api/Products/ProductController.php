<?php

namespace App\Http\Controllers\Api\Products;

use App\Http\Controllers\Controller;
use App\Repository\VideoRepository;
use App\Repository\PdfRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /**
     * Получить информацию по продукту
     * @param Request $request
     *
     * @return false|\Illuminate\Database\Query\Builder|mixed
     */

    public function getProduct(Request $request)
    {
        switch ($request->input('type')) {
            case 'videos':
                return (new VideoRepository())->findById($request->input('id'));

            case 'pdfs':
                return (new PdfRepository())->findById($request->input('id'));
        }

        return false;
    }
}
