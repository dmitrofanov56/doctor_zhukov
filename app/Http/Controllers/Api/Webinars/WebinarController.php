<?php

namespace App\Http\Controllers\Api\Webinars;

use App\Http\Controllers\Controller;
use App\Models\Webinar;
use App\Notifications\Customer\Invoice;
use App\Repository\OrderRepository;
use Illuminate\Http\Request;

class WebinarController extends Controller
{

    public function createOrderWebinar(Webinar $webinar, Request $request)
    {
        if (! $request->user()) {
            return response()->json(['noAuth' => true]);
        }

        $order = (new OrderRepository())->create('Yoo');

        $order->items()->create([
            'webinar_id' => $webinar->id,
            'price'      => $webinar->price,
            'type'       => 'webinar',
        ]);

        $request->user()->notify(new Invoice($order));

        return $order->payYoo();
    }

}
