<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\User;
use App\Repository\StatsRepository;
use App\Repository\Telegram\SubscriptionRepository;
use Illuminate\Http\JsonResponse;

class StatsController extends Controller
{

    /**
     * @return JsonResponse
     */

    public function stats(): JsonResponse
    {
        return response()->json([
            'users'         => (new StatsRepository(new User))->todayCount(),
            'successOrders' => (new StatsRepository(new Order))->successPayCount(),
            'earn'          => (new StatsRepository(new OrderItem))->sumRelationToday('order', 'status', 1) . ' руб.',
            'notPay'        => (new StatsRepository(new OrderItem))->sumRelationToday('order', 'status', 0) . ' руб.',
            'month'         => (new StatsRepository(new OrderItem))->sumRelationMonth('order', 'status', 1),
            'developer'     => number_format((new StatsRepository(new OrderItem))->percentForDeveloper() - env('PERCENT')),
            'subscriptions' => number_format((new SubscriptionRepository)->forMonth(), 0, ',', '')
        ]);
    }

}
