<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function logout() : RedirectResponse
    {
        Auth::logout();

        return redirect()->route('login');
    }
}
