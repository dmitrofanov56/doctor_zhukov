<?php

namespace App\Http\Controllers\Bot;

use App\Actions\Telegram\Converstation\Buy\Webinar;
use App\Actions\Telegram\Converstation\Monitoring;
use App\Actions\Telegram\Converstation\Subscription;
use App\Actions\Telegram\Join;
use App\Actions\Telegram\LiveStarted;
use App\Actions\Telegram\Members;
use App\Actions\Telegram\Middleware\Sending;
use App\Http\Controllers\Controller;
use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\Exceptions\TelegramAttachmentException;
use BotMan\Drivers\Telegram\TelegramDriver;
use Carbon\Carbon;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Support\Facades\RateLimiter;

class TelegramBotController extends Controller
{
    public function handle()
    {
      //  config()->set('botman.telegram.token', env('TELEGRAM_TOKEN'));

        try {

            (new Join)->getInfoUserJoin();

            (new Members)->getNewMembers();

            (new LiveStarted)->getChatVoiceEvent();

            //  $middleware = new Message();

            BotMan::receivesVideos(function () {
             //   info(1);
            });

            BotMan::on('chat_join_request', function ($payload, \BotMan\BotMan\BotMan $botMan) {
                info($payload);
            });

            BotMan::hears('доктор', function (\BotMan\BotMan\BotMan $botMan) {

                info($botMan->getMessage()->getPayload());

                $botMan->sendRequest('sendContact', [
                    'chat_id'      => $botMan->getMessage()->getPayload()['chat']['id'],
                    'phone_number' => '+7 911 829 0740',
                    'first_name'   => 'Доктор для личных сообщений',
                ]);
            });

            BotMan::hears('подписка', function (\BotMan\BotMan\BotMan $botMan) {
                $botMan->startConversation(new Subscription);
            });

            BotMan::hears('/start', function (\BotMan\BotMan\BotMan $botMan) {
                $botMan->startConversation(new Subscription);
            });

            BotMan::hears('вебинары', function (\BotMan\BotMan\BotMan $botMan) {
                $botMan->startConversation(new Webinar);
            });

            BotMan::hears('мой id', function (\BotMan\BotMan\BotMan $botMan) {
                $id = $botMan->getUser()->getId();
                $botMan->reply($id);
            });

            BotMan::hears('/monitor', function (\BotMan\BotMan\BotMan $botMan) {
                $botMan->startConversation(new Monitoring);
            });

            BotMan::fallback(function (\BotMan\BotMan\BotMan $botMan) {
                //
            });

            BotMan::listen();

        } catch (TelegramAttachmentException $exception) {

        }

    }
}
