<?php

namespace App\Interfaces;

interface Pay {

    // Сумма платежа

    public function amount(int $amount) : self;

    // Описание платежа

    public function description(string $description) : self;

    // Название платежа

    public function title(string $title) : self;
}
