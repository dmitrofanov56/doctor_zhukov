<?php

namespace App\Notifications\User\Register;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class LightToEmail extends Notification
{
    use Queueable;

    public $login;

    public function __construct($login)
    {
        $this->login = $login;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Доброго времени суток, ' . $notifiable->username)
                    ->line('Вы прошли упрощенную регистрацию')
                    ->line('Ваш логин: ' . $this->login)
                    ->line('Ваш пароль: ' . $this->login)
                    ->action('Перейти в личный кабинет' , route('customer'))->subject('Вы зарегистрировались на проекте ' . env('APP_NAME'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
