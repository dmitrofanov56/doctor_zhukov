<?php

namespace App\Mail\Order;

use App\Models\User;
use App\Repository\OrderRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendReminder extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.order.reminder', [
            'url'    => route('customer'),
            'orders' => app(OrderRepository::class)->userNotPaidOrdersId($this->user),
            'user'   => $this->user
        ])->subject('Есть неоплаченные заказы на сайте');
    }
}
