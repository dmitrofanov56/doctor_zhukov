<?php

namespace App\Mail\Order;

use App\Models\Order;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Invoice extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $order;

    public function __construct(User $user, Order $order)
    {
        $this->user  = $user;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.order.invoice', [
            'url'     => route('customer'),
            'order'   => $this->order,
            'user'    => $this->user,
        ])->subject('Оплата заказа');
    }
}
