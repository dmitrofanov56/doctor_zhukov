<?php

namespace App\Mail\Subscriptions;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMaterial extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $material;

    public function __construct(User $user, $material)
    {
        $this->user         = $user;
        $this->material     = $material;
    }

    public function build()
    {
        $make = $this->markdown('mail.subscriptions.sendMaterial', [
            'material' => $this->material,
            'user'     => $this->user
        ]);

        if ($this->material->type == 'pdf') {
            $make->attach(asset('storage/' . $this->material->pdfUrl ));
        }

        $make->subject('Ежемесячный материал');

        return $make;
    }
}
