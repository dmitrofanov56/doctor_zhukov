<?php

namespace App\Mail\Admin\NewsLetter;

use App\Models\NewsLetter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TemplateSend extends Mailable
{
    use Queueable, SerializesModels;

    protected $newsLetter;

    public function __construct(NewsLetter $newsLetter)
    {
        $this->newsLetter = $newsLetter;
    }

    public function build()
    {
        return $this->markdown('mail.newsletter.templateSend', [
            'newsLetter' => $this->newsLetter
        ])->subject($this->newsLetter->subject)->onQueue('newsLetters');
    }
}
