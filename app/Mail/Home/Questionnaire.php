<?php

namespace App\Mail\Home;

use App\Http\Livewire\Home\Questionnaire\Store;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Questionnaire extends Mailable
{

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $data;

    public function __construct(Store $store)
    {
        $this->data = $store;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->view('mail.questionnaire.send', [
            'fio'        => $this->data->fio,
            'age'        => $this->data->age,
            'sex'        => $this->data->sex,
            'complaints' => $this->data->complaints,
            'diagnoses'  => $this->data->diagnoses,
            'tablets'    => $this->data->tablets,
            'allergy'    => $this->data->allergy,
            'period'     => $this->data->period,
            'user'       => auth()->user()
        ])->subject('Анкета пациента - ( ' . $this->data->fio . ' )' );

        foreach ($this->data->file as $file) {
            $email->attach($file->getRealPath(), [
                'as'   => $file->getClientOriginalName(),
                'mime' => $file->getMimeType(),
            ]);
        }

        return $email;
    }

}
