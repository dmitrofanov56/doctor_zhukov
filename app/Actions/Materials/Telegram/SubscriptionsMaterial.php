<?php

namespace App\Actions\Materials\Telegram;

use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\Extensions\Keyboard;
use BotMan\Drivers\Telegram\Extensions\KeyboardButton;
use BotMan\Drivers\Telegram\TelegramDriver;

class SubscriptionsMaterial
{
    public function send($telegram_id, $material)
    {
        $typeMaterial = $material->type;

        $keyboard = Keyboard::create()
            ->type(Keyboard::TYPE_INLINE)
            ->oneTimeKeyboard(true)
            ->addRow(
                KeyboardButton::create("Перейти в платный канал")->url(config('medschool.invite_telegram')),
                KeyboardButton::create($typeMaterial == 'video' ? 'Смотреть ( ' . $material->title . ') '  : 'Читать ( ' . $material->title . ') ' )->url($typeMaterial == 'video' ? $material->videoUrl : asset('storage/' . $material->pdfUrl)),
            )->toArray();

        $params = collect([
            'parse_mode' => 'html'
        ])->merge($keyboard)->toArray();

        BotMan::say("Спасибо, вы оформили подписку на ежемесячный материал, вся информация отправлена на вашу почту которую вы указали\n\nВведите <b>подписка</b> для того чтобы узнать информацию",
            $telegram_id,
            TelegramDriver::class,
            $params
        );
    }
}
