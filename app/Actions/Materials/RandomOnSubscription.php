<?php

namespace App\Actions\Materials;

use App\Repository\PdfRepository;
use App\Repository\VideoRepository;
use Illuminate\Support\Collection;


class RandomOnSubscription
{
    public $material;

    public function __construct()
    {
        $pdfs = (new PdfRepository)->getAll();

        $videos = (new VideoRepository)->getAll();

        $this->material =  Collection::make()->merge($videos)->merge($pdfs)->random();
    }
}
