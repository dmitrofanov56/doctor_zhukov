<?php

namespace App\Actions\Telegram;

use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\TelegramDriver;

class LiveStarted
{
    protected $chat_id;

    public function say($message) {
       BotMan::say($message, $this->chat_id, TelegramDriver::class);
    }

    /**
     * Отправим сообщение
     * @return void
     */

    public function getChatVoiceEvent()
    {

        if (request()->has('message.chat')) {

            $this->chat_id = request()->collect('message.chat')->get('id');

            // Если эфир запущен

            if (request()->has('message.voice_chat_started') ) {
                $this->say('Уважаемые пользователи чата, начался прямой эфир.');
            }

            // Если эфир закончен

            if ( request()->has('message.voice_chat_ended') ) {
                $this->say('Прямой эфир закончен, спасибо всем кто присутствовал.');
            }
        }

    }
}
