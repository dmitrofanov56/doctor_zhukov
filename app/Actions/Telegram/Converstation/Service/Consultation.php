<?php

namespace App\Actions\Telegram\Converstation\Service;

use BotMan\BotMan\Messages\Conversations\Conversation;

class Consultation extends Conversation
{
    public function info()
    {
        $this->say('Вам нужна консультация?');
    }

    public function run()
    {
        $this->info();
    }
}
