<?php

namespace App\Actions\Telegram\Converstation;

use App\Actions\Telegram\Converstation\Buy\Webinar;
use App\Payments\Yoo;
use App\Repository\Telegram\SubscriptionRepository;
use App\Repository\UserRepository;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;

use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\Drivers\Telegram\Extensions\Keyboard;
use BotMan\Drivers\Telegram\Extensions\KeyboardButton;


class Subscription extends Conversation
{
    public $period = 30;
    public $amount = 299;

    public $user;

    public function checkUser()
    {
        try {
            $user_id = $this->bot->getUser()->getId();

            $this->user = (new UserRepository)->checkTelegramId($user_id);

            if (is_null($this->user)) {
                $this->bot->startConversation(new User(Subscription::class));
            } else {
                $this->mySubscriptions();
            }
        } catch (\Exception $exception) {

        }
    }

    public function mySubscriptions()
    {
        if ($this->user->subscriptions()->active()->count() > 0 ) {

            foreach ($this->user->subscriptions()->active()->get() as $subscription) {

                $keyboard = Keyboard::create()
                    ->type(Keyboard::TYPE_INLINE)
                    ->oneTimeKeyboard(true)
                    ->addRow(
                        KeyboardButton::create("Отменить")->callbackData('unsub_id_' . $subscription->id),
                        KeyboardButton::create("Перейти в платный канал")->url(config('medschool.invite_telegram')),
                    )->toArray();

                $data = array_merge([
                    'parse_mode' => 'html'
                ], $keyboard);

                $this->ask("Здравствуйте, у вас есть платная подписка\n\nТариф: материал каждый месяц\n\nСтоимость: <b>{$this->amount}</b> руб\n\nДата подписки: {$subscription->updated_at}\nОкончание подписки: {$subscription->expired_at}\n\nЕсли данная подписка вам не нужна, вы можете отменить её, но деньги которые вы заплатили вам не вернутся, и ежемесячный материал вы не сможете больше получать.", function (Answer $answer) use ($data) {
                    if ($answer->isInteractiveMessageReply()) {

                        $callbackId = str_replace('unsub_id_', '', $answer->getValue());

                        if ((new SubscriptionRepository)->unsubscribe($callbackId)) {
                            $this->say('Подписка отменена');
                        }
                    }
                }, $data);
            }
        } else {
            /*$keyboard = Keyboard::create()
                ->type(Keyboard::TYPE_INLINE)
                ->oneTimeKeyboard(true)
                ->addRow(
                    KeyboardButton::create("месяц {$this->amount} руб")->callbackData('1_month')
                )->toArray();

            $this->ask('Здравствуйте, на данный момент у вас нет платной ежемесячной подписки.', function (Answer $answer) {
                if ($answer->isInteractiveMessageReply()) {
                    $this->processPay();
                }
            }, $keyboard);*/

            $this->processPay();
        }
    }

    /**
     * @return void
     */

    public function processPay()
    {
        $amount = $this->user->is_admin ? 1 : $this->amount;

        $orderId = uniqid();

        $order = (new Yoo)
            ->getCustomPayUrl(
                $orderId,
                $amount,
                'Оплата ежемесячной подписки на материалы',
                [
                    'telegram_subs' => true,
                    'amount'        => $amount,
                    'period'        => $this->period,
                    'phone'         => $this->user->phone,
                    'email'         => $this->user->email
                ]
            );

        $keyboard = Keyboard::create()
            ->type(Keyboard::TYPE_INLINE)
            ->oneTimeKeyboard(true)
            ->addRow(
                KeyboardButton::create("Оплатить подписку")->url($order)
            )->toArray();


        $this->say("<b>Внимание</b>\n\nЕсли вам не пришло на почту письмо напишите в этом чате подписка и увидите всю информацию после оплаты" , [
            'parse_mode' => 'html'
        ]);

        $this->say("Для оплаты перейдите по ссылке", array_merge($keyboard));
    }

    public function run()
    {
        $this->checkUser();
    }
}
