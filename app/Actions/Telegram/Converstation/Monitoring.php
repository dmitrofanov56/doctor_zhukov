<?php

namespace App\Actions\Telegram\Converstation;

use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\Drivers\Telegram\Extensions\Keyboard;
use BotMan\Drivers\Telegram\Extensions\KeyboardButton;
use Illuminate\Support\Collection;

class Monitoring extends Conversation
{
    protected $region;
    protected $gender;
    protected $age;

    public function keyboard()
    {
        return Keyboard::create()
            ->type(Keyboard::TYPE_INLINE)
            ->oneTimeKeyboard(true)
            ->addRow(KeyboardButton::create('<<')->callbackData('prev'),
                KeyboardButton::create('👍')->callbackData('accept'),
                KeyboardButton::create('👎')->callbackData('decline'),
                KeyboardButton::create('>>')->callbackData('next'))
            ->toArray();
    }

    public function message()
    {
        $this->askRegion();
    }

    /**
     * Запросить регион
     * @return void
     */

    public function askRegion()
    {
        $this->ask('Введите ваш регион проживания', function (Answer $answer)
        {
            $message = preg_match('/^[0-9]*$/', $answer->getText());

            if (!$message) :
                $this->say('Регион должен быть числом например 56 или 99');
                $this->repeat();
            else:
                $this->region = $answer->getText();
                $this->askGender();
            endif;

        });
    }

    public function askAge()
    {
        $this->ask("Укажите ваш возраст (цифрой)", function (Answer $answer)
        {
            $message = preg_match('/^[0-9]*$/', $answer->getText());

            if (!$message) :
                $this->say('Возраст должен быть цифрой');
                $this->repeat();
            else:
                $this->bot->sendRequest('editMessageText', array_merge([
                    'chat_id'    => $this->bot->getMessage()->getPayload()['chat']['id'],
                    'message_id' => $this->bot->getMessage()->getPayload()['message_id'],
                    'text'       => 'Вы указали возраст: ' . $answer->getText()
                ]));
                $this->age = $answer->getText();
                $this->say('Спасибо вы указали все данные.');
            endif;
        });
    }

    public function askGender()
    {
        $question = Question::create('Выберите ваш пол')
            ->fallback('Unable to create a new database')
            ->callbackId('select_gender')
            ->addButtons([
                Button::create('Мужской')->value('male'),
                Button::create('Женский')->value('female'),
            ]);

        $this->ask($question, function (Answer $answer)
        {
            if ($answer->isInteractiveMessageReply()) :

                $gender = $answer->getValue() == 'male' ? 'мужской' : 'женский';

                $this->bot->sendRequest('editMessageText', array_merge([
                    'chat_id'    => $this->bot->getMessage()->getPayload()['chat']['id'],
                    'message_id' => $this->bot->getMessage()->getPayload()['message_id'],
                    'text'       => 'Вы выбрали пол: ' . $gender
                ]));
                $this->gender = $answer->getValue();
                $this->askAge();
            endif;
        });
    }

    public function run()
    {
        $this->message();
    }
}
