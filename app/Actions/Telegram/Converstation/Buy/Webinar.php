<?php

namespace App\Actions\Telegram\Converstation\Buy;

use App\Actions\Telegram\Converstation\User;
use App\Payments\Yoo;
use App\Repository\OrderRepository;
use App\Repository\Telegram\SubscriptionRepository;
use App\Repository\UserRepository;
use App\Repository\WebinarRepository;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\Drivers\Telegram\Extensions\Keyboard;
use BotMan\Drivers\Telegram\Extensions\KeyboardButton;

class Webinar extends Conversation
{
    public $user;

    public function checkUser()
    {
        $this->user = (new UserRepository)->checkTelegramId($this->bot->getUser()->getId());

        if (is_null($this->user)) {
            $this->bot->startConversation(new User(Webinar::class));
        } else {
            $this->webinars();
        }
    }

    public function webinars()
    {
        $webinars = (new WebinarRepository)->getAll();

        $buttonArray = [];

        foreach ($webinars as $webinar)
        {
            if ($webinar->start_date->diffInDays() > 0 && $webinar->start_date->diffInDays() < 30) {
                $title = $webinar->title . " {$webinar->price} руб. (скоро)";
            } else {
                $title = $webinar->title . ' '.$webinar->price.' руб.';
            }

            $button = Button::create($title)->value($webinar->id);
            $buttonArray[] = $button;
        }

        $question = Question::create('Список всех вебинаров доступных для покупки, выберите нужный и выполните процедуру оплаты')
            ->callbackId('webinars')
            ->addButtons($buttonArray);

        $this->ask($question, function (Answer $answer) use ($question)
        {
            if ($answer->isInteractiveMessageReply())
            {
                $webinar = (new WebinarRepository)->findById($answer->getValue());

                $amount = $this->user->is_admin ? 1 : $webinar->price;

                $orderId = uniqid();

                $order = (new Yoo)
                    ->getCustomPayUrl(
                        $orderId,
                        $amount,
                        'Оплата вебинара',
                        [
                            'webinar'       => $webinar->id,
                            'amount'        => $amount,
                            'phone'         => $this->user->phone,
                            'email'         => $this->user->email
                        ]
                    );

                $keyboard = Keyboard::create()
                    ->type(Keyboard::TYPE_INLINE)
                    ->oneTimeKeyboard(true)
                    ->addRow(
                        KeyboardButton::create("Оплатить " . $webinar->title)->url($order)
                    )->toArray();

                $this->say("Для оплаты перейдите по ссылке", array_merge($keyboard));

            } else {
                $this->repeat($question);
            }
        });
    }

    public function run()
    {
        $this->checkUser();
    }
}
