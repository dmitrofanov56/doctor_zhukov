<?php

namespace App\Actions\Telegram\Converstation;

use App\Repository\UserRepository;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;

class User extends Conversation
{
    public $email;
    public $phone;
    public $username;

    public $converstation;

    public function __construct($converctation)
    {
        $this->converstation = $converctation;
    }

    public function askEmail()
    {
        $this->ask('Необходимо указать электронную почту', function (Answer $answer) {

            $email = $answer->getText();

            $doNotEmail = Question::create('Неверный формат почты, попробуйте снова');

            if (!preg_match('/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/', $email)) {
                $this->repeat($doNotEmail);
            } else {
                $this->email = $email;
                $this->askPhone();
            }
        });
    }


    public function askPhone()
    {
        $this->ask('Необходимо указать номер телефона', function (Answer $answer) {
            $phone = $answer->getText();
            $this->phone    = $phone;
            $this->username = $phone;

            $this->newUser();
        });
    }

    public function newUser()
    {
        (new UserRepository)->createFields([
            'telegram_id' => $this->bot->getUser()->getId(),
            'username'    => $this->username,
            'email'       => $this->email,
            'phone'       => $this->phone,
            'password'    => bcrypt($this->phone)
        ]);

        $this->bot->startConversation(app($this->converstation));
    }


    public function run()
    {
        $this->askEmail();
    }
}
