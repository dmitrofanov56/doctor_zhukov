<?php

namespace App\Actions\Telegram;

use App\Repository\Telegram\InviteUserRepository;
use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\TelegramDriver;

class Members
{
    public function getNewMembers()
    {
        // Если есть список новых пользователей

        if (request()->collect('message.new_chat_members')->count() > 0)
        {
            $chat_id = request()->collect('message.chat')->get('id');

            $from = request()->has('message.from') ? request()->collect('message.from') : null;

            $fromUser = collect([
                'telegram_id'            => $chat_id,
                'telegram_from_id'       => $from->get('id'),
                'telegram_from_username' => $from->get('username') ? $from->get('username') : $from->get('first_name') . ' ' . $from->get('last_name'),
            ]);

            $countInvite = request()->collect('message.new_chat_members')->count();

            request()->collect('message.new_chat_members')->each(function ($item) use ($chat_id, $fromUser, $countInvite)
            {
                $scope = $countInvite < 2 ? 4 : 8.5;

                $user = $fromUser->merge([
                    'telegram_join_id'       => $item['id'],
                    'telegram_join_username' => $item['first_name'],
                    'scope'                  => $scope
                ])->toArray();

                if ($user['telegram_from_id'] == $user['telegram_join_id'] )  {
                    $message = sprintf("✨ К нам присоединился пользователь <b>%s</b> ✨ рады приветствовать в нашем чате 👏",
                        $user['telegram_join_username']
                    );
                } else {
                    $message = sprintf("✨ Рады приветствовать <b>%s</b> в нашем чате ✨ \n\nПригласил - <b>%s</b> и получил <b>%s</b> бонусов.\n\nБольшое спасибо за помощь.👏",
                        $user['telegram_join_username'],
                        $user['telegram_from_username'],
                        $scope
                    );
                }

                BotMan::say($message, $chat_id, TelegramDriver::class, [
                    'parse_mode' => 'html'
                ]);

                (new InviteUserRepository)->storeInvite($user);
            });
        }
    }
}
