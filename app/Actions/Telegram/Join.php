<?php

namespace App\Actions\Telegram;

use App\Models\ChatJoin;
use App\Repository\UserRepository;
use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\Extensions\Keyboard;
use BotMan\Drivers\Telegram\Extensions\KeyboardButton;
use BotMan\Drivers\Telegram\TelegramDriver;

class Join
{
    public function getInfoUserJoin()
    {
        if (request()->collect()->has('chat_join_request')) {

            $chat = request()->post('chat_join_request');

            $from = collect($chat['from']);

            if ($from->has('username')) {
                $username = $from->get('username');
            } else {
                $username = $from->get('first_name') . ' ' . $from->get('last_name');
            }

            $join = ChatJoin::firstOrCreate(['user_chat_id' => $from->get('id')] , [
                'chat_id'      => env('SUBS_CHAT_ID'),
                'username'     => $username
            ]);

            $user = (new UserRepository)->checkTelegramId($from->get('id'));

            if ($user->subscriptions()->where('status' , 'active')->exists() ) {

                $bot = \Illuminate\Support\Facades\Http::post('https://api.telegram.org/bot' . config('botman.telegram.token') . '/approveChatJoinRequest', [
                    'chat_id' => env('SUBS_CHAT_ID'),
                    'user_id' => $user->telegram_id,
                ])->json();

                $join->update(['status' => 'active']);

            } else {

                $data = array_merge([
                    'parse_mode' => 'html'
                ]);

                $bot = \Illuminate\Support\Facades\Http::post('https://api.telegram.org/bot' . config('botman.telegram.token') . '/declineChatJoinRequest', [
                    'chat_id' => env('SUBS_CHAT_ID'),
                    'user_id' => $user->telegram_id,
                ])->json();

                $join->update(['status' => 'decline']);

                BotMan::say('Заявка на вступление <b>отклонена</b>, у вас нет подписки, нажмите /start чтобы подписаться',  $from->get('id') , TelegramDriver::class , $data);
            }
         }
    }
}
