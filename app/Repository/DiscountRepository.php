<?php

namespace App\Repository;

use App\Models\Discount;
use App\Models\Order;
use Illuminate\Database\Eloquent\Builder;

class DiscountRepository
{

    /**
     * @var Builder $model
     */

    protected $model;

    public function __construct()
    {
        $this->model = new Discount();
    }

    /**
     * Получить все промокоды
     * @return mixed
     */

    public function getAll()
    {
        return  $this->model->orderByDesc('created_at')->paginate(10);
    }

    /**
     * @param string $code
     *
     * @return array|\Illuminate\Database\Eloquent\HigherOrderBuilderProxy|mixed
     */

    public function findByCode(string $code)
    {
        $find = $this->model->where('code', $code)->first();

        if ($find && $find->used_count > 0  ) {
            return $find;
        }

        return [];
    }

    /**
     * Использовать промокод
     * @param string $code
     *
     * @return int
     */

    public function codeUse(string $code) : int
    {
        return $this->model->where('code', $code)->decrement('used_count', 1);
    }
}
