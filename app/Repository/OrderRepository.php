<?php

namespace App\Repository;

use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class OrderRepository
{
    const NO_PAY = 0;
    const SUCCESS_PAY = 1;
    const CANCEL_PAY = 2;

    const PAGINATE = 10;

    /**
     * @var Builder $model
     */

    protected $model;

    public function __construct()
    {
        //$this->model = !is_null($order) ? $order : new Order();
        $this->model = new Order();
    }

    /**
     * Создаём заказ
     *
     * @param null $discount
     *
     * @return Order
     */

    public function create($paySystem = 'TPay', $discount = null) : Order
    {
        $orderUid = (string) Str::uuid();

        return $this->model->create([
            'user_id'         =>  auth()->id(),
            'order'           => $orderUid,
            'pay_system'      => $paySystem,
            'discount_id'     => $discount ? $discount->id : null
        ]);
    }

    /**
     * @param $paySystem
     * @param User $user
     * @return Order
     */

    public function createCustom($paySystem = 'Yoo', User $user) : Order
    {
        $orderUid = (string) Str::uuid();

        return $this->model->create([
            'user_id'         => $user->id,
            'order'           => $orderUid,
            'pay_system'      => $paySystem
        ]);
    }

    /**
     * Получить неоплаченные ордеры по пользователю
     *
     * @param User $user
     *
     * @return Collection
     */

    public function userNotPaidOrdersId(User $user): Collection
    {
        return
            $this->model
                ->where('status', self::NO_PAY)
                ->where('user_id', $user->id)
                ->get();
    }

    /**
     * Получить все ордеры пользователя
     *
     * @param string $order_by
     *
     * @return LengthAwarePaginator
     */

    public function ordersByUser(string $order_by = 'asc'): LengthAwarePaginator
    {
        return
            $this->model
                ->where('user_id', auth()->id())
                ->orderBy('created_at', $order_by)
                ->paginate(10);
    }

    public function setCancelOrderStatus(string $id) : bool
    {
       return $this->orderFindTransaction($id)
            ->where('user_id', auth()->id())
            ->update(['status' => self::CANCEL_PAY]);

    }

    /**
     * Установить статус оплачено
     *
     * @param int $order_id
     *
     * @return int
     */

    public function setSuccessOrderStatus(int $order_id): int
    {
        return
            $this->model
                ->where('id', $order_id)
                ->where('status', self::NO_PAY)
                ->update(['status' => self::SUCCESS_PAY]);
    }

    /**
     * Найти платёж по номеру транзакции
     * @param string $transactionId
     *
     * @return Builder|\Illuminate\Database\Eloquent\Model|object|null
     */

    public function orderFindTransaction(string $transactionId)
    {
        return
            $this->model
                ->where('order', '=' ,$transactionId)
                ->first();
    }

    /**
     * Установить скидку всем товарам по промокоду
     *
     * @param       $discount
     */

    public function setDiscountItems($discount)
    {
        // Если указан промокод

        if ($this->model->discount && $this->model->discount->used_count > 0)
        {
            // Уменьшаем сумму всем товарам по промокоду

            $this->model->items()->each(function ($item) use ($discount)
            {
                $item->update([
                    'price' => $item->price - ($item->price * $discount['percent'] / 100),
                ]);
            });
        }
    }

    /**
     * Все неоплаченные отменить
     * @return int
     */

    public function noPaysToCanceled() : int
    {
        return $this->model
            ->where('status', self::NO_PAY)
            ->update(['status' => self::CANCEL_PAY]);
    }

    public function getAll($user_id = null) : LengthAwarePaginator
    {
        if (! is_null($user_id) ) {
            return $this->model
                ->where('user_id', $user_id)
                ->orderByDesc('created_at')
                ->paginate(10);
        }

        return $this->model->orderByDesc('created_at')->paginate(10);
    }

    /**
     * Проверим есть ли у пользователя созданные и неоплаченные заказы
     * @return int
     */

    public function existsForUser() : int
    {
        return Auth::check() ? $this->userNotPaidOrdersId(Auth::user())->count() : 0;
    }

    /**
     * Успешные платежи
     * @return Collection
     */

    public function successPays() : Collection
    {
        return $this->model
            ->where('status', self::SUCCESS_PAY)
            ->orderByDesc('created_at')
            ->get();
    }

    /**
     * Неоплаченные
     * @return Collection
     */

    public function noPays() : Collection
    {
        return $this->model
            ->where('status', self::NO_PAY)
            ->orderByDesc('created_at')
            ->get();

    }

    /**
     * Отменённые
     * @return Collection
     */

    public function canceledPays(): Collection
    {
        return $this->model
            ->where('status', self::CANCEL_PAY)
            ->orderByDesc('created_at')
            ->get();

    }
}
