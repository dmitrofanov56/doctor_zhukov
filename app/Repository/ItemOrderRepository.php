<?php

namespace App\Repository;

use App\Models\OrderItem;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ItemOrderRepository
{

    /**
     * @var Builder $model
     */

    protected $model;

    public function __construct()
    {
        $this->model = new OrderItem();
    }

    /**
     * Получить все купленные вебинары
     * @return LengthAwarePaginator
     */

    public function forUserWebinars(): LengthAwarePaginator
    {
        return $this->model
            ->join('orders', function ($join) {
                $join->on('orders.id', '=', 'order_items.order_id')
                    ->where('orders.user_id', '=', auth()->user()->id)
                    ->where('orders.status', '=', 1);
            })
            ->where('order_items.type', 'webinar')
            ->select('order_items.*')
            ->groupBy('order_items.id')
            ->orderByDesc('created_at')
            ->paginate(10);

    }

    /**
     * Получить все купленные вебинары и активные
     * @return LengthAwarePaginator
     */

    public function subscribesWebinars(): LengthAwarePaginator
    {
        return $this->model
            ->join('orders', function ($join) {
                $join->on('orders.id', '=', 'order_items.order_id')
                    ->where('orders.status', '=', 1);
            })
            ->where('order_items.type', '=', 'webinar')
            ->join('webinars', 'webinars.id', '=', 'order_items.webinar_id')
            ->where('webinars.status', '=', 1)
            ->select('order_items.*')
            ->groupBy('order_items.id')
            ->orderByDesc('created_at')
            ->paginate(100);

    }

    public function subscribesWebinarsMonth()
    {
        return $this->model
            ->join('orders', function ($join) {
                $join->on('orders.id', '=', 'order_items.order_id')
                    ->where('orders.status', '=', 1);
            })
            ->whereBetween('orders.created_at',  [now()->subDays(30), now()])
            ->where('order_items.type', '=', 'webinar')
            ->join('webinars', 'webinars.id', '=', 'order_items.webinar_id')
            ->where('webinars.status', '=', 1)
            ->select('order_items.*')
            ->groupBy('order_items.id')
            ->orderByDesc('created_at')
            ->get();

    }

    /**
     * Получить все купленные вебинары и активные
     * @return LengthAwarePaginator
     */

    public function subscribesWebinarMonthTelegramIds()
    {
        return $this->model
            ->join('orders', function ($join) {
                $join->on('orders.id', '=', 'order_items.order_id')->where('orders.status', '=', 1)->whereBetween('orders.created_at',  [now()->subDays(30), now()]);
            })->join('users' , 'users.id' , 'orders.user_id')
            ->where('order_items.type', '=', 'webinar')
            ->join('webinars', 'webinars.id', '=', 'order_items.webinar_id')
            ->select('users.*')
            ->groupBy('users.id')
            ->orderByDesc('created_at')
            ->pluck('users.telegram_id');

    }


    /**
     * @return LengthAwarePaginator
     */

    public function pdfsForUser(): LengthAwarePaginator
    {
        return $this->model->with(['pdf'])
            ->where('type', 'pdf')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->where('user_id', Auth::id())
            ->where('status', 1)
            ->select('order_items.*')
            ->orderByDesc('created_at')
            ->paginate(10);
    }

    /**
     * @return LengthAwarePaginator
     */

    public function videosForUser(): LengthAwarePaginator
    {
        return $this->model->with(['video'])
            ->where('type', 'video')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->where('user_id', Auth::id())
            ->where('status', 1)
            ->select('order_items.*')
            ->orderByDesc('created_at')
            ->paginate(10);
    }

    /**
     * @return LengthAwarePaginator
     */

    public function tgForUser(): LengthAwarePaginator
    {
        return $this->model->with(['telegram'])
            ->where('type', 'telegram')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->where('user_id', Auth::id())
            ->where('status', 1)
            ->select('order_items.*')
            ->orderByDesc('created_at')
            ->paginate(10);
    }

    /**
     * @return LengthAwarePaginator
     */

    public function userService(): LengthAwarePaginator
    {
        return $this->model->with(['service'])
            ->where('type', 'service')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->where('user_id', Auth::id())
            ->where('status', 1)
            ->select('order_items.*')
            ->orderByDesc('created_at')
            ->paginate(10);
    }
}
