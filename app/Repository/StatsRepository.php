<?php

namespace App\Repository;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class StatsRepository
{
    /** @var Builder $model  */

    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @return int
     */

    public function todayCount() : int
    {
        return $this->model
            ->whereDate('created_at', '>=', Carbon::today())
            ->count();
    }

    /**
     * @return int
     */

    public function successPayCount() : int
    {
        return $this->model
            ->whereDate('created_at', '>=', Carbon::today())
            ->where('status', 1)
            ->count();
    }


    public function sumRelationToday($relation, $column, $value, $columnSum = 'price') : int
    {
        return $this->model->with($relation)
            ->whereRelation($relation, $column, '=', $value)
            ->whereDate('created_at', '>=', Carbon::today())
            ->sum($columnSum);
    }

    public function sumRelationMonth($relation, $column, $value, $columnSum = 'price') : int
    {
        return $this->model->with($relation)
                           ->whereRelation($relation, $column, '=', $value)
                           ->whereMonth('created_at', '=', now()->month)
                           ->sum($columnSum);
    }

    /**
     * @param $column
     *
     * @return int|mixed
     */

    public function sum($column)
    {
        return $this->model
            ->sum($column);
    }

    /**
     * Получить за выбранный месяц
     * @param int $month
     * @return Collection
     */

    public function withMonth(int $month) : Collection
    {
        return $this->model
            ->whereMonth('created_at' , '=', $month)
            ->get();
    }

    /**
     * Заработок за год
     * @param        $relation string
     * @param        $column string
     * @param        $value string
     * @param string $columnSum
     *
     * @return int
     */

    public function withYearSum(string $relation, string $column, string $value, string $columnSum = 'price') : int
    {
        $dateEnd = now()->toDateString();

        $dateStart = now()->subDays(7)->toDateString();

        return $this->model
            ->whereRelation($relation, $column, '=', $value)
            ->whereBetween('created_at' , [$dateStart , $dateEnd])
            ->sum($columnSum);
    }

    /**
     * Разработчику
     * @return float|int
     */
    public function percentForDeveloper()
    {
        $sumCurrentMonth = $this->sumRelationMonth('order' , 'status', 1);

        return ($sumCurrentMonth * 40) / 100;
    }

}
