<?php

namespace App\Repository;

use App\Models\NewsLetter as ModelNewsLetter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class NewsLetter
{

    /**
     * @var Builder
     */

    public $model;

    public function __construct()
    {
        $this->model = new ModelNewsLetter();
    }

    /**
     * @param Request $request
     *
     * @return ModelNewsLetter
     */

    public function create(string $subject, string $message) : ModelNewsLetter
    {
        return $this->model->create([
            'subject' => $subject,
            'message' => $message
        ]);
    }

    /**
     * @param        $find
     * @param string $subject
     * @param string $message
     *
     * @return int
     */

    public function updateByFind($find, string $subject, string $message)
    {
        return $this->model->find($find->id)->update(['subject' => $subject , 'message' => $message]);
    }

    public function getAll()
    {
        return $this->model->orderByDesc('created_at')->get();
    }

}
