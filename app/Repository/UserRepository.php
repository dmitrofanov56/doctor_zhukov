<?php

namespace App\Repository;

use App\Events\CustomRegister;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;

class UserRepository
{

    /**
     * @var Builder $model
     */

    protected $model;

    public function __construct()
    {
        $this->model = new User();
    }

    public function getAllWithPagination(string $search = '')
    {
        return $this->model->when($search, function ($q, $search) {
            return $q->where('email', 'like', '%' . $search . '%')->orWhere('phone', 'like', '%' . $search . '%');
        })->orderByDesc('created_at')->paginate(50);
    }

    /**
     * Получить всех пользователей
     * @return Collection
     */

    public function all($order_by = 'asc'): Collection
    {
        return $this->model->with(['orders'])->orderBy('created_at', $order_by)->get();
    }

    /**
     * @param string $email
     * @return Builder|\Illuminate\Database\Eloquent\Model|object|null
     */

    public function checkEmail(string $email)
    {
        return $this->model->where('email', $email)->first();
    }

    public function checkTelegramId(string $telegram_id)
    {
        return $this->model->where('telegram_id', $telegram_id)->first();
    }

    /**
     * @param string $email
     * @param string $phone
     * @param string|null $telegram_id
     * @return User|array|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object
     */

    public function createForEmail(string $email, string $phone = null, string $telegram_id = null)
    {
        $login = preg_replace('/[@].*/', '', $email);

        $checkEmail = $this->checkEmail($email);

        if (! $checkEmail)
        {
            $checkEmail = $this->model->create([
                'username'    => $login,
                'phone'       => $phone,
                'email'       => $email,
                'telegram_id' => $telegram_id,
                'password'    => Hash::make($login),
            ]);

            event(new CustomRegister($checkEmail, $login));
        }

        $checkEmail->update(['telegram_id' => $telegram_id , 'phone' => $phone]);

        return $checkEmail;
    }

    public function createFields(array $data)
    {
        $email = $data['email'];

        return $this->model->updateOrCreate(['email' => $email] , $data);
    }
}
