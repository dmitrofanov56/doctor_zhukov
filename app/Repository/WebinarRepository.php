<?php

namespace App\Repository;

use App\Models\Webinar;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

class WebinarRepository
{

    /**
     * @var Builder $model
     */

    protected $model;

    public function __construct()
    {
        $this->model = new Webinar();
    }

    public function getAll()
    {
        return $this->model->orderBy('created_at' , 'desc')->get();
    }

    /**
     * Создание вебинара
     * @param array $data
     *
     * @return Webinar
     */

    public function create(array $data = []) : Webinar
    {
        return $this->model->firstOrCreate(['title' => $data['title']] , $data);
    }

    /**
     * @param int $id
     * @return Builder|\Illuminate\Database\Eloquent\Model|object|null
     */

    public function findById(int $id)
    {
        return $this->model->where('id' , $id)->first();
    }

}
