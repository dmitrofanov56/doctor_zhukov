<?php

namespace App\Repository;

use App\Models\TelegramAccess;
use App\Models\Webinar;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

class TelegramRepository
{

    /**
     * @var Builder $model
     */

    protected $model;

    public function __construct()
    {
        $this->model = new TelegramAccess();
    }
}
