<?php

namespace App\Repository;

use App\Models\Post;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class PostsRepository
{

    /**
     * @var Builder $model
     */

    protected $model;

    public function __construct()
    {
        $this->model = new Post();
    }

    /**
     * Получить все посты
     * @return Collection
     */

    public function getPosts() : Collection
    {
      return $this->model->orderByDesc('created_at')->get();
    }

    public function newPost(array $data)
    {
        return $this->model->create($data);
    }
}
