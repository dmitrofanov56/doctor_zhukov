<?php

namespace App\Repository\Telegram;

use App\Models\Telegram\InviteChat;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class InviteUserRepository
{

    /**
     * @var Builder $model
     */

    protected $model;

    public function __construct()
    {
        $this->model = new InviteChat();
    }

    public function storeInvite(array $array)
    {
        return $this->model->firstOrCreate(['telegram_join_id' => $array['telegram_join_id']], $array);
    }

    public function topInviteUsers()
    {
        return $this->model->withCount('invite')->each(function ($item) {
            if ($item->invite_count >= 1) {

                if ($item->telegram_from_id !== $item->telegram_join_id) {
                    echo $item->telegram_from_id . ' > ' . $item->telegram_from_username . '</br>';
                }
            }
        });
    }
}
