<?php

namespace App\Repository\Telegram;

use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class SubscriptionRepository
{
    /**
     * @var Builder $model
     */
    public $model;

    public function __construct()
    {
        $this->model = new Subscription();
    }

    public function unsubscribe($sub_id)
    {
        return $this->model->where('id', $sub_id)->update(['status' => 'not_active']);
    }

    /**
     * Активные подписки
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */

    public function subscriptionsActive()
    {
        return $this->model
            ->join('users' , 'users.id' , '=' , 'subscriptions.user_id')
            ->where('status' , 'active')
           // ->where('user_id' , 1)
            ->select('subscriptions.*')
            ->groupBy('subscriptions.id')
            ->get();
    }

    public function subscriptionsLatest(array $data = [])
    {
        return $this->model
            ->join('users' , 'users.id' , '=' , 'subscriptions.user_id')
            // ->where('user_id' , 1)
            ->select('subscriptions.*')
            ->where($data)
            ->whereDate('subscriptions.updated_at', Carbon::today())
            ->groupBy('subscriptions.id')
            ->get();
    }

    public function subscriptionsNotActive()
    {
        return $this->model
            ->join('users' , 'users.id' , '=' , 'subscriptions.user_id')
            ->where('status' , 'not_active')
            // ->where('user_id' , 1)
            ->select('subscriptions.*')
            ->groupBy('subscriptions.id')
            ->get();
    }


    public function winMonthRandomUser(int $days = 30)
    {
        $days       = Carbon::now()->subDays($days);

        return $this->model
            ->join('users' , 'users.id' , '=' , 'subscriptions.user_id')
            ->where('subscriptions.status' , 'active')
            ->whereDate('subscriptions.created_at' , '>=', $days)
            ->select('subscriptions.*')
            ->groupBy('subscriptions.id')->limit(30)->inRandomOrder()->first();
    }

    /**
     * @return Collection
     */

    public function subscriptionsAll() : Collection
    {
        return $this->model
            ->join('users' , 'users.id' , '=' , 'subscriptions.user_id')
           // ->where('status' , 'active')
            // ->where('user_id' , 1)
            ->select('subscriptions.*')
            ->groupBy('subscriptions.id')
            ->get();
    }


    public function forMonth()
    {
        return $this->model
            ->whereMonth('created_at', now()->month)
            ->where('status', 'active')
            ->sum('amount');
    }
}
