<?php

namespace App\Repository;

use App\Models\OrderItem;
use App\Models\Pdf;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class PdfRepository
{

    /**
     * @var Builder $model
     */

    protected $model;

    public function __construct()
    {
        $this->model = new Pdf();
    }

    public function remove(int $id)
    {
        return $this->findById($id)->delete();
    }

    public function store(array $data)
    {
        return $this->model->create($data);
    }

    public function update(int $id, array $data)
    {
        return $this->model->find($id)->update($data);
    }

    public function findById(int $id)
    {
        return $this->model->find($id);
    }

    public function getAll()
    {
        return $this->model->orderByDesc('created_at')->get();
    }

    public function bestWeeks()
    {
        return $this->model->with('items')
            ->join('order_items', 'pdfs.id', 'order_items.pdf_id')
            ->join('orders', 'orders.id', 'order_items.order_id')
            ->whereDate('order_items.created_at' , '>=', now()->subDays(7))
            ->where('orders.status' , '=', 1)
            ->select('pdfs.*')
            ->selectRaw('COUNT(orders.status) as totalPay')
            ->groupBy('pdfs.id')
            ->orderByDesc('totalPay')
            ->first();
    }
}
