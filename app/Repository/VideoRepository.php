<?php

namespace App\Repository;

use App\Models\OrderItem;
use App\Models\Video;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class VideoRepository
{
    /**
     * @var Builder $model
     */

    protected $model;

    public function __construct()
    {
        $this->model = new Video();
    }

    public function findById(int $id)
    {
        return $this->model->find($id);
    }

    public function getAll()
    {
        return $this->model->orderByDesc('created_at')->get();
    }

    public function bestWeeks()
    {
        return $this->model->with('items')
            ->join('order_items', 'videos.id', 'order_items.video_id')
            ->join('orders', 'orders.id', 'order_items.order_id')
            ->whereDate('order_items.created_at' , '>=', now()->subDays(7))
            ->where('orders.status' , '=', 1)
            ->select('videos.*')
            ->selectRaw('COUNT(orders.status) as totalPay')
            ->groupBy('videos.id')
            ->orderByDesc('totalPay')
            ->first();
    }

}
