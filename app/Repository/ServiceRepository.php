<?php

namespace App\Repository;

use App\Models\Service;
use Illuminate\Database\Eloquent\Builder;

class ServiceRepository
{
    /**
     * @var Builder $model
     */

    protected $model;

    public function __construct()
    {
        $this->model = new Service();
    }

    public function all()
    {
        return $this->model->orderByDesc('created_at')->get();
    }

    /** Найти услугу
     * @param string $slug
     * @return array|Builder|\Illuminate\Database\Eloquent\Model|object
     */

    public function findServiceByTag(string $slug)
    {
        if ($find = $this->model->where('slug', $slug)->first()) {
            return $find;
        }

        return [];
    }

    /**
     * @param int $id
     * @return Service
     */

    public function findById(int $id) : Service
    {
        return $this->model->find($id);
    }

}
