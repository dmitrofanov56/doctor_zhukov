<?php

use App\Http\Controllers\Api\Admin\StatsController;
use App\Http\Controllers\Api\Products\ProductController;
use App\Http\Controllers\Api\Webinars\WebinarController;


Route::middleware('api')->prefix('admin')->group(function () {
    Route::get('/stats' , [StatsController::class , 'stats'])->name('api.admin.stats');
});

Route::middleware('api')->prefix('products')->group(function () {
    Route::post('/get-info', [ProductController::class , 'getProduct'])->name('api.product.info');
});

Route::middleware('api')->prefix('webinars')->group(function () {
    Route::post('/subscribe/{webinar}', [WebinarController::class , 'createOrderWebinar'])->name('api.webinar.subscribe');
});
