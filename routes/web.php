<?php

use BotMan\BotMan\Facades\BotMan;
use BotMan\Drivers\Telegram\Extensions\Keyboard;
use BotMan\Drivers\Telegram\Extensions\KeyboardButton;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Account\AccountController;

use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\Home\WebinarController;
use App\Http\Controllers\Home\SitemapController;
use App\Http\Controllers\Home\FinanceSupportController;
use App\Http\Controllers\Home\QuestionnaireController;

use App\Http\Controllers\Customer\CustomerController;
use App\Http\Controllers\Customer\MyWebinarsController;
use App\Http\Controllers\Customer\ProductController;

// ps

use App\Http\Controllers\Payment\TinkoffController;
use App\Http\Controllers\Payment\YooController;

use App\Http\Livewire\Dashboard\Customer\Subscription\Telegram as TelegramSubs;


Route::prefix('account')->middleware('auth')->group(function () {
    Route::get('/logout', [AccountController::class, 'logout'])->name('account.logout');
});

Route::prefix('/')->group(function () {
    Route::get('/create-pay', [YooController::class, 'createPay']);

    Route::get('/sitemap-generate', [SitemapController::class, 'generate']);

    Route::get('/', [HomeController::class, 'index'])->name('home');

    // Финансовая поддержка

    Route::post('/pay', [HomeController::class, 'createPay'])->name('home.pay');

    Route::post('/find-discount/{discount}', [HomeController::class, 'findDiscount'])->name('home.find.discount');

    Route::prefix('pages')->group(function () {

        Route::any('/questionnaire', [QuestionnaireController::class, 'index'])->name('page.questionnaire');

        Route::prefix('finance-support')->group(function () {
            Route::get('/', [FinanceSupportController::class, 'index'])->name('finance.support');
            Route::post('/pay', [FinanceSupportController::class, 'store'])->name('finance.support.store');
        });

        Route::prefix('webinar')->group(function () {
            Route::get('/', [WebinarController::class, 'webinarsList'])->name('home.webinars');
            Route::post('/subscribe', [WebinarController::class, 'subscribeWebinar'])->name('home.webinar.subscribe');
        });

        Route::get('/products', [HomeController::class, 'products'])->name('home.products');

        Route::get('/posts', [HomeController::class, 'posts'])->name('home.posts');
    });

});

Route::prefix('cabinet')->middleware('auth')->group(function () {

    Route::get('/', [CustomerController::class, 'index'])->name('customer');
    Route::get('/videos', [CustomerController::class, 'videos'])->name('customer.videos');
    Route::get('/pdfs', [CustomerController::class, 'pdfs'])->name('customer.pdfs');
    Route::get('/orders', [CustomerController::class, 'orders'])->name('customer.orders');

    Route::prefix('pay')->group(function () {
        Route::get('/pay/{order}', [CustomerController::class, 'pay'])->name('customer.pay.order');
        Route::get('/pay-cancel/{order}', [CustomerController::class, 'cancelOrder'])->name('customer.pay.cancel');
    });

    Route::prefix('webinars')->group(function () {
        Route::get('/my', [MyWebinarsController::class, 'mySubscribe'])->name('customer.webinars');
    });

    Route::prefix('products')->group(function () {
        Route::get('/', [ProductController::class, 'index'])->name('customer.chat.buy');
    });

    Route::prefix('telegram')->group(function () {
        Route::get('/access', [CustomerController::class, 'tgAccess'])->name('customer.telegram.access');
    });

    Route::prefix('service')->group(function () {
        Route::get('/', [CustomerController::class, 'myService'])->name('customer.services');
    });

    Route::prefix('subscriptions')->group(function () {
        Route::get('/telegram' , [CustomerController::class, 'subscription'])->name('customer.telegram.subscriptions');
    });

    Route::get('/notify', [CustomerController::class, 'notify']);

});

Route::prefix('admin')->middleware(['auth', 'admin'])->group(function () {

    Route::get('/', App\Http\Livewire\Admin\Index::class)->name('admin');

    Route::prefix('products')->group(function () {
        Route::get('/', App\Http\Livewire\Admin\Products\Index::class)->name('admin.products');
        Route::get('/create', App\Http\Livewire\Admin\Products\Create::class)->name('admin.product.create');
        Route::get('/edit', App\Http\Livewire\Admin\Products\Edit::class)->name('admin.product.edit');
    });

    Route::prefix('discount')->group(function () {
        Route::get('/', App\Http\Livewire\Admin\Discount\Index::class)->name('admin.discount');
        Route::get('/edit/{discount}', App\Http\Livewire\Admin\Discount\Edit::class)->name('admin.discount.edit');
    });

    Route::prefix('users')->group(function () {
        Route::get('/', App\Http\Livewire\Admin\Users\Index::class)->name('admin.users');
        Route::get('/show/{user}', App\Http\Livewire\Admin\Users\Show::class)->name('admin.user.show');
        Route::get('/edit/{user}', App\Http\Livewire\Admin\Users\Edit::class)->name('admin.user.edit');
    });

    Route::prefix('posts')->group(function () {
        Route::get('/telegram', \App\Http\Livewire\Admin\Posts\Telegram\Index::class)->name('admin.posts.telegram');
        Route::get('/show/{user}', App\Http\Livewire\Admin\Users\Show::class)->name('admin.user.show');
    });

    Route::prefix('services')->group(function () {
        Route::get('/', \App\Http\Livewire\Admin\Service\Index::class)->name('admin.services');
    });


    Route::get('webinars', \App\Http\Livewire\Admin\Webinar\Index::class)->name('admin.webinars');

    Route::prefix('mailing')->group(function () {
        Route::get('/', \App\Http\Livewire\Admin\NewsLetterTable::class)->name('newsletter');
        Route::get('/new', \App\Http\Livewire\Admin\NewsLetter::class)->name('newsletter.create');
    });


    Route::get('/orders', App\Http\Livewire\Admin\Orders\Index::class)->name('admin.orders');

    Route::get('/support-finance', App\Http\Livewire\Admin\OrderSupports\Index::class)->name('admin.support.finance');


    Route::prefix('blog')->group(function () {
        Route::get('/', App\Http\Livewire\Admin\Blog\Index::class)->name('admin.blog');
    });

});

Route::prefix('payment')->group(function () {
    Route::post('/', [TinkoffController::class, 'handle'])->name('notification');

    Route::prefix('yoo')->group(function () {
        Route::post('/handle', [YooController::class, 'handle'])->name('yoo.notification');
    });

    Route::get('/success', [TinkoffController::class, 'success'])->name('payment.success');
    Route::get('/error', [TinkoffController::class, 'error'])->name('payment.error');
});


Route::prefix('bot')->group(function () {
    Route::post('/v1/telegram', [\App\Http\Controllers\Bot\TelegramBotController::class, 'handle'])->name('bot.telegram');
});


Route::get('/message', function () {
    \App\Models\Subscription::all()->each(function (\App\Models\Subscription  $subscription) {
        BotMan::say("Друзья, те кто хотел <b>купить вебинар</b> но не получалось, сможете сделать это повторно, был небольшой технический сбой, приносим извинения.\n\nДля покупки введите <b>Вебинары</b>\n\nБудет очень интересный материал, спешите время ограничено." , $subscription->user->telegram_id , \BotMan\Drivers\Telegram\TelegramDriver::class, [
            'parse_mode' => 'html'
        ]);
    });
});


Route::get('/unban/{email}', function ($email) {

   $user = \App\Models\User::query()->where('email' , $email)->first();

   if (! $user) return 'Ошибка';

   $bot = \Illuminate\Support\Facades\Http::post('https://api.telegram.org/bot'.config('botman.telegram.token').'/unbanChatMember', [
       'chat_id' => -1001539682289,
       'user_id' => $user->telegram_id,
       'only_if_banned' => true
   ])->json('ok');

   return $bot ? 'Разбанен' : 'не разбанен';
});

Route::get('/stats', function () {
    return response([
        'active' => (new \App\Repository\Telegram\SubscriptionRepository)->subscriptionsActive()->count(),
        'not_active' => (new \App\Repository\Telegram\SubscriptionRepository)->subscriptionsNotActive()->count()
    ]);
});

Route::get('/unban', function () {
    $set = \Illuminate\Support\Facades\Artisan::call('chat:unban');
});


Route::get('/send-webinar', function () {

    $webinar = \App\Models\Webinar::find(5);

    //  1454219570

    $webinars = (new \App\Repository\ItemOrderRepository)->subscribesWebinarMonthTelegramIds()->each(function ($telegram_id) {
        BotMan::say('Трансляция началась / https://youtu.be/M3ElLmJIi70 /' , $telegram_id , \BotMan\Drivers\Telegram\TelegramDriver::class);
       // BotMan::say('Все кто не смог посмотреть вебинар про холестерин который был 04.01.2023 в 20:00, можете открыть запись трансляции https://youtu.be/eDkpzVSOwMs' , $telegram_id , \BotMan\Drivers\Telegram\TelegramDriver::class);
    });


});
