<x-guest-layout>
    <x-auth.card>
        <div>
            <h3 class="text-center">{{ __('Регистрация кабинета') }}</h3>
        </div>
        <form class="login-form" method="POST" action="{{ route('register') }}">
            @csrf
            <div class="form-group">
                <x-form.label for="email" :value="__('Username')"/>
                <x-form.input id="username"
                    type="text"
                    name="username"
                    placeholder="Придумайте логин"
                    :value="old('username')"
                />
            </div>
            <div class="form-group">
                <x-form.label for="phone" :value="__('Phone Number')"/>
                <x-form.input id="phone"
                    type="text"
                    name="phone"
                    placeholder="Номер телефона"
                    :value="old('phone')"
                />
            </div>
            <div class="form-group">
                <x-form.label for="email" :value="__('E-mail')"/>
                <x-form.input id="email"
                    type="email"
                    name="email"
                    placeholder="Ваша почта"
                    :value="old('email')"
                />
            </div>
            <div class="form-group">
                <x-form.label for="password" :value="__('Password')"/>
                <div class="position-relative">
                    <x-form.input id="password" type="password" name="password" autocomplete="new-password" placeholder="Придумайте пароль"/>
                </div>
            </div>
            <div class="form-group">
                <x-form.label for="password_confirmation" :value="__('Confirm Password')"/>
                <div class="position-relative">
                    <x-form.input id="password_confirmation" type="password" name="password_confirmation" placeholder="Подтвердите пароль"/>
                </div>
            </div>
            <div class="form-action mb-3">
                <x-form.submit class="btn btn-primary btn-rounded btn-login">{{ __('Register') }}</x-form.submit>
            </div>
        </form>
    </x-auth.card>
</x-guest-layout>
