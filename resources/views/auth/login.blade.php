<x-guest-layout>

    <x-auth.card>

        <!-- Session Status -->
        <x-auth.session-status class="mb-4" :status="session('status')"/>

        <h3 class="text-center">{{ __('Sign in to your Account') }}</h3>

        <form class="login-form" method="POST" action="{{ route('login') }}">

        @csrf

        <!-- Email Address -->
            <div class="form-group">
                <x-form.label for="email" :value="__('Email')"/>
                <x-form.input id="email"
                    type="email"
                    name="email"
                    :value="old('email')"
                    autofocus/>
            </div>

            <!-- Password -->
            <div class="form-group">
                <x-form.label for="password" :value="__('Password')"/>

                <a href="{{ route('password.request') }}" class="link float-right">{{ __('Forget Password ?') }}</a>

                <div class="position-relative">
                    <x-form.input id="password"
                        type="password"
                        name="password"
                        autocomplete="current-password"/>
                </div>
            </div>

            <div class="form-group form-action-d-flex mb-3">
                <div class="custom-control custom-checkbox">

                    <x-form.input type="checkbox" class="custom-control-input" for="rememberme"
                        name="rememberme"
                        id="rememberme" checked />

                    <x-form.label for="rememberme" class="custom-control-label">{{ __('Remember Me') }}</x-form.label>

                </div>
                <x-form.submit class="btn-primary">
                    {{ __("Sign in") }}
                </x-form.submit>
            </div>

            <div class="login-account">
                <span class="msg">{{ __("Don't have an account yet ?") }}</span> <a href="{{ route('register') }}" id="show-signup" class="link">{{ __("Sign Up") }}</a>
            </div>

        </form>
    </x-auth.card>
</x-guest-layout>
