<x-guest-layout>

    <x-auth.card>

        <div class="mb-4 text-muted">
            {{ __('Для сброса пароля укажите вашу почту, и мы отправим ссылку на восстановление пароля') }}
        </div>

        <!-- Session Status -->
        <x-auth.session-status class="mb-4" :status="session('status')" />

        <form method="POST" action="{{ route('password.email') }}">

            @csrf

            <!-- Email Address -->
            <div>
                <x-form.label for="email" :value="__('Email')" />

                <x-form.input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="flex items-center justify-end mt-4">
                <x-form.submit class="btn-primary w-100">
                    {{ __('Отправить ссылку на восстановление') }}
                </x-form.submit>
            </div>
        </form>
    </x-auth.card>
</x-guest-layout>
