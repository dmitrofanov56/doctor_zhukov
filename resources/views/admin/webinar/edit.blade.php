<x-admin-layout>
    <div class="page-inner mt--5">
        <div class="d-flex justify-content-center">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('webinar.update', $webinar) }}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <x-form.label>Название:</x-form.label>
                                        <x-form.input name="title" :value="old('title', $webinar->title)"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <x-form.label>Превью изображение:</x-form.label>
                                        <x-form.input name="preview_image" :value="old('preview_image', $webinar->preview_image)"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <x-form.label>Превью видео:</x-form.label>
                                        <x-form.input name="preview_video" :value="old('preview_video', $webinar->preview_video)"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <x-form.label>Время начала:</x-form.label>
                                        <x-form.input name="start_date" :value="old('start_date', $webinar->start_date)"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <x-form.label>Ссылка трансляции:</x-form.label>
                                        <x-form.input name="live_url" :value="old('live_url', $webinar->live_url)"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <x-form.label>Стоимость подписки:</x-form.label>
                                        <x-form.input name="price" :value="old('price', $webinar->price)"/>
                                    </div>
                                </div>
                            </div>


                            <x-form.textarea name="description">{{ $webinar->description }}</x-form.textarea>


                            <x-form.submit class="btn btn-outline-secondary">Обновить</x-form.submit>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</x-admin-layout>
