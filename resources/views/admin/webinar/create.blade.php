<x-admin-layout>
    <div class="page-inner mt--5">
        <div class="d-flex justify-content-center">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('webinar.store') }}" method="POST">
                            @csrf

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <x-form.label>Название:</x-form.label>
                                        <x-form.input name="title" :value="old('title')"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <x-form.label>Превью изображение:</x-form.label>
                                        <x-form.input name="preview_image" :value="old('preview_image')"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <x-form.label>Превью видео:</x-form.label>
                                        <x-form.input name="preview_video" :value="old('preview_video')"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <x-form.label>Время начала:</x-form.label>
                                        <x-form.input name="start_date" :value="old('start_date')"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <x-form.label>Ссылка трансляции:</x-form.label>
                                        <x-form.input name="live_url" :value="old('live_url')"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <x-form.label>Стоимость подписки:</x-form.label>
                                        <x-form.input name="price" :value="old('price')"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <x-form.label>Описание:</x-form.label>
                                <x-form.textarea name="description" class="form-controk" rows="2">{{ old('description') }}</x-form.textarea>
                            </div>

                            <hr>

                            <x-form.submit class="btn btn-outline-secondary">Добавить</x-form.submit>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</x-admin-layout>
