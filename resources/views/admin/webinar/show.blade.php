<x-admin-layout>
    <div class="page-inner mt--5">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ url()->previous()  }}" class="btn btn-outline-secondary">Вернуться назад</a>
                        <hr>
                        <ul class="list-unstyled">
                            <li>Кол-во подписанных: {{ $webinar->orders->count() }} чел. </li>
                            <li>Общая стоимость: {{ $webinar->orders->sum('price') }} руб. </li>
                        </ul>
                        <hr>
                        <table class="table table-striped table-striped-bg-default mt-3">
                            <thead>
                            <tr>
                                <th scope="col">Логин</th>
                                <th scope="col">Почта</th>
                                <th scope="col">Телефон</th>
                                <th scope="col">Дата подписки</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($webinar->orders as $item)
                                <tr>
                                    <td data-label="Логин">{{ $item->order->user->username }}</td>
                                    <td data-label="Почта">{{ $item->order->user->email }}</td>
                                    <td data-label="Телефон">{{ $item->order->user->phone }}</td>
                                    <td data-label="Дата подписки">{{ $item->order->created_at }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">НЕТ ПОДПИСАННЫХ ПОЛЬЗОВАТЕЛЕЙ</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-admin-layout>
