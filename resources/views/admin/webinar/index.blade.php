<x-admin-layout>
    <div class="page-inner mt--5">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <a href="{{ route('webinar.create') }}" class="btn btn-outline-primary">Добавить вебинар</a>
                        </div>
                    </div>
                </div>
                @forelse($webinars as $webinar)
                    <div class="card card-body">
                        <div class="d-flex justify-content-between">
                            <p># {{ $webinar->id }}</p>


                            <div class="d-flex justify-content-end">
                                <p><a class="mr-3" href="{{ route('webinar.show', $webinar) }}"><i class="fal fa-eye"></i></a></p>
                                <p><a href="{{ route('webinar.edit', $webinar) }}"><i class="fal fa-edit"></i></a></p>
                            </div>
                        </div>
                        <p>Название: {{ $webinar->title }}</p>
                        <p>Запланировано на: {{ $webinar->start_date }}</p>
                        <p>Стоимость входа: {{ $webinar->price }} руб. </p>
                        <p class="text-muted">{!! $webinar->description !!}</p>
                        <p>Участники: {{ $webinar->orders->count() }} чел. </p>
                        @if($webinar->start_date > now())
                            <a href="{{ route('webinar.show', $webinar) }}" class="btn btn-success">Оповестить всех ( {{ $webinar->orders->count() }} )</a>
                        @elseif($webinar->status == 1)
                            <button class="btn btn-outline-danger">Вебинар завершён</button>
                        @endif
                    </div>
                @empty
                    <div class="card card-body">
                        <p class="text-center m-0">Нет запланированных вебинаров</p>
                    </div>
                @endforelse
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            let showSubs = $('#showSubs').hide()

            $('.show-hide-sub').click(function () {
                $( "#showSubs" ).toggle( "slow");
            })

        </script>
    @endpush

</x-admin-layout>
