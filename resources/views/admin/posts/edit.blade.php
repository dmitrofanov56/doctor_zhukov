<x-admin-layout>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card card-body">
                    <form action="{{ route('posts.update', $post) }}" method="POST">

                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <x-form.label for="title">Название:</x-form.label>
                            <x-form.input id="title" name="title" :value="$post->title" />
                        </div>

                        <div class="form-group">
                            <x-form.label for="content">Контент:</x-form.label>
                            <x-form.textarea id="content" name="content" rows="3">{{ $post->content }}</x-form.textarea>
                        </div>

                        <div class="form-group">
                            <x-form.label for="views">Просмотры:</x-form.label>
                            <x-form.input id="views" name="views" :value="$post->views" />
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Обновить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-admin-layout>
