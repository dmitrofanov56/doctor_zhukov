<x-admin-layout>
    <div class="page-inner mt--5">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('posts.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <x-form.label for="title">Название:</x-form.label>
                                <x-form.input id="title" name="title" placeholder="Придумайте название поста"/>
                            </div>

                            <div class="form-group">
                                <x-form.label for="content">Контент:</x-form.label>
                                <x-form.textarea id="content" name="content" placeholder="Текст поста" rows="3" />
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Добавить пост</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-admin-layout>
