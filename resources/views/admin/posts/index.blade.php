<x-admin-layout>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <a href="{{ route('posts.create') }}" class="btn btn-outline-primary">Добавить пост</a>
                            <a href="{{ route('admin.posts.all.delete') }}" class="btn btn-outline-danger">Удалить все посты</a>
                        </div>
                    </div>
                </div>
                @forelse($posts as $post)
                    <div class="card-body shadow-lg mb-3">
                        <h3>{{ $post->title }}</h3>
                        <p>{{ $post->content }}</p>
                        @if($post->status)
                            <span class="badge badge-success">Активен</span>
                        @else
                            <span class="badge badge-danger">Не активен</span>
                        @endif
                        <p class="mt-3">Просмотров: {{ $post->views }}</p>

                        <hr>

                        <div class="d-flex justify-content-start mt-3">
                            <a href="{{ route('posts.edit', $post) }}" class="btn btn-outline-primary mr-3"><i class="fal fa-edit"></i> Редактировать</a>
                            <form action="{{ route('posts.destroy', $post) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-outline-danger"><i class="fal fa-trash"></i> Удалить</button>
                            </form>
                        </div>
                    </div>
                @empty
                    <div class="card-body shadow-lg text-center">
                        Нет постов
                    </div>
                @endforelse
            </div>
        </div>
    </div>
</x-admin-layout>
