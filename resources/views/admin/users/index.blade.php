<x-admin-layout>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">

                <div class="card card-body">
                    <p class="mb-0">Пользователей: {{ $users->total() }}</p>
                </div>

                <table class="table table-striped table-striped-bg-default mt-3">
                    <thead>
                    <tr>
                        <th scope="col">Телеграм ID</th>
                        <th scope="col">Подписка</th>
                        <th scope="col">Логин</th>
                        <th scope="col">Почта</th>
                        <th scope="col">Телефон</th>
                        <th scope="col">Сделано заказов</th>
                        <th scope="col">Дата регистрации</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($users as $user)
                        <tr>
                            <td data-label="Телеграм">{{ $user->telegram_id }}</td>
                            <td data-label="Телеграм">{{ $user->subscriptions()->active()->count() }}</td>
                            <td data-label="Логин">{{ $user->username }}</td>
                            <td data-label="Почта">{{ $user->email }}</td>
                            <td data-label="Телефон">{{ $user->phone }}</td>
                            <td data-label="Сделано заказов">{{ $user->orders->count() }}</td>
                            <td data-label="Дата регистрации">{{ $user->created_at }}</td>
                            <td data-label="">
                                <a href="{{ route('users.show', $user) }}"><i class="fal fa-eye"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="3">НЕТ ПОЛЬЗОВАТЕЛЕЙ</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                {{ $users->links() }}
            </div>
        </div>
    </div>
</x-admin-layout>
