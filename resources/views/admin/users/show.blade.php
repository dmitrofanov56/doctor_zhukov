<x-admin-layout>
    <div class="page-inner mt--5">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li>Кол-во товаров: {{ $user->orders->count() }}</li>
                        </ul>
                        <hr>

                        <table class="table table-striped table-striped-bg-default mt-3">
                            <thead>
                            <tr>
                                <th scope="col">Тип продукта</th>
                                <th scope="col">Название</th>
                                <th scope="col">Стоимость</th>
                                <th scope="col">Дата покупки</th>
                                <th scope="col">Статус</th>
                            </tr>
                            </thead>
                            <tbody>

                            @forelse($user->orders as $order)

                                <tr>
                                    <td colspan="5" class="font-weight-bold">{{ $order->order }}</td>
                                </tr>

                                @foreach($order->items as $item)
                                    <tr>

                                        <td data-label="Тип продукта">
                                            @if($item->video)
                                                <p><span class="badge badge-gray">Видео</span></p>
                                            @elseif($item->pdf)
                                                <p><span class="badge badge-gray">PDF</span></p>
                                            @elseif($item->webinar)
                                                <p><span class="badge badge-gray">Вебинар</span></p>
                                            @endif
                                        </td>

                                        <td data-label="Название">
                                            @if($item->video)
                                                <p>{{ $item->video->title }}</p>
                                            @elseif($item->pdf)
                                                <p>{{ $item->pdf->title }}</p>
                                            @elseif($item->webinar)
                                                <p>{{ $item->webinar->title }}</p>
                                            @endif
                                        </td>

                                        <td data-label="Стоимость">{{ $item->price }} руб.</td>

                                        <td data-label="Дата покупки">{{ $item->created_at }}</td>

                                        <td data-label="Статус">
                                            @if($order->status == 1)
                                                <span class="badge badge-success">Оплачен</span>
                                            @elseif($order->status == 0)
                                                <span class="badge badge-danger">Не оплачен</span>
                                            @elseif($order->status == 2)
                                                <span class="badge badge-danger">Отменён</span>
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach

                            @empty
                                <tr>
                                    <td colspan="5">НЕТ ЗАКАЗОВ</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-admin-layout>
