<x-admin-layout>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-7">
                <div class="card card-body">
                    <form action="{{ route('discounts.update', $discount) }}" method="POST">

                        @method('PUT')

                        <div class="form-group">
                            <x-form.label for="code">Код:</x-form.label>
                            <x-form.input id="code" name="code" type="text" :value="old('code', $discount->code)"/>
                        </div>

                        <div class="form-group">
                            <x-form.label for="used_count">Кол-во:</x-form.label>
                            <x-form.input id="used_count" name="used_count" type="number" min="1" :value="old('used_count', $discount->used_count)"/>
                        </div>

                        <div class="form-group">
                            <x-form.label for="percent">Скидка:</x-form.label>
                            <x-form.input id="percent" name="percent" type="number" min="1" :value="old('percent', $discount->percent)"/>
                        </div>

                        <x-form.submit class="btn btn-outline-primary">Обновить</x-form.submit>

                    </form>
                </div>
            </div>
        </div>
    </div>

</x-admin-layout>
