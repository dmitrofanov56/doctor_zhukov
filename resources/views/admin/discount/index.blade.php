<x-admin-layout>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-12">
                <a href="" class="btn btn-outline-primary mb-4">Создать промокод</a>
                <table class="table table-striped table-striped-bg-default mt-3">
                    <thead>
                    <tr>
                        <th scope="col">Код</th>
                        <th scope="col">Использований</th>
                        <th scope="col">Скидка</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($discounts as $discount)
                            <tr>
                                <td data-label="Код">{{ $discount->code }}</td>
                                <td data-label="Использований">{{ $discount->used_count }}</td>
                                <td data-label="Скидка">{{ $discount->percent . '%' }}</td>
                                <td data-label="Действия">
                                    <div class="d-flex justify-content-end">
                                        <a href="{{ route('admin.discount.edit', $discount) }}"><i class="fal fa-edit"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4">На данный момент нет промокодов</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                {{ $discounts->links() }}
            </div>
        </div>
    </div>
</x-admin-layout>
