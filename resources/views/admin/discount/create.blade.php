<x-admin-layout>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-7">
                <div class="card card-body">
                    <form action="{{ route('discounts.store') }}" method="POST">

                        <div class="form-group">
                            <x-form.label for="code">Код:</x-form.label>
                            <x-form.input id="code" name="code" type="text" placeholder="Придумайте произвольный код" :value="old('code')"/>
                        </div>

                        <div class="form-group">
                            <x-form.label for="used_count">Кол-во:</x-form.label>
                            <x-form.input id="used_count" name="used_count" type="number" min="1" :value="old('used_count', 1)"/>
                        </div>

                        <div class="form-group">
                            <x-form.label for="percent">Скидка:</x-form.label>
                            <x-form.input id="percent" name="percent" type="number" min="1" :value="old('percent', 10)"/>
                        </div>

                        <x-form.submit class="btn btn-outline-primary">Добавить</x-form.submit>

                    </form>
                </div>
            </div>
        </div>
    </div>

</x-admin-layout>
