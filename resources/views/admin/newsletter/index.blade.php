<x-admin-layout>
    <div class="page-inner mt--5">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <livewire:admin.news-letter-table />
                        <livewire:admin.news-letter />
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-admin-layout>
