<x-admin-layout>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                @forelse($supports as $support)

                    <div class="card-body shadow-lg mb-3">
                        <p>Номер платежа: {{ $support->order->order }}</p>
                        <p>Дата: {{ $support->order->created_at }}</p>
                        <p>Сумма: {{ $support->sum_pay }} руб. </p>

                        <hr>
                        <p>E-mail: {{ $support->email }}</p>
                        <p>{{ $support->text_comment }}</p>

                        @if($support->order->status == 0)
                            <span class="badge badge-danger">Не оплачен</span>
                        @elseif($support->order->status == 1)
                            <span class="badge badge-success">Оплачен</span>
                        @endif
                    </div>
                @empty
                    <div class="card-body shadow-lg text-center">
                        Не кто не поддерживал проект
                    </div>
                @endforelse
                {{ $supports->links() }}
            </div>
        </div>
    </div>
</x-admin-layout>
