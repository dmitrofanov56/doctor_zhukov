<x-admin-layout>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                @forelse($orders as $order)
                    <div class="card-body shadow-lg mb-3">

                        <p>Номер платежа: {{ $order->order }}</p>
                        <p>Кол-во материалов: {{ $order->items->count() }}</p>

                        <p>Дата: {{ $order->created_at }}</p>

                        <ul>
                            @foreach($order->items as $item)
                                @if($item->video)
                                    <li><p><span class="badge badge-secondary">Видео </span> - {{ $item->video->title }} - <b>{{ $item->price }}</b> руб. </p></li>
                                @elseif($item->pdf)
                                    <li><p><span class="badge badge-primary">PDF </span> - {{ $item->pdf->title }} - <b>{{ $item->price }}</b> руб. </p></li>
                                @elseif($item->webinar)
                                    <li><p><span class="badge badge-info">Вебинар </span> - {{ $item->webinar->title }} - <b>{{ $item->price }}</b> руб. </p></li>
                                @elseif($item->support)
                                    <li><p><span class="badge badge-gray">Поддержка проекта</span></p></li>
                                @elseif($item->telegram)
                                    <li><p><span class="badge badge-info">Доступ к группе телеграм</span></p></li>
                                @endif
                            @endforeach
                        </ul>

                        <p>Стоимость: {{ $order->items->sum('price') }} руб. </p>
                        <p>Пользователь: {{ $order->user->email }}  </p>

                        @if($order->status == 1)
                            <span class="badge badge-success">Оплачен</span>
                        @elseif($order->status == 0)
                            <span class="badge badge-danger">Не оплачен</span>
                        @elseif($order->status == 2)
                            <span class="badge badge-danger">Отменён</span>
                        @endif
                    </div>
                @empty
                    <div class="card-body shadow-lg text-center">
                        Ещё никто не делал покупки
                    </div>
                @endforelse

                {{ $orders->links() }}
            </div>
        </div>
    </div>
</x-admin-layout>
