<x-admin-layout>


    <div class="page-inner mt--5">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <a href="{{ route('videos.create') }}" class="btn btn-outline-primary">Добавить продукт</a>
                            <button class="btn btn-outline-danger">Удалить все продукты</button>
                        </div>
                    </div>
                </div>
                @foreach($videos as $video)
                    <div class="card-body shadow-lg mb-3">
                        <h5 class="card-title mb-3">{{ $video->title }}</h5>
                        <p class="card-text">{{ $video->description ?: 'Нет описания' }}</p>
                        <p class="card-text">{{ $video->videoUrl }}</p>

                        <span>Стоимость: {{ $video->price }} руб. </span>

                        <hr>

                        <div class="d-flex justify-content-start mt-3">
                            <a href="{{ route('videos.edit', $video) }}" class="btn btn-outline-primary mr-3"><i class="fal fa-edit"></i> Редактировать</a>
                            <form action="{{ route('videos.destroy', $video) }}" method="POST">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-outline-danger"><i class="fal fa-trash"></i> Удалить</button>

                            </form>
                        </div>

                    </div>
                @endforeach

            </div>
        </div>
    </div>
</x-admin-layout>
