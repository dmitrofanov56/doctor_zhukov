<x-admin-layout>
    <div class="page-inner mt--5">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('pdf.update', $pdf) }}" method="POST" enctype="multipart/form-data">

                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <x-form.label for="title">Название:</x-form.label>
                                <x-form.input id="title" name="title" :value="$pdf->title" />
                            </div>

                            <div class="form-group">
                                <x-form.label for="description">Описание:</x-form.label>
                                <x-form.textarea id="description" name="description" placeholder="Описание видео если нужно">{{ $pdf->description }}</x-form.textarea>
                            </div>

                            <div class="form-group">
                                <x-form.label for="pdfUrl">Ссылка на PDF:</x-form.label>
                                <x-form.input type="file" id="pdfUrl" name="pdfUrl" />
                                <p class="mt-5">Текущий PDF: <a href="{{ asset('storage/' . $pdf->pdfUrl) }}">просмотр</a></p>
                            </div>

                            <div class="form-group">
                                <x-form.label for="price">Стоимость:</x-form.label>
                                <x-form.input id="price" name="price" placeholder="300" :value="$pdf->price" />
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Редактировать</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-admin-layout>
