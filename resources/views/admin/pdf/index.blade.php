<x-admin-layout>


    <div class="page-inner mt--5">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <a href="{{ route('pdf.create') }}" class="btn btn-outline-primary">Добавить PDF</a>
                            <button class="btn btn-outline-danger">Удалить все PDF</button>
                        </div>
                    </div>
                </div>
                @foreach($pdfs as $pdf)
                    <div class="card-body shadow-lg mb-3">
                        <h5 class="card-title mb-3">{{ $pdf->title }}</h5>
                        <p class="card-text">{{ $pdf->description ?: 'Нет описания' }}</p>
                        <p class="card-text"><a href="{{ asset('storage/' . $pdf->pdfUrl) }}">открыть файл</a></p>

                        <span>Стоимость: {{ $pdf->price }} руб. </span>

                        <hr>

                        <div class="d-flex justify-content-start mt-3">
                            <a href="{{ route('pdf.edit', $pdf) }}" class="btn btn-outline-primary mr-3"><i class="fal fa-edit"></i> Редактировать</a>
                            <form action="{{ route('pdf.destroy', $pdf) }}" method="POST">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-outline-danger"><i class="fal fa-trash"></i> Удалить</button>

                            </form>
                        </div>

                    </div>
                @endforeach

            </div>
        </div>
    </div>
</x-admin-layout>
