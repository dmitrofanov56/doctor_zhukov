<div>
    @if($isMessage)
        <div class="alert alert-{{ $type }}">
            {{ $message }}
        </div>
    @endif
</div>
