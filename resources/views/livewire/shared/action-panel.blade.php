<div>
    <style>
        .btn.btn-default {
            border: 1px solid #ffffff!important;
            color: #ffffff!important;
        }
    </style>
    <div class="panel-header bg-primary-gradient">
        <div class="page-inner py-5">
            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                <div>
                    <h2 class="text-white pb-2 fw-bold">{{ $title }}</h2>
                    <a href="{{ url()->previous() }}" class="btn btn-default btn-border border-white">Вернуться назад</a>
                </div>
            </div>
        </div>
    </div>

</div>
