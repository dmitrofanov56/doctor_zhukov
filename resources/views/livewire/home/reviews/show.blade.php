<div>
    <!-- Star Health Tips
    ============================================= -->
    <div class="health-tips-area bg-gray default-padding">
        <div class="container">
            <div class="health-tips-items">
                <div class="row">
                    <div class="col-lg-5 left-info">
                        <h2>Отзывы</h2>
                        <p>
                            Здесь вы сможете ознакомиться с отзывами, и как обо мне отзываются пациенты
                        </p>

                        @livewire('home.reviews.actions')

                    </div>
                    <div class="col-lg-7 right-info">
                        <div class="tips-items health-tips-carousel owl-carousel owl-theme">
                            <!-- Single Item -->
                            <div class="item">
                                <div class="info">
                                    <h4>Отзыв клиента</h4>
                                    <p>
                                        Сергей Юрьевич - отличный кардиолог! Наблюдаюсь у него с 2013 года. Всегда внимательно выслушает и подробно все объяснит. Никогда не откажет в помощи, всегда на связи, даже в нерабочее время. Доктор грамотно подошёл к решению моей проблемы. Благодаря Сергею Юрьевичу я веду активный образ жизни. Рекомендую этого врача, так как на личном опыте убедилась в его квалификации! (Пациент проходил лечение по адресу ул. Краснодонцев, д. 72а)
                                    </p>
                                </div>
                            </div>
                            <!-- End Single Item -->
                            <!-- Single Item -->
                            <div class="item">
                                <div class="info">
                                    <h4>Отзыв клиента</h4>
                                    <p>
                                        Обратилась к Жукову Сергею Юрьевичу по совету знакомых. Врач внимательно выслушал проблему, изучил все имеющиеся обследования, назначил лечение, грамотно подобрав препараты. Результат - значительное улучшение состояния здоровья, возвращение к обычному образу жизни. Рекомендую данного кардиолога. Огромное спасибо! (Пациент посещал учреждение по адресу ул. Краснодонцев, д. 72а)
                                    </p>
                                </div>
                            </div>
                            <!-- End Single Item -->

                            <!-- Single Item -->
                            <div class="item">
                                <div class="info">
                                    <h4>Отзыв клиента</h4>
                                    <p>
                                        Если хотите попасть именно к профессионалу своего дела, то обязательно обратитесь к Жукову С. Ю. Внимательное отношение, четкие указания, и 100% правильные назначения! (Оставивший отзыв лечился по адресу ул. Краснодонцев, д. 72а)
                                    </p>
                                </div>
                            </div>
                            <!-- End Single Item -->

                            <!-- Single Item -->
                            <div class="item">
                                <div class="info">
                                    <h4>Отзыв клиента</h4>
                                    <p>
                                        Я была у доктора Сергея Юрьевича (Сергей Юрьевич)، Он - замечательный и очень умный врач и, благодаря ему, мое здоровье стало лучше. Он - опытный, дотошный врач и дальновидный، Удобно, что если вам нужно задать им вопрос, он ответит вам даже удаленно، Он обсуждает важные вопросы на своем канале YouTube (...). Консультация с доктором Сергеем Юрьевичем - ключ к здоровью вашего сердца! Клиника очень чистая и оснащена высокими технологиями. Спасибо от всего сердца! (Пациент проходил лечение по адресу ул. Маршала Захарова, д. 20)
                                    </p>
                                </div>
                            </div>
                            <!-- End Single Item -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Health Tips -->

</div>

