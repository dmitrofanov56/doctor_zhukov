<div>
    <div class="d-flex justify-content-center m-3">
        @if($isFormActive)
            @if(! $isAuth)
                <div class="alert alert-danger" role="alert">
                    Для подписки необходимо <a href="{{ route('login') }}">войти</a> в ваш аккаунт, или <a href="{{ route('register') }}">создать</a>
                </div>
            @else
                <form wire:submit.prevent="pay">
                <div class="container">
                    <div class="register-items">
                        <div class="row">
                            <div class="col-lg-6 form w-100">
                                <div class="appoinment-box">
                                    <form action="#">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <input class="form-control" name="amount" wire:model="amount" disabled type="text">
                                                </div>
                                            </div>
                                            <div class="col-lg-12 d-flex justify-content-center">
                                                <button>
                                                    Оплатить <i class="fa fa-paper-plane"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6 info">
                                <p>Закрытый телеграм канал с полезной информацией для тех, кому важно знать больше о своём здоровье</p>
                                <p>Сегодня в течение дня у вас уже будет первые 🔥 материалы, которые закроют многие вопросы про холестерин.</p>
                                <p>А если останутся вопросы, то вы можете задать их все в чате</p>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            @endif
        @else
            <a class="btn btn-md btn-primary" wire:click="$toggle('isFormActive')" href="#"><i class="fab fa-telegram-plane"></i> Купить доступ в закрытый телеграм канал</a>
        @endif
    </div>
</div>
