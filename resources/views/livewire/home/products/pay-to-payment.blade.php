<div class="mt-3">

    @guest
        <div class="form-group">
            <div class="d-flex justify-content-around">
                <div class="form_radio">
                    <input id="radio-1" type="radio" name="typePay" wire:model="typePay" value="1"> <label for="radio-1">У меня есть аккаунт</label>
                </div>

                <div class="form_radio">
                    <input id="radio-2" type="radio" name="typePay" wire:model="typePay" value="0"> <label for="radio-2">У меня нет акакунта</label>
                </div>
            </div>
        </div>
    @endguest

    @if(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            <p class="m-0 text-center">{{ session('error') }}</p>
        </div>
    @elseif($isOrderIsset)
        <div class="alert alert-warning" role="alert">
            <p><b>Внимание!</b></p>
            <p>У вас уже есть неоплаченные заказы, нужно либо оплатить либо отменить в личном кабинете.</p>
            <hr>
            <div class="d-flex justify-content-center mt-3">
                <a href="{{ route('customer.orders') }}" class="btn btn-dark">Личный кабинет</a>
            </div>
        </div>
        @elseif($typePay)
            @guest
                <div class="mb-2">
                    <x-form.label>Почта:</x-form.label>
                    <x-form.input name="account.email" wire:model="account.email" placeholder="Почта"/>
                </div>
                <div>
                    <x-form.label>Пароль:</x-form.label>
                    <x-form.input type="password" name="account.password" wire:model="account.password" placeholder="Пароль"/>
                </div>
                <div class="d-flex justify-content-center mt-2">
                    <button class="btn btn-primary" wire:loading.attr="disabled" wire:click="setAuth">Войти</button>
                </div>
            @elseauth
                <div class="d-flex justify-content-around mt-3">
                    <button wire:click="pay" wire:loading.attr="disabled" class="btn w-50 btn-primary ml-3 {{ $cart_count == 0 ? 'disabled' : '' }}">Оплатить {{ Cart::getTotal()  }} руб.</button>
                </div>
            @endauth
        @elseif($typePay == 0)
        <div class="alert alert-info" role="alert">
            <p><b>Внимание!</b>
                <br> вы на сайте сейчас как гость, после оплаты система вам создаст личный кабинет автоматически, и данные будут отправлены вам на почту, где вы сможете просмотреть весь купленный материал.
            </p>
        </div>
        <div class="mb-2">
            <x-form.label>Почта:</x-form.label>
            <x-form.input name="guest.email" wire:model="guest.email" placeholder="Почта"/>
        </div>
        <div>
            <x-form.label>Номер телефона:</x-form.label>
            <x-form.input name="guest.phone" wire:model="guest.phone" placeholder="Номер телефона"/>
        </div>

        <div class="d-flex justify-content-around mt-3">
            <button wire:click="pay" wire:loading.attr="disabled" class="btn w-50 btn-primary ml-3 {{ $cart_count == 0 ? 'disabled' : '' }}">Оплатить {{ Cart::getTotal()  }} руб.</button>
        </div>

        @endif

</div>

