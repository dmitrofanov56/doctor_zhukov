<div>
    <table>
        <thead>
        <tr>
            <th colspan="3" class="text-center">Видеоматериалы</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($videos as $video)
            <tr>
                <td style="font-size: 15px">
                    <p>
                        {{ $video->title }}
                        @if($video->created_at->diffInDays() < 10)
                            - <span class="badge badge-danger">новый</span>
                        @endif
                    </p>
                </td>
                <td style="font-size: 13px" data-label="Стоимость">
                    {{ $video->price }} руб.
                </td>
                <td class="d-flex justify-content-center">
                    <button wire:click="addToCart('video', {{ $video->id }})" class="{{ Cart::has('video_' . $video->id) ? 'btn btn-outline-danger' : 'btn btn-outline-primary' }}">
                        @if(Cart::has('video_' . $video->id))
                            <i class="fal fa-trash" style="font-size: 30px"></i>
                        @else
                            <i class="fal fa-cart-plus" style="font-size: 30px"></i>
                        @endif
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <table>
        <thead>
        <tr>
            <th colspan="3">PDF материалы</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($pdfs as $pdf)
            <tr>
                <td style="font-size: 15px">
                    <p>
                        {{ $pdf->title }}
                        @if($pdf->created_at->diffInDays() < 10)
                            - <span class="badge badge-danger">новый</span>
                        @endif
                    </p>
                </td>
                <td style="font-size: 13px" data-label="Стоимость">
                    {{ $pdf->price }} руб.
                </td>
                <td class="d-flex justify-content-center">
                    <button wire:click="addToCart('pdf', {{ $pdf->id }})" class="{{ Cart::has('pdf_' . $pdf->id) ? 'btn btn-outline-danger' : 'btn btn-outline-primary' }}">
                        @if(Cart::has('pdf_' . $pdf->id))
                            <i class="fal fa-trash" style="font-size: 30px"></i>
                        @else
                            <i class="fal fa-cart-plus" style="font-size: 30px"></i>
                        @endif
                    </button>
                </td>
            </tr>
            @endforeach
            </tr>
        </tbody>
    </table>
    <div class="mt-2">
        <input type="text" wire:model="discount" placeholder="Промокод" class="pl-1 w-100">
    </div>
</div>
