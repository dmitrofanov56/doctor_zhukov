@push('styles')
    <style>
        .contact-area .contact-items form textarea {
            min-height: 0px
        }
        .contact-area .contact-items form .input-error {
            border-bottom: 2px solid #d51111b3;
        }
    </style>

@endpush
<div class="contact-area">
    <div class="container">
        <div class="contact-items">
            <div class="row">
                <div class="col-lg-12">
                    @if (session()->has('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if(! $isAuth)
                        <div class="alert alert-danger">
                            <p class="text-center m-0">Для того чтобы оставить свою анкету доктору, вам необходимо <a href="{{ route('login') }}">войти</a> в свой акакунт</p>
                        </div>
                    @endif
                    <div class="content">
                        <h2>Заполнить свою <strong>анкету</strong></h2>
                        <p class="text-success">и получите качественный приём как в поликлинике ну только онлайн</p>
                        <form wire:submit.prevent="save" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input class="form-control @error('fio') input-error @enderror" wire:model="fio" placeholder="Ф.И.О" type="text">
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <input class="form-control @error('age') input-error @enderror" wire:model="age" placeholder="Ваш возраст" type="text">
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <span>Пол:</span>
                                        <div class="d-flex justify-content-between">
                                            <div class="form_radio">
                                                <input id="radio-1" type="radio" name="sex" wire:model="sex" value="Мужской"> <label for="radio-1">Мужской</label>
                                            </div>

                                            <div class="form_radio">
                                                <input id="radio-2" type="radio" name="sex" wire:model="sex" value="Женский"> <label for="radio-2">Женский</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="form-group comments">
                                        <textarea class="form-control" wire:model="complaints" placeholder="Жалобы"></textarea>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group comments">
                                        <textarea class="form-control" wire:model="diagnoses" placeholder="Диагнозы (если есть)"></textarea>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group comments">
                                        <textarea class="form-control" wire:model="tablets" placeholder="Таблетки (если принимаете)"></textarea>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group comments">
                                        <textarea class="form-control" wire:model="allergy" placeholder="Аллергия на лекарства (непереносимость)"></textarea>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="exampleFormControlFile1">Анализы крови и обследования (сканы или фото)</label>
                                        <input type="file" wire:model="file" multiple="multiple" class="form-control-file @error('file') input-error @enderror" id="exampleFormControlFile1">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="">
                                            <div class="form_radio">
                                                <input id="period-1" type="radio" wire:model="period" value="3">
                                                <label for="period-1" style="width: 200px">3 месяца (10.000 руб)</label>
                                            </div>

                                            <div class="form_radio">
                                                <input id="period-2" type="radio" wire:model="period" value="6">
                                                <label for="period-2" style="width: 200px">6 месяцев (15.000 руб)</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 d-flex justify-content-center">
                                    <button type="submit" {{ ! $isAuth ? 'disabled' : '' }}>
                                        Отправить
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
