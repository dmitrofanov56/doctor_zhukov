<div>
    <div class="page-inner">
        @forelse($subscriptions as $subscription)
            <div class="card">
                <div class="card-header">
                    материал на каждый месяц
                </div>
                <div class="card-body">
                    <p>Стоимость {{ $subscription->amount }} руб/мес</p>
                    <p>Дата подписки: {{ $subscription->created_at }}</p>
                    <p>Истекает: {{ $subscription->expired_at }}</p>
                    <p>Ссылка на платный канал: <a href="{{ config('medschool.invite_telegram') }}">перейти</a></p>
                    <p>Статус:
                        @if($subscription->status == 'active')
                            Активна
                        @elseif($subscription->status == 'not_active')
                            Подписка остановлена
                        @elseif($subscription->status == 'banned')
                            <span class="text-danger">доступ в канал заблокирован</span>
                        @endif
                    </p>
                    @if($subscription->status == 'active' )
                        <button class="btn btn-danger" wire:click="unsubscribe({{ $subscription }})">описаться</button>
                    @elseif($subscription->status == 'not_active' && $subscription->expired_at->diffInDays() > 0)
                        <button class="btn btn-success" wire:click="reSubscribe({{ $subscription }})">вернуть подписку</button>
                    @endif
                </div>
            </div>
        @empty
            <p class="text-center">Нет подписок</p>
        @endforelse
    </div>
</div>
