<div>
    <div class="page-inner page-inner-fill">
        <div class="conversations">
            <div class="message-header">
                <div class="message-title">
                    <a class="btn btn-light" href="messages.html"> <i class="fa fa-flip-horizontal fa-share"></i> </a>
                    <div class="user ml-2">
                        <div class="avatar avatar-online">
                            <img src="{{ asset('images/robot-chat.png') }}" alt="..." class="avatar-img rounded-circle border border-white">
                        </div>
                        <div class="info-user ml-2">
                            <span class="name">Бот</span> <span class="last-active">Сейчас активен</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="conversations-body">
                <div class="conversations-content bg-white">
                    @foreach($messages->toArray() as $k => $message)

                        <div class="message-content-wrapper">
                            <div class="message message-{{ in_array($message['type'] , ['new_message' , 'reply_action']) ? 'in' : 'out' }}">
                                <div class="avatar avatar-sm">
                                    <img src="{{ $message['avatar'] }}" alt="..." class="avatar-img rounded-circle border border-white">
                                </div>
                                <div class="message-body">
                                    <div class="message-content">
                                        <div class="name">{{ $message['username'] }}</div>
                                        <p class="content">{{ $message['content'] }}</p>

                                        @if(isset($message['materials']))
                                            @foreach($message['materials'] as $material)
                                                <button wire:click="add({{ $material['id'] }}, '{{ $message['type_material']  }}')" class="{{ $message['class'] }}">
                                                    @if(Cart::has($message['type_material'] . '_' . $material['id']))
                                                        <b>(выбрано)</b>
                                                    @endif
                                                    {{ $material['title'] . ' ' . number_format($material['price'] , 0 , ',' , ' ') }} руб
                                                </button>
                                            @endforeach

                                            @if($message['feature']['getTotal'])
                                                <hr>
                                                <p class="text-center">
                                                    Общая стоимость: <b>{{ $total }}</b> руб.
                                                </p>
                                            @endif
                                        @endif

                                        @if($message['role'] == 'user')
                                            <input type="text" class="form-control mb-2" placeholder="Промокод" wire:model.defer="discount">
                                        @endif

                                        <div class="">
                                            @if(count($message['actions']))
                                                <hr>
                                                @foreach($message['actions'] as $k => $action)
                                                    @if($k == 'other')
                                                        {!! $message['actions']['other']  !!}
                                                    @else
                                                        {!! $action !!}
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>

                                    @if($message['feature']['isDate'])
                                        <div class="date">{{ $message['created_at'] }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

</div>
