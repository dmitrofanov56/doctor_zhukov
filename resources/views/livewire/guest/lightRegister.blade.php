<x-guest-layout>
    <div>
        <div>
            <h3 class="text-center">{{ __('Регистрация кабинета') }}</h3>
        </div>
        <hr>
        <form wire:submit.prevent="register">
            <div class="form-group">
                <x-form.label>Почта</x-form.label>
                <x-form.input placeholder="Введите почту" name="email" wire:model="email" />
            </div>
            <div class="form-action mb-3">
                <x-form.submit class="btn btn-primary btn-rounded btn-login">{{ __('Register') }}</x-form.submit>
            </div>
        </form>

    </div>
</x-guest-layout>
