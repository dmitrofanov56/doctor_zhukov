<div style="width: 600px">
    <div class="card card-body">
        <h3 class="text-center">Упрощенная регистрация</h3>
        <form wire:submit.prevent="register">
            <x-form.label class="mb-2">Email:</x-form.label>
            <x-form.input name="email" placeholder="Введите адрес электронной почты" wire:model="email" />

            <x-form.label class="mt-3 mb-2">Телефон:</x-form.label>
            <x-form.input name="phone" id="phone-mask" placeholder="7(900)000-00-00" wire:model="phone" />

            <div class="custom-control custom-checkbox mt-3">
                <input type="checkbox" class="custom-control-input" name="agree" id="agree" checked>
                <label class="custom-control-label" for="agree">Я согласен с условиями и <a href="{{ asset('policy.pdf')  }}">правилами проекта</a></label>
            </div>

            <div class="d-flex justify-content-center mt-2">
                <x-form.submit class="btn-outline-primary">Зарегистрироваться</x-form.submit>
            </div>
        </form>
    </div>

    @push('scripts')
        <script src="https://unpkg.com/imask"></script>

        <script>
            var element     = document.getElementById('phone-mask');
            var maskOptions = {
                mask: '+{7}(000)000-00-00'
            };
            var mask        = IMask(element, maskOptions);
        </script>
    @endpush

</div>

