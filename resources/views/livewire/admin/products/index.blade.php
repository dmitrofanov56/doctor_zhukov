<div>
    <div class="page-inner">

        <a class="btn btn-outline-primary" href="{{ route('admin.product.create') }}">Добавить продукт</a>
        <hr>
        <h2>Видео материалы</h2>
        @foreach($videos as $video)
            <div class="card">
                <div class="card-header">
                    {{ $video->title }}
                </div>
            </div>
        @endforeach
        <h2>ПДФ материалы</h2>
        @foreach($pdfs as $pdf)
            <div class="card">
                <div class="card-header">
                    {{ $pdf->title }}
                </div>
            </div>
        @endforeach
    </div>
</div>
