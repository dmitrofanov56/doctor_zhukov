<div>
    <div class="page-inner mt--5">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <form wire:submit.prevent="addProduct" enctype="multipart/form-data">

                            @csrf

                            <div class="form-group">
                                <x-form.label for="title">Тип продукта:</x-form.label>
                                <select wire:model="type">
                                    <option value="pdf">PDF материал</option>
                                    <option value="video">Видео материал</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <x-form.label for="title">Название:</x-form.label>
                                <x-form.input id="title" name="title" wire:model="product.title"
                                              placeholder="Придумайте название"/>
                            </div>

                            <div class="form-group">
                                <x-form.label for="description">Описание:</x-form.label>
                                <x-form.textarea id="description"
                                                 name="description"
                                                 wire:model="product.description"
                                                 placeholder="Описание"
                                />
                            </div>

                            @if($type == 'pdf')
                                <div class="form-group">
                                    <x-form.label for="pdfUrl">Загрузить файл PDF:</x-form.label>
                                    <x-form.input type="file" id="pdfUrl" name="pdfUrl" wire:model="product.pdfUrl"/>
                                </div>
                            @elseif($type == 'video')
                                <div class="form-group">
                                    <x-form.label for="videoUrl">Ссылка на видео:</x-form.label>
                                    <x-form.input type="text" id="videoUrl" name="videoUrl" wire:model="product.videoUrl"/>
                                </div>
                            @endif

                            <div class="form-group">
                                <x-form.label for="price">Стоимость:</x-form.label>
                                <x-form.input id="price" name="price" wire:model="product.price" placeholder="300" />
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary">Добавить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
