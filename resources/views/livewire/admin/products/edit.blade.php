<div>
    <div class="page-inner mt--5">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('videos.update', $video) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <x-form.label for="title">Название:</x-form.label>
                                <x-form.input id="title" name="title" placeholder="Придумайте название видео" :value="$video->title" />
                            </div>

                            <div class="form-group">
                                <x-form.label for="description">Описание:</x-form.label>
                                <x-form.textarea id="description" name="description" placeholder="Описание видео если нужно">{{ $video->description }}</x-form.textarea>
                            </div>

                            <div class="form-group">
                                <x-form.label for="videoUrl">Ссылка на видео:</x-form.label>
                                <x-form.input id="videoUrl" name="videoUrl" placeholder="Введите ссылку на видео" :value="$video->videoUrl" />
                            </div>

                            <div class="form-group">
                                <x-form.label for="price">Стоимость:</x-form.label>
                                <x-form.input id="price" name="price" placeholder="300" :value="$video->price" />
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Редактировать</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
