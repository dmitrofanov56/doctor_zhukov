<div>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card card-body">

                <div class="d-flex justify-content-between">
                    <p class="mb-0 w-50">Пользователей: {{ $users->total() }}</p>
                    <x-form.input placeholder="Номер или почта клиента" wire:model="search" />
                </div>

                <table class="table table-striped table-striped-bg-default mt-3">
                    <thead>
                    <tr>
                        <th scope="col">TELEGRAM ID</th>
                        <th scope="col">Подписка</th>
                        <th scope="col">USER ID</th>
                        <th scope="col">Логин</th>
                        <th scope="col">Почта</th>
                        <th scope="col">Телефон</th>
                        <th scope="col">Сделано заказов</th>
                        <th scope="col">Дата регистрации</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($users as $userItem)
                        <tr>
                            <td data-label="TELEGRAM ID">{{ $userItem->telegram_id }}</td>
                            <td data-label="Подписка">
                                @if($userItem->subscriptions()->active()->count())
                                    <span class="badge badge-success">есть подписка</span>
                                @elseif(! $userItem->subscriptions()->active()->count())
                                    <span class="badge badge-danger">нет подписки</span>
                                @endif
                            </td>
                            <td data-label="USER ID">{{ $userItem->id }}</td>
                            <td data-label="Логин">{{ $userItem->username }}</td>
                            <td data-label="Почта">{{ $userItem->email }}</td>
                            <td data-label="Телефон">{{ $userItem->phone }}</td>
                            <td data-label="Сделано заказов">{{ $userItem->orders->count() }}</td>
                            <td data-label="Дата регистрации">{{ $userItem->created_at }}</td>
                            <td class="d-flex justify-content-around">
                                <a href="{{ route('admin.user.show', $userItem) }}"><i class="fal fa-eye"></i></a>
                                <a class="text-primary" href="{{ route('admin.user.edit', $userItem) }}"><i class="fab fa-edit"></i></a>

                                @if($userItem->subscriptions()->active()->count())
                                    <a class="text-danger" href="javascript:void(0)" wire:click="unsub('{{ $userItem->id }}')">отключить подписку</a>
                                @else
                                    <a class="text-success" href="javascript:void(0)" wire:click="subOn('{{ $userItem->id }}')">включить подписку на 30 дней</a>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">НЕТ ПОЛЬЗОВАТЕЛЕЙ</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                {{ $users->links() }}
            </div>
        </div>
    </div>
</div>
</div>
