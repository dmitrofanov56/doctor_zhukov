<div>
    <div class="page-inner">
        <form wire:submit.prevent="add" wire:ignore>
            <div class="mr-0 mb-2">
                <x-form.input placeholder="Название темы" name="subject" wire:model="subject"/>
            </div>
            <textarea class="form-control" name="message" wire:ignore wire:model.defer="message"></textarea>
            <x-form.submit>Добавить</x-form.submit>
        </form>
    </div>

    @push('scripts')
        <script src="{{ asset('vendor/plugins/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>

        <script>
            const ckeditor = tinymce.init({
                selector: 'textarea',
                plugins: 'link',
                toolbar: 'link',
                link_context_toolbar: true,
                setup: function (editor) {

                    editor.on('init change', function () {
                        editor.save();
                    });

                    editor.on('input', function (e) {
                        @this.set('message', editor.getContent())
                    });

                }
            });
        </script>
    @endpush
</div>
