<div>
    <div class="page-inner">

        <a class="btn btn-success" href="{{ route('newsletter.create') }}">Создать расслку</a>

        <table class="table table-striped table-striped-bg-default mt-3">
            <thead>
            <tr>
                <th scope="col">Тема письма</th>
                <th scope="col">Текст</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($templates as $template)
                <tr wire:key="{{ $template->id }}">
                    <td data-label="Тема письма">{{ $template->subject }}</td>
                    <td data-label="Письмо">{{ $template->message }}</td>
                    <td data-label="Действия">
                        <div class="d-flex justify-content-center">
                            <button data-toggle="modal" wire:click="edit({{ $template }})" data-target="#edit-{{ $template->id }}" class="btn btn-outline-primary mr-3"><i class="fal fa-edit"></i></button>

                            <button wire:click="send({{ $template }})" wire:loading.class="disabled" class="btn btn-outline-primary mr-3"><i class="fal fa-envelope"></i></button>

                            <button wire:click="delete( {{ $template }})" class="btn btn-outline-danger mr-3"><i class="fal fa-trash"></i></button>

                            @if($template->active)
                                <button wire:click="changeStatus({{ $template }} , 0)" class="btn btn-default mr-3">Выкл</button>
                            @else
                                <button wire:click="changeStatus({{ $template }} , 1)" class="btn btn-success mr-3">Вкл</button>
                            @endif

                            <div wire:ignore.self class="modal fade" id="edit-{{ $template->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <x-form.label>Тема письма</x-form.label>
                                                <x-form.input name="editSubject" wire:model="editSubject"/>
                                            </div>

                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                    <span class="input-group-text"><i
                                                            class="fal fa-money-check-edit"></i></span>
                                                    </div>
                                                    <textarea class="form-control" wire:model="editMessage"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary"
                                                    wire:click="update({{ $template }})" data-dismiss="modal">Сохранить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </td>
                </tr>

            @empty
                <tr>
                    <td colspan="3">НЕТ ШАБЛОНОВ ДЛЯ РАССЫЛКИ</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>

</div>
