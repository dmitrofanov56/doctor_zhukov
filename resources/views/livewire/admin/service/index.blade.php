<div>
    <div class="page-inner">

        <div class="card card-body">
            <button wire:click="$toggle('isNew')" class="btn btn-primary">Добавить услугу</button>
        </div>

        @if($isNew)
            <div class="card">
                <div class="card-header">Добавить услугу</div>
                <div class="card-body">

                    <div class="row">

                        <div class="col-md-4">
                            <x-form.label value="Название" class="mb-1" />
                            <x-form.input wire:model="service.title" placeholder="Введите название услуги" />
                        </div>

                        <div class="col-md-4">
                            <x-form.label value="Тип услуги" class="mb-1" />
                            <select class="form-control" wire:model="service.type">
                                <option value="call">Звонок</option>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <x-form.label value="Стоимость" class="mb-1" />
                            <x-form.input wire:model="service.price" placeholder="Введите стоимость услуги" />
                        </div>

                        <div class="col-md-12 mt-3">
                            <x-form.label value="Описание" class="mb-1" />
                            <x-form.textarea wire:model="service.description" placeholder="Описание минимум 25 символов" />
                        </div>

                        <div class="col-md-12 mt-3">
                            <x-form.submit wire:click="addService" class="btn-primary">Добавить</x-form.submit>
                        </div>

                    </div>
                </div>
            </div>
        @endif

        <hr>
        <div class="row">
            @foreach($services as $service)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">{{ $service->title }}</div>
                        <div class="card-body">
                            {!! $service->description !!}
                        </div>
                        <div class="card-footer">
                            <div class="d-flex">
                                <div class="p-2">
                                    <button class="btn btn-sm btn-dark">
                                        <i class="fal fa-edit"></i>
                                    </button>
                                </div>
                                <div class="p-2">
                                    <button class="btn btn-sm btn-danger" wire:click="deleteService({{ $service }})">
                                        <i class="fal fa-trash"></i>
                                    </button>
                                </div>
                                <div class="ml-auto p-2">
                                    <span class="mb-auto">Стоимость: {{ $service->price }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
