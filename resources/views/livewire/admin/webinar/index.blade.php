<div>

    <div class="page-inner">

        <div class="card card-body">
            <button wire:click="$toggle('isNewWebinar')" class="btn btn-outline-dark">Добавить</button>
        </div>

        @if($isNewWebinar)
            <div class="card card-body">
                <div class="row">
                    <div class="col-md-3">
                        <x-form.label class="mb-2" value="Название" />
                        <x-form.input wire:model="new.title" />
                    </div>

                    <div class="col-md-3">
                        <x-form.label class="mb-2" value="Превью видео" />
                        <x-form.input wire:model="new.preview_video" />
                    </div>

                    <div class="col-md-3">
                        <x-form.label class="mb-2" value="Превью изображение" />
                        <x-form.input wire:model="new.preview_image" />
                    </div>

                    <div class="col-md-3">
                        <x-form.label class="mb-2" value="Ссылка трансляции" />
                        <x-form.input wire:model="new.live_url" />
                    </div>

                    <div class="col-md-12">
                        <x-form.label class="mb-2 mt-4" value="Описание" />
                        <x-form.textarea wire:model="new.description">
                            //
                        </x-form.textarea>
                    </div>

                    <div class="col-md-6">
                        <x-form.label class="mb-2 mt-4" value="Стоимость" />
                        <x-form.input wire:model="new.price" />
                    </div>

                    <div class="col-md-6">
                        <x-form.label class="mb-2 mt-4" value="Дата старта" />
                        <x-form.input wire:model="new.start_date" />
                    </div>

                    <div class="col-md-12 mt-4">
                        <button wire:click="addNew" class="btn btn-outline-dark">Добавить</button>
                    </div>
                </div>
            </div>
        @endif

        <div class="card card-body">
            <span>За месяц заработано на вебинарах: <span>{{ $totalPayMonth }}</span> руб.</span>
            <hr>
            @foreach($payMonth as $webinarItem)
                <p>{{ $webinarItem->webinar->title . ' - ' . $webinarItem->webinar->price . ' руб.' }} | {{ $webinarItem->created_at }} | {{ $webinarItem->order->user->email }}</p>
            @endforeach
        </div>

        @foreach($webinars as $webinar)
            <div class="card card-body">
                <p>Название: <span class="font-weight-bold text-uppercase">{{ $webinar->title }}</span></p>
                <p>Купили раз: <span class="font-weight-bold">{{ $webinar->orders->count() }}</span></p>
                <p>Заработано: <span class="font-weight-bold">{{ $webinar->orders->sum('price') }}</span> руб. </p>
                <p>{{ $webinar->live_url }}</p>
            </div>
        @endforeach
    </div>
</div>
