<div>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-7">
                <div class="card card-body">

                    <form wire:submit.prevent="update">
                        <div class="form-group">
                            <x-form.label for="code">Код:</x-form.label>
                            <x-form.input id="code" name="code" type="text" wire:model="discount.code" />
                        </div>
                        <div class="form-group">
                            <x-form.label for="used_count">Кол-во:</x-form.label>
                            <x-form.input id="used_count" name="used_count" type="number" min="1" wire:model="discount.used_count" />
                        </div>
                        <div class="form-group">
                            <x-form.label for="percent">Скидка:</x-form.label>
                            <x-form.input id="percent" name="percent" type="number" min="1" wire:model="discount.percent" />
                        </div>
                        <x-form.submit class="btn btn-outline-primary">Обновить</x-form.submit>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
