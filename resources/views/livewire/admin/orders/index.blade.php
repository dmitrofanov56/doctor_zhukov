<div>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">

                <div class="card card-body">
                    <div class="form-group">
                        <input type="text" class="form-control" wire:model.debounce.500ms="searchEmail" placeholder="Введите почту для поиска">
                    </div>
                </div>

                @forelse($orders as $order)
                    <div class="card-body shadow-lg mb-3">

                        <p>Номер платежа: {{ $order->order }}</p>
                        <p>Кол-во: {{ $order->items->count() }}</p>

                        <p>Дата: {{ $order->created_at }}</p>

                        <ul>
                            @foreach($order->items as $item)
                                @if($item->video)
                                    <li><p><span class="badge badge-secondary">Видео </span> - {{ $item->video->title }} <b>{{ $item->price }}</b> руб. </p></li>
                                @elseif($item->pdf)
                                    <li><p><span class="badge badge-primary">PDF </span> - {{ $item->pdf->title }} <b>{{ $item->price }}</b> руб. </p></li>
                                @elseif($item->webinar)
                                    <li><p><span class="badge badge-info">Вебинар </span> - {{ $item->webinar->title }} <b>{{ $item->price }}</b> руб. </p></li>
                                @elseif($item->support)
                                    <li><p><span class="badge badge-gray">Поддержка проекта </span> - <b>{{ $item->price }}</b> руб.</p></li>
                                @elseif($item->telegram)
                                    <li><p><span class="badge badge-info">Доступ к группе телеграм</span> - <b>{{ $item->price }}</b> руб.</p></li>
                                @elseif($item->service)
                                    <li><p><span class="badge badge-warning">Услуга</span> - {{ $item->service->title }} <b>{{ $item->price }}</b> руб.</p></li>
                                @endif
                            @endforeach
                        </ul>


                        <p>Общая стоимость: {{ $order->items->sum('price') }} руб. </p>
                        <p>Телефон клиента: <b>{{ $order->user->phone }}</b></p>
                        <p>Почта клиента: {{ $order->user->email }}</p>
                        <p>ID Клиента: {{ $order->user->id }}</p>

                        @if($order->status == 1)
                            <span class="badge badge-success">Оплачен</span>
                        @elseif($order->status == 0)
                            <span class="badge badge-danger">Не оплачен</span>
                        @elseif($order->status == 2)
                            <span class="badge badge-danger">Отменён</span>
                        @endif
                    </div>
                @empty
                    <div class="card-body shadow-lg text-center">
                        Ещё никто не делал покупки
                    </div>
                @endforelse

                {{ $orders->links() }}
            </div>
        </div>
    </div>
</div>
