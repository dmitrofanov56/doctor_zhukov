<div>

    <div class="page-inner">

        <div class="card card-body">
            <div class="d-flex justify-content-between">
                <button class="btn btn-outline-primary" wire:click="setNewPost">Добавить пост</button>
            </div>
        </div>

        <hr>

        @if (session()->has('error'))
            <div class="alert alert-success">
                {{ session('error') }}
            </div>
        @endif

        @if($isEdit)
            <div class="card card-body">
                <div class="form-group form-group-default">
                    <label>Название</label>
                    <input type="text" class="form-control" wire:model="post.title">
                </div>

                <textarea>{{ $post->content }}</textarea>

                <button class="btn btn-outline-info mt-3" id="save">Обновить</button>

            </div>
        @endif

        @if($isNew)

            <div class="card card-body">

                <div class="form-group form-group-default">
                    <label>Название</label>
                    <input type="text" class="form-control" wire:model.defer="newPost.title">
                </div>

                <textarea>Добить описание...</textarea>

                <button class="btn btn-outline-info mt-3" id="newPost">Добавить</button>

            </div>
        @endif

        <div class="row">
            @foreach($posts as $post)

                <div class="col-md-3">
                    <div class="card">
                        <div class="card-header">{{ $post->title }}</div>
                        <div class="card-body">
                            <select class="form-control" wire:model="chat_id.{{ $post->id }}">
                                <option selected> Не выбрано</option>
                                @foreach($chats as $k => $chat)
                                    <option value="{{ $chat['chat_id'] }}">{{ $k }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex">

                                <button class="mr-auto btn btn-dark" wire:click="setEditPost({{ $post }})">
                                    <i class="fad fa-edit"></i>
                                </button>

                                <button class="btn btn-danger" wire:click="deletePost({{ $post }})">
                                    <i class="fad fa-trash"></i>
                                </button>

                                <button class="btn btn-primary ml-2" wire:click="sendPostWithTelegram({{ $post }})">
                                    <i class="fab fa-telegram"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    @push('scripts')
        <script src="{{ asset('vendor/plugins/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>

        <script>

            document.addEventListener("DOMContentLoaded", () => {
                Livewire.hook('element.initialized', (el, component) => {
                    tinymce.init({

                        plugins: 'link',
                        selector: 'textarea',
                        setup: function (editor) {
                            editor.on('init change', function () {
                                editor.save();
                            });

                            $('#save').click(function () {
                               @this.set('post.content', editor.getContent());
                               @this.call('save');
                            })

                            $('#newPost').click(function () {
                                @this.set('newPost.content', editor.getContent());
                                @this.call('newPostSave');
                            })
                        }
                    })
                })
            });
        </script>
    @endpush


</div>
