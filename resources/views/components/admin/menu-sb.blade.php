<!-- Sidebar -->
<div class="sidebar sidebar-style-2">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <ul class="nav nav-primary">
                <li class="nav-section">
                    <h4 class="text-section">Продукция</h4>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.products') }}">
                        <i class="fad fa-shopping-cart"></i>
                        <p>Продукты</p>
                    </a>
                </li>

                <li class="nav-section">
                    <h4 class="text-section">Пользователи</h4>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.users') }}">
                        <i class="fal fa-hospital-user"></i>
                        <p>Пациенты</p>
                    </a>
                </li>

                <li class="nav-section">
                    <h4 class="text-section">Финансы</h4>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.orders') }}">
                        <i class="fal fa-receipt"></i>
                        <p>Оплаты</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.support.finance') }}">
                        <i class="fal fa-piggy-bank"></i>
                        <p>Поддержка проекта</p>
                    </a>
                </li>

                <li class="nav-section">
                    <h4 class="text-section">Управление</h4>
                </li>


                <li class="nav-item">
                    <a href="{{ route('admin.webinars') }}">
                        <i class="fab fa-youtube"></i>
                        <p>Платные вебинары</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.posts.telegram') }}">
                        <i class="fab fa-telegram"></i>
                        <p>Посты для канала</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.services') }}">
                        <i class="fal fa-folders"></i>
                        <p>Услуги</p>
                    </a>
                </li>

                <li class="nav-section">
                    <h4 class="text-section">Скидки</h4>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.discount') }}">
                        <i class="fal fa-percentage"></i>
                        <p>Промокоды</p>
                    </a>
                </li>

                <li class="nav-section">
                    <h4 class="text-section">Системное</h4>
                </li>

                <li class="nav-item">
                    <a href="{{ route('newsletter') }}">
                        <i class="fal fa-envelope-open-text"></i>
                        <p>Рассылки</p>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</div>
<!-- End Sidebar -->
