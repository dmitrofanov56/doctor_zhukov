@if (session()->has('success'))
    <div class="alert alert-success" role="alert">
        <div class="font-medium text-red-600">
            {{ session()->get('success') }}
        </div>
    </div>
@endif
