<div class="row form-sub {{ $class }}">
    <div class="custom-control custom-checkbox">
        <input type="checkbox" name="remember" class="custom-control-input" id="remember">
        <label class="custom-control-label" for="remember">Запомнить меня</label>
    </div>
    <a href="{{ route('password.request') }}" class="link float-right">Забыли пароль ?</a>
</div>
