@props(['status'])

@if ($status)
    <div class="alert alert-success" role="alert" {{ $attributes->merge(['class' => 'font-medium text-sm text-green-600']) }}>
        {{ $status }}
    </div>
@endif
