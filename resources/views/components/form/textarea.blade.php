
<textarea {!! $attributes->class(['form-control' , $errors->has(trim($attributes->wire('model')->value)) ? 'is-invalid' : ''])->merge(['rows' => 5])  !!}>{{ $slot }}</textarea>



@error(trim($attributes->wire('model')->value))
  <p class="text-danger mt-1">{{ $message }}</p>
@enderror
