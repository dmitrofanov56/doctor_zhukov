<label class="selectgroup-item">
    <input type="checkbox" {{ $attributes->merge() }} class="selectgroup-input" {{ isset($current) && $current ? 'checked' : '' }}>
    <span class="selectgroup-button">
        @isset($title)
            {{ $title }}
        @else
            Не указано
        @endisset
    </span>
</label>
