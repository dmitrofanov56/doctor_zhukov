@props(['value'])

<label {{ $attributes->merge(['class' => 'placeholder']) }}>
    <b>{{ $value ?? $slot }}</b>
</label>
