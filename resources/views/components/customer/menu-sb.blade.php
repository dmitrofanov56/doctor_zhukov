<!-- Sidebar -->
<div class="sidebar sidebar-style-2">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <ul class="nav nav-primary">

                <li class="nav-section">
                    <h4 class="text-section">Навигация</h4>
                </li>

                <li class="nav-item">
                    <a href="{{ route('home') }}">
                        <i class="fad fa-home"></i>
                        <p>Перейти на сайт</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('customer') }}">
                        <i class="fal fa-shopping-cart"></i>
                        <p>Оформить покупку</p>
                    </a>
                </li>


                <li class="nav-section">
                    <h4 class="text-section">Материалы</h4>
                </li>
                <li class="nav-item">
                    <a href="{{ route('customer.videos') }}">
                        <i class="fad fa-camera-movie"></i>
                        <p>Мои видео</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('customer.pdfs') }}">
                        <i class="fad fa-print"></i>
                        <p>Мои PDF</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('customer.webinars') }}">
                        <i class="fab fa-youtube"></i>
                        <p>Вебинары</p>
                    </a>
                </li>

                <li class="nav-section">
                    <h4 class="text-section">Финансы</h4>
                </li>

                <li class="nav-item">
                    <a href="{{ route('customer.orders') }}">
                        <i class="fal fa-receipt"></i>
                        <p>Счета</p>
                    </a>
                </li>

                <li class="nav-section">
                    <h4 class="text-section">Доступ</h4>
                </li>

                <li class="nav-item">
                    <a href="{{ route('customer.telegram.access') }}">
                        <i class="fab fa-telegram"></i>
                        <p>Телеграм канал</p>
                    </a>
                </li>

                <li class="nav-section">
                    <h4 class="text-section">Другое</h4>
                </li>

                <li class="nav-item">
                    <a href="{{ route('customer.services') }}">
                        <i class="fal fa-user-md"></i>
                        <p>Платные услуги</p>
                    </a>
                </li>

                <li class="nav-section">
                    <h4 class="text-section">Подписки</h4>
                </li>

                <li class="nav-item">
                    <a href="{{ route('customer.telegram.subscriptions') }}">
                        <i class="fal fa-books"></i>
                        <p>Подписка на материал</p>
                    </a>
                </li>


                @can('is_admin', auth()->user())
                    <li class="nav-section">
                        <h4 class="text-section">Управление</h4>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin') }}">
                            <i class="fal fa-cog"></i>
                            <p>Админка</p>
                        </a>
                    </li>
                @endcan


            </ul>
        </div>
    </div>
</div>
<!-- End Sidebar -->
