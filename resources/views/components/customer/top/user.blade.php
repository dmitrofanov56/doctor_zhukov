<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-uppercase text-white pb-2 fw-bold">{{ auth()->user()->username }}</h2>
                <h5 class="text-white op-7 mb-2">Это ваш профиль</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="#" class="btn btn-white btn-border btn-round mr-2">Пополнить баланс</a>
                <a href="#" class="btn btn-secondary btn-round">Выйти</a>
            </div>
        </div>
    </div>
</div>
