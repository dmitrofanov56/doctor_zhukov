<div class="card card-body">
    <p>Вы можете выбрать подходящий раздел, если вы оплатили материал 100% вы его сразу получите и сможете ознакомиться.</p>
    <div class="card-help-btn d-flex justify-content-between">
        <a href="{{ route('customer.videos') }}" class="btn btn-outline-primary mb-2"><i class="fal fa-film"></i> Мои видеоматериалы</a>
        <a href="{{ route('customer.pdfs') }}" class="btn btn-outline-danger mb-2"><i class="fal fa-file-medical-alt"></i> Мои PDF</a>
        <a href="{{ route('customer.telegram.access') }}" class="btn btn-outline-info mb-2"><i class="fab fa-telegram-plane"></i> Телеграм</a>
        <a href="{{ route('customer.webinars') }}" class="btn btn-outline-secondary mb-2"><i class="fal fa-chalkboard-teacher"></i> Вебинар</a>
    </div>
</div>
