<x-home-layout>
    <div class="breadcrumb-area gradient-bg bg-cover shadow dark text-light text-center" style="background-image: url({{ asset('assets/home/images/bg/questionnaire.jpg') }})">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2">
					<h1>Анкета пациента</h1>
                    <p>Получить рекомендации кардиолога на долгосрочный период по снижению <br>(холестерина, лечению гипертонии, или любого другого заболевания), на долгосрочной основе</p>
				</div>
			</div>
		</div>
	</div>
	<div class="blog-area single full-blog full-blog default-padding">
		<div class="container">
            @livewire('home.questionnaire.store')
		</div>
	</div>
</x-home-layout>
