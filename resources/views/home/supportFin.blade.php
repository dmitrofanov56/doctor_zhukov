<x-home-layout>
    <div class="breadcrumb-area gradient-bg bg-cover shadow dark text-light text-center" style="background-image: url(assets/img/banner/10.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h1>Финансовая поддержка</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="contact-area default-padding">
        <div class="container">
            <div class="contact-items">
                <div class="row">

                    <div class="col-lg-12">
                        <div class="content">
                            <h2 class="text-uppercase">Финансовая поддержка <strong>ШКОЛА ПАЦИЕНТА</strong></h2>
                            <form action="{{ route('finance.support.store') }}" method="POST" class="support-finance-form">
                                @CSRF
                                <input type="hidden" name="user_id" value="{{ auth()->user()  ?  auth()->user()->id : '' }}">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <input class="form-control" id="name" name="name" placeholder="Имя" type="text">
                                            <span class="alert-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <input class="form-control" id="sum_pay" name="sum_pay" placeholder="Любая сумма" type="text">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <input class="form-control" id="email" name="email" placeholder="Почта" type="text">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <input class="form-control" id="phone" name="phone" placeholder="Телефон" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group comments">
                                            <textarea class="form-control" id="text_comment" name="text_comment" placeholder="Текст по желанию"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="submit" name="submit" id="submit">
                                            Поддержать проект <i class="fa fa-paper-plane"></i>
                                        </button>
                                    </div>
                                </div>

                                <div class="row">
                                    <!-- Alert Message -->
                                    <div class="col-lg-12 alert-notification">
                                        <div id="message" class="alert-msg" style="display: none">
                                            <ul class="ajax_error"></ul>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-home-layout>
