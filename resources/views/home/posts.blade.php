<x-home-layout>
    <div class="breadcrumb-area gradient-bg bg-cover shadow dark text-light text-center" style="background-image: url(assets/img/banner/10.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h1>Акции и предложения</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-area single full-blog full-blog default-padding">
        <div class="container">
            <div class="blog-items">
                <div class="row">
                    <div class="blog-content col-lg-10 offset-lg-1 col-md-12">
                        @foreach($posts as $post)
                            <div class="single-item">
                                <div class="blog-item-box">
                                    <div class="item">
                                        <div class="info">
                                            <h1>{{ $post->title }}</h1>
                                            <p>{!! $post->content !!}</p>
                                            <p>Дата добавления: {{ $post->created_at }}</p>
                                            <p>Просмотры: {{ $post->views }}</p>
                                            <!-- <div class="post-pagi-area">
                                                <a href="#"><i class="fas fa-angle-double-left"></i> Previus Post</a>
                                                <a href="#">Next Post <i class="fas fa-angle-double-right"></i></a>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-home-layout>
