<x-home-layout>
    <div class="breadcrumb-area gradient-bg bg-cover shadow dark text-light text-center" style="background-image: url({{ asset('assets/home/images/bg/banner-products.jpg') }});">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h1>Уроки про гипертонию от кардиолога</h1>
                    <p>Вы хотите снизить высокое артериальное давление? но в то же время не хотите себе навредить, вы всегда сможете купить мои авторские методички и видео курсы, и узнать как это можно сделать безопасно и какие лекарства можно принимать.</p>
                </div>
            </div>
        </div>
    </div>


    <div class="default-padding half-bg">
        <div class="container">
            <div class="blog-items">
                <livewire:home.products.card-counter />
                <livewire:home.products.list-table />
                <livewire:home.products.pay-to-payment />
            </div>
        </div>
    </div>
</x-home-layout>
