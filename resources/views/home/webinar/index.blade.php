<x-home-layout>
    <div class="breadcrumb-area gradient-bg bg-cover shadow dark text-light text-center" style="background-image: url(https://st2.depositphotos.com/1158045/8496/i/600/depositphotos_84966424-stock-photo-male-doctor-in-an-hospital.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h1>Вебинары и видео уроки</h1>
                    <p>Вы хотите избавиться от гипертонии или снизить холестерин и не навредить вашему здоровью? в данном разделе вы сможете самостоятельно ознакомиться с платным авторским материалом и узнать как поддерживать ваше здоровье самостоятельно.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Start Our Services
============================================= -->
    <div class="services-area default-padding bg-gray">
        <div class="container">
            <div class="services-box">
                <div class="services-carousels services-one-item-carousel owl-carousel owl-theme">
                    @foreach($webinars as $webinar)
                        <!-- Single Item -->
                            <div class="services-single" id="{{ $webinar->id }}">
                                <div class="row">
                                    <div class="col-lg-5 thumb bg-cover" style="background-image: url({{ $webinar->preview_image }});"></div>
                                    <div class="col-lg-7 info">
                                        <div class="fixed-icon">
                                            <i class="fal fa-camera-movie"></i>
                                        </div>
                                        <div class="title">
                                            <div class="content">
                                                <h4>
                                                    <a href="#">{{ $webinar->title }}</a>
                                                </h4>

                                                <p>

                                                    @if($webinar->isExpire())
                                                        <strong class="text-danger">Вебинар прошёл </strong> <br>
                                                        <small>И теперь доступен для покупки просмотра в записи</small>
                                                    @else
                                                        <strong>Начало: </strong> {{ $webinar->start_date }}
                                                    @endif

                                                </p>
                                            </div>
                                        </div>
                                        <p>
                                            {!! $webinar->description !!}
                                        </p>

                                        <hr>

                                        <div class="custom-list">

                                            @if(! $webinar->isExpire())
                                                <ul>
                                                    <li class="text-uppercase"><i class="fal fa-comments-alt"></i> Чат с доктором</li>
                                                    <li class="text-uppercase"><i class="fal fa-question-circle"></i> Вопросы</li>
                                                    <li class="text-uppercase"><i class="fal fa-percent"></i> Скидки</li>
                                                    <li class="text-uppercase"><i class="fal fa-save"></i> Сохранение вебинара</li>
                                                </ul>
                                            @endif

                                            <div class="d-flex justify-content-center mt-5">

                                                @if($webinar->isExpire())
                                                    <button id="{{ $webinar->id }}" class="btn circle btn-md btn-gradient pay">Купить запись {{ $webinar->price() }} <i class="fal fa-ruble-sign"></i></button>
                                                @else
                                                    <button id="{{ $webinar->id }}" class="btn circle btn-md btn-gradient pay">Купить доступ {{ $webinar->price() }} <i class="fal fa-ruble-sign"></i></button>
                                                @endif
                                            </div>
                                            <div id="message" style="margin-top: 35px"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Item -->

                    @endforeach
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>

            $('.pay').on('click', function () {

                $('#message p').each(function () {
                    $(this).hide();
                });

                let id = $(this).attr('id')
                $.ajax({
                    type: 'POST',
                    url: '/api/webinars/subscribe/' + id,
                    success: function (data) {

                        if (data.noAuth) {
                            $('#message').append("<p class='text-center text-danger'>Вы не авторизованы, для покупки необходимо войти в аккаунт <a href='/login'>авторизоваться</a></p>")
                        }

                        if (typeof data == 'string') {
                            window.location.href = data;
                        }
                    }
                })
            })
        </script>
    @endpush
    <!-- End Our Services -->
</x-home-layout>
