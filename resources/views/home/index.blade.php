<x-home-layout>
    <!-- Start Banner
============================================= -->

    <div class="banner-area single-banner auto-height bg-gray">
        <div class="container">

            <div class="d-flex mt-5">
                <a class="btn btn-sm btn-primary effect" href="https://t.me/Medschool35ru">Перейти в канал телеграм</a>
                <span class="ml-5 mt-3">Хотите быстро <b>получать ответы</b> по состоянию вашего здоровья? подписывайтесь на канал и я буду с вами на связи.</span>
            </div>

            <div class="row align-center">
                <div class="col-lg-7">
                    <div class="content">
                        <h2>Доктор Жуков</h2>
                        <p>Доброго дня, уважаемые друзья и подписчики 👋 </p>
                        <p>Меня зовут <b>Сергей Юрьевич Жуков</b>. Я врач-кардиолог с большим опытом практической работы
                            и управленческой работы.</p>
                        <p>В настоящее время я руковожу частным медицинским центром и веду приём пациентов ежедневно.
                            Мой стаж составляет 19 лет.</p>
                        <a class="btn btn-md btn-light effect" href="{{ route('home.products') }}">Купить</a>
                        <a class="btn btn-md btn-gradient" href="{{ route('customer') }}">Просмотр материалов</a>
                    </div>
                </div>
                <div class="col-lg-5 thumbs">
                    <img src="{{ asset('assets/images/dc.jpeg') }}" alt="Доктор Жуков кардиолог">
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner -->

    <div class="about-area default-padding">
        <div class="container">
            <div class="about-items">
                <div class="row">
                    <div class="col-lg-7 thumb-box">
                        <div class="thumb">
                            <a id="showVideo"><i class="fas fa-play"></i></a> <img
                                src="{{ asset('assets/home/images/center/1.jpeg') }}" alt="Thumb">
                            <img src="{{ asset('assets/home/images/center/2.jpeg') }}" alt="Thumb">
                        </div>
                    </div>
                    <div class="col-lg-5 info">
                        <h5>МедЭксперт</h5>
                        <h2>Медицинский центр</h2>
                        <p>
                            <b>Медэксперт</b> начал свой путь <b>10 лет назад.</b> В настоящее время это крупнейший
                            центр Череповца.
                        </p>

                        <p>В центре установлено современное оборудование МРТ и КТ аппараты Siemens.</p>

                        <p>Единственный в городе аппарат ОКТ с ангиографией. В центре постоянно ведут приемы врачи из
                            (Санкт Петербурга, Вологды, Ярославля) .Отбор пациентов на лечение по федеральным квотам.
                            Официальный партнёр центра Клиника Высоких медицинских технологий имени НИ Павлова</p>
                        <ul>
                            <li>
                                <div class="info">
                                    <h4>Спецификации</h4>
                                    <div class="items">
                                        <span>Кардиология</span> <span>Терапия</span> <span>Нутрициология</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="feature-entry-area default-padding-bottom">
        <!-- Shape -->
        <div class="shape">
            <img src="{{ asset('assets/home/images/shape/1.png') }}" alt="Школа пациента">
        </div>
        <!-- End Shape -->
        <div class="container">
            <div class="feature-entry-items text-center">
                <div class="row">
                    <!-- Single Item -->
                    <div class="col-lg-3 single-item">
                        <div class="item">
                            <i class="fal fa-check"></i>
                            <h4>Вебинары</h4>
                            <p>
                                Проведение онлайн трансляций, познавательные видео уроки для вашего здоровья
                            </p>
                            <a class="btn circle btn-theme border btn-sm"
                               href="{{ route('home.webinars') }}">Перейти</a>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="col-lg-3 single-item">
                        <div class="item">
                            <i class="fal fa-check"></i>
                            <h4>Рекомендации</h4>
                            <p>
                                Консультации от 3 - 6 месяцев, ведение пациента на долгосрочной основе
                            </p>
                            <a class="btn circle btn-theme border btn-sm" href="{{ route('page.questionnaire') }}">Перейти</a>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="col-lg-3 single-item">
                        <div class="item">
                            <i class="fal fa-check"></i>
                            <h4>Материалы</h4>
                            <p>
                                Постоянно новые видео и PDF материалы для доступного обучения, которые хранятся в личном
                                кабинете
                            </p>
                            <a class="btn circle btn-theme border btn-sm"
                               href="{{ route('home.products') }}">Перейти</a>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="col-lg-3 single-item">
                        <div class="item">
                            <i class="fal fa-check"></i>
                            <h4>Консультации</h4>
                            <p>
                                Провожу рекомендации пациента по готовым анализам из лаборатории
                            </p>
                            <a class="btn circle btn-theme border btn-sm" href="{{ route('page.questionnaire') }}">Перейти</a>
                        </div>
                    </div>
                    <!-- End Single Item -->
                </div>
            </div>
        </div>
    </div>

    <!-- Start Our Services
    <div class="services-area bg-gray default-padding-bottom bottom-less pt-5">
        <div class="container">
            <div class="services-items">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="site-heading text-center">
                            <h4>Помощь</h4>
                            <h2>Онлайн консультации</h2>
                            <p class="pt-5">Иметь любой мессенджер (<b>Viber, telegram, What`s App</b>), все рекомендации я напишу туда</p>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="services-single col-lg-6">
                        <div class="item">
                            <div class="info">
                                <div class="top">
                                    <i class="flaticon-call"></i>
                                    <strong>01</strong>
                                </div>
                                <h4>
                                    <a href="#">Первичная консультация</a>
                                </h4>
                                <p>
                                    Осмотр пациента, назначения препаратов
                                </p>
                                <ul>
                                    <li>Стоимость 1500 руб</li>
                                </ul>

                                <a class="btn btn-md circle btn-gradient" href="#">Записаться <i class="fas fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="services-single col-lg-6">
                        <div class="item">
                            <div class="info">
                                <div class="top">
                                    <i class="flaticon-call"></i>
                                    <strong>02</strong>
                                </div>
                                <h4>
                                    <a href="#">Повторная консультация</a>
                                </h4>
                                <p>
                                    Осмотр пациента, после лечения
                                </p>
                                <ul>
                                    <li>Стоимость 1000 руб</li>
                                </ul>

                                <a class="btn btn-md circle btn-gradient" href="#">Записаться <i class="fas fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div> -->


    <div class="services-area three-colums bg-gray default-padding-bottom bottom-less">
        <div class="container">
            <div class="services-items text-center">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="site-heading text-center">
                            <h4>Как поддерживать и укрепить ваше здоровье</h4>
                            <p>вы сможете пройти базовый курс обучения по видео урокам и PDF файлам где доступны и понятны мои рекомендации в примерах, по улучшению общего состояния вашего здоровья</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- Single Item -->
                    <div class="services-single col-lg-4 col-md-6">
                        <div class="item">
                            <div class="info">
                                <div class="top">
                                    <img src="{{ asset('assets/home/images/card/1.jpg') }}"
                                         alt="Как вылечить проблемы сердца">
                                </div>
                                <h4>
                                    <a href="#" class="text-uppercase">Лечение <br> давления</a>
                                </h4>
                                <p>
                                    Вы узнаете как правильно следить за давлением
                                </p>

                                <a class="btn circle btn-theme border btn-sm"
                                   href="{{ route('home.products') }}">Узнать</a>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="services-single col-lg-4 col-md-6">
                        <div class="item">
                            <div class="info">
                                <div class="top">
                                    <img src="{{ asset('assets/home/images/card/2.png') }}"
                                         alt="Как снизить холестерин">
                                </div>
                                <h4>
                                    <a href="#" class="text-uppercase">Снижение <br> холестерина</a>
                                </h4>
                                <p>
                                    Вы узнаете как снизить холестерин в домашних условиях
                                </p>
                                <a class="btn circle btn-theme border btn-sm"
                                   href="{{ route('home.webinars') }}">Узнать</a>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="services-single col-lg-4 col-md-6">
                        <div class="item">
                            <div class="info">
                                <div class="top">
                                    <img src="{{ asset('assets/home/images/card/3.jpg') }}"
                                         alt="Как снизить высокое давление">
                                </div>
                                <h4>
                                    <a href="#" class="text-uppercase">Снижение <br> пульса</a>
                                </h4>
                                <p>
                                    Вы узнаете как правильно следить за своим сердцем
                                </p>
                                <a class="btn circle btn-theme border btn-sm"
                                   href="{{ route('home.products') }}">Узнать</a>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                </div>
            </div>
        </div>
    </div>

<!--    review-->

    <div class="choseus-area default-padding">
        <div class="container">

            <div class="site-heading text-center">
                <h4>ОТВЕТЫ</h4>
                <h2>Повседневные вопросы</h2>
            </div>

            <div class="row">

                <div class="col-lg-12 faqs">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h4 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseOne"
                                    aria-expanded="false" aria-controls="collapseOne">
                                    Какой университет я закончил?
                                </h4>
                            </div>

                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                 data-parent="#accordionExample" style="">
                                <div class="card-body">
                                    <p>
                                        Я закончил <b>Ярославскую</b> Государственную медицинскую академию в <b>2002</b>
                                        году. По специальности
                                        <b>«Лечебное дело»</b>. После этого прошёл интернатуру по специальности
                                        «Терапия» В
                                        <b>2003</b> году прошёл первичную переподготовку по специальности <b>«Клиническая
                                            кардиология»</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h4 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo"
                                    aria-expanded="false" aria-controls="collapseTwo">
                                    Мои действующие сертификаты на 2021 год.
                                </h4>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    <p class="mb-3">Организация Здравоохранения и Общественное здоровье.</p>
                                    <ul class="list-group">
                                        <li><i class="fal fa-check"></i> Кардиология</li>
                                        <li><i class="fal fa-check"></i> Терапия</li>
                                        <li><i class="fal fa-check"></i> Нутрициология</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h4 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree"
                                    aria-expanded="false" aria-controls="collapseThree">
                                    Опыт работы?
                                </h4>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>
                                        Стаж работы 19 лет
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="register-area default-padding half-bg">
        <div class="container">
            <div class="register-items">
                <div class="row align-center">
                    <div class="col-lg-12 info">
                        <livewire:home.products.card-counter/>
                        <livewire:home.products.list-table/>
                        <livewire:home.products.pay-to-payment/>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-home-layout>
