<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! SEO::generate() !!}

    <link rel="stylesheet" href="{{ mix('/assets/cabinet/css/atlantis.css') }}">

    @stack('styles')

    @livewireStyles

</head>

<body class="login">

<div class="wrapper wrapper-login">
    {{ $slot }}
</div>

<script src="{{ mix('/assets/cabinet/js/vendor_atlantis.js') }}" defer></script>
<script src="{{ asset('/assets/cabinet/js/atlantis2.js')}}" defer></script>
@stack('scripts')
@livewireScripts
</body>
</html>
