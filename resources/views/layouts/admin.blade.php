<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! SEO::generate() !!}

    <link rel="stylesheet" href="{{ mix('/assets/cabinet/css/atlantis.css') }}">

    @livewireStyles

    @stack('styles')

</head>

<body>
<div class="wrapper" id="app">

    <x-admin.header/>

    <x-admin.menu-sb/>

    <div class="main-panel">
        <div class="content">
            <livewire:shared.action-panel />
            <livewire:shared.flash-message />
            @yield('content')
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <div class="copyright">
                    developed <i class="fa fa-heart heart text-danger"></i> by <a href="https://t.me/AppEXEdev">PULIDIG</a>
                </div>
            </div>
        </footer>

    </div>
</div>

<script src="{{ mix('/assets/cabinet/js/vendor_atlantis.js') }}"></script>

<script src="{{ asset('/assets/cabinet/js/atlantis2.js')}}"></script>

@livewireScripts

@stack('scripts')

</body>
</html>
