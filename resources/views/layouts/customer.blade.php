<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! SEO::generate() !!}

    <link rel="stylesheet" href="{{ mix('/assets/cabinet/css/atlantis.css') }}">

    @stack('styles')

    @livewireStyles

</head>

<body>
<div class="wrapper" id="app">

    <x-customer.header/>

    <x-customer.menu-sb/>

    <div class="main-panel">
        <div class="content">
            {{ $slot }}
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <div class="copyright">
                    developed <i class="fa fa-heart heart text-danger"></i> by <a href="https://t.me/UNIQDEV">РАЗРАБОТКА САЙТОВ</a>
                </div>
            </div>
        </footer>

    </div>
</div>


<script src="{{ mix('/assets/cabinet/js/vendor_atlantis.js') }}"></script>

<script src="{{ asset('/assets/cabinet/js/atlantis2.js')}}"></script>

@stack('scripts')

@livewireScripts

</body>
</html>
