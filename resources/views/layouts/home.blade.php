<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="yandex-verification" content="a7edc87bc625fd9d" />
    <meta name="mailru-domain" content="abs6eiFHGwToQhMc" />

    {!! SEO::generate() !!}

    <!-- ========== Favicon Icon ========== -->
    <link rel="shortcut icon" href="{{ asset('assets/home/images/favicon.png') }}" type="image/x-icon">

    <!-- ========== Start Stylesheet ========== -->
    <link href="{{ asset('assets/home/css/bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/home/css/font-awesome.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/home/css/themify-icons.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/home/css/flaticon-set.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/home/css/magnific-popup.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/home/css/owl.carousel.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/home/css/owl.theme.default.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/home/css/animate.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/home/css/bootsnav.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/home/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/home/css/responsive.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/home/css/lightbox.min.css') }}" rel="stylesheet"/>
    @livewireStyles
    <!-- ========== End Stylesheet ========== -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// --><!--[if lt IE 9]>
    <script src="assets/js/html5/html5shiv.min.js"></script>
    <script src="assets/js/html5/respond.min.js"></script><![endif]-->

    <!-- ========== Google Fonts ========== -->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@200;300;400;600;700;800&display=swap" rel="stylesheet">

    <script src="//unpkg.com/alpinejs" defer></script>

    @stack('styles')

    @livewireStyles

</head>

<body>

<!-- Preloader Start -->
<div class="se-pre-con"></div>
<!-- Preloader Ends -->

<!-- Start Header Top
============================================= -->
<div class="top-bar-area solid">
    <div class="container">
        <div class="row align-center">
            <div class="col-lg-3 logo">
                <a href="/"> <img src="{{ asset('assets/home/images/logo.png') }}" class="logo" alt="Logo"> </a>
            </div>
            <div class="col-lg-9 address-info text-right">
                <div class="info box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="flaticon-email"></i>
                            </div>
                            <div class="info">
                                <span>Почта</span> rean35@yandex.ru
                            </div>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="flaticon-call"></i>
                            </div>
                            <div class="info">
                                <span>WhatsApp</span> 8(911)829-07-40
                            </div>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="flaticon-clock-1"></i>
                            </div>
                            <div class="info">
                                <span>Время</span> индивидуально
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Header Top -->

<!-- Header
============================================= -->
<header id="home">
    <!-- Start Navigation -->
    <nav class="navbar navbar-default logo-less dark attr-border bootsnav">
        <div class="container">
            <!-- Start Atribute Navigation -->
            <div class="attr-nav">
                <ul>
                    <li>
                        @auth
                            <a class="lk_right text-uppercase text-danger" href="{{ route('customer') }}">Мой кабинет</a>
                        @elseguest
                            <a class="lk_right text-uppercase" href="{{ route('customer') }}">Войти в кабинет</a>
                        @endauth
                    </li>
                </ul>
            </div>
            <!-- End Atribute Navigation -->

            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="/"> <img src="{{ asset('assets/home/images/logo_light.png') }}" class="logo" alt="Logo"> </a>
            </div>
            <!-- End Header Navigation -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp">
                    <li>
                        <a href="{{ route('home') }}">Главная</a>
                    </li>
                    <li>
                        <a href="{{ route('finance.support') }}">ПОДДЕРЖАТЬ ПРОЕКТ</a>
                    </li>
                    <li>
                        <a href="{{ route('home.posts') }}">Блог</a>
                    </li>
                    <li>
                        <a href="{{ route('home.products') }}" title="Обучающие материалы о поддержании здоровья">Материалы</a>
                    </li>
                    <li>
                        <a href="{{ route('home.webinars') }}" title="Вебинары и видео уроки про поддержание здоровья">ВЕБИНАРЫ </a>
                    </li>
                    <li>
                        <a href="{{ route('page.questionnaire') }}" title="Получить рекомендации онлайн">Анкета рекомендаций</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>

    </nav>
    <!-- End Navigation -->

</header>
<!-- End Header -->

{{ $slot }}

<!-- Star Footer
============================================= -->
<footer class="bg-dark text-light">
    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row align-center">
                <div class="col-lg-6">
                    <p>&copy; 2021 <strong>Medschool 35</strong>. все права защищены</p>
                </div>
                <div class="col-lg-6 text-right link">
                    <ul>
                        <li>
                            <a href="https://w1do.ru">Сайт разработан студией w1do.ru </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer-->

<!-- jQuery Frameworks
============================================= -->
<script src="{{ asset('assets/home/js/jquery-1.12.4.min.js') }}"></script>
<script src="{{ asset('assets/home/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/home/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/home/js/equal-height.min.js') }}"></script>
<script src="{{ asset('assets/home/js/jquery.appear.js') }}"></script>
<script src="{{ asset('assets/home/js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('assets/home/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/home/js/modernizr.custom.13711.js') }}"></script>
<script src="{{ asset('assets/home/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/home/js/wow.min.js') }}"></script>
<script src="{{ asset('assets/home/js/progress-bar.min.js') }}"></script>
<script src="{{ asset('assets/home/js/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/home/js/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/home/js/count-to.js') }}"></script>
<script src="{{ asset('assets/home/js/YTPlayer.min.js') }}"></script>
<script src="{{ asset('assets/home/js/circle-progress.js') }}"></script>
<script src="{{ asset('assets/home/js/jquery.nice-select.min.js') }}"></script>
<script src="{{ asset('assets/home/js/bootsnav.js') }}"></script>
<script src="{{ asset('assets/home/js/main.js') }}"></script>
<script src="{{ asset('assets/home/js/lightbox.js') }}"></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();
        for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
        k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(92353199, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/92353199" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    lightbox.option({
        'resizeDuration': 200,
        'wrapAround'    : true
    })

    $('#showVideo').magnificPopup({
        items: {
            src: "{{ asset('assets/home/video.mp4') }}"
        },
        type : 'iframe'
    });

</script>
@stack('scripts')
@livewireScripts

<script src="//code-ya.jivosite.com/widget/FnvkrNFn2a" async></script>

</body>
</html>
