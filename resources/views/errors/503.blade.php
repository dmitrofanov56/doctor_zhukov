@extends('errors::minimal')

@section('title', __('Сайт временно недоступен'))
@section('code', '503')
@section('message', __('Разбираемся с проблемой'))
