@component('mail::message')

Здравствуйте {{ $user->username }}, вас приветствует <b>«ШКОЛА ПАЦИЕНТА»</b>

Напоминаем, что вебинар "{{ $webinar->title }}" ,уже начался и я уверен что у вас получилось на него зайти, не переживайте вы всегда сможете посмотреть запись в своём
личном
кабинете.

<img src="{{ $webinar->preview_image }}" alt="">

@component('mail::button' , ['url' => $webinar->live_url])
    Ссылка на онлайн трансляцию
@endcomponent

@component('mail::button' , ['url' => route('customer')])
    Мои вебинары
@endcomponent

@endcomponent
