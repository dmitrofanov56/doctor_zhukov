@component('mail::message')

Здравствуйте {{ $user->username }}, вас приветствует <b>«ШКОЛА ПАЦИЕНТА»</b>

Напоминаем, что  {{ $webinar->start_date->isoFormat('D MMMM YYYY г в HH:mm') }} будет проходить вебинар на тему {{ $webinar->title }}, мы вам будем напоминать каждый день, что-бы вы, не пропустили данное мероприятие.

<img src="{{ $webinar->preview_image }}" alt="">

@component('mail::button' , ['url' => $webinar->live_url])
    Прямая трансляция через {{ $webinar->start_date->diffInDays() }} дней
@endcomponent

@endcomponent
