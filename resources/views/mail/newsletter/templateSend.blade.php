@component('mail::message')

{!! $newsLetter->message !!}

@component('mail::button' , ['url' => route('home')])
    Перейти на сайт
@endcomponent

@component('mail::button' , [ 'color' => 'success' , 'url' => 'https://t.me/Medschool35ru'])
    Вступить в телеграм канал
@endcomponent

@endcomponent
