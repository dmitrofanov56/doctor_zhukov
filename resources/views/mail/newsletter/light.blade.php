@component('mail::message')

{!! $newsLetter->message  !!}

@component('mail::button' , ['url' => route('home')])
    Перейти на сайт
@endcomponent

@endcomponent
