@component('mail::message')

Здравствуйте {{ $user->username }}, вас приветствует <b>«ШКОЛА ПАЦИЕНТА»</b>

Вы недавно оплатили заказ на сайте, что-бы вам было проще найти материал кликните по кнопке, и вы попадёте сразу в личный кабинет с купленным материалом.

@foreach($order->items as $item)
@if($item->video)
@component('mail::button', ['url' => route('customer.videos') , 'color' => 'primary'])
    Смотреть видео ( {{ $item->video->title }} )
@endcomponent
@endif
@if($item->pdf)
@component('mail::button', ['url' => route('customer.pdfs') , 'color' => 'primary'])
    Смотреть PDF ( {{ $item->pdf->title }} )
@endcomponent
@endif
@if($item->webinar)
@component('mail::button', ['url' => route('customer.webinars') , 'color' => 'primary'])
    Доступ к вебинару ( {{ $item->webinar->title }} )
@endcomponent
@endif
@if($item->telegram)
@component('mail::button', ['url' => route('customer.telegram.access') , 'color' => 'primary'])
    Доступ в телеграм канал
@endcomponent
@endif
@if($item->service)
@component('mail::button', ['url' => route('customer.customer.services') , 'color' => 'primary'])
    Услуга ( {{ $item->service->title }} )
@endcomponent
@endif
@endforeach
@endcomponent

