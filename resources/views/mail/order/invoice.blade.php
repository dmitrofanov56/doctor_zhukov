@component('mail::message')
Здравствуйте {{ $user->username }}, вас приветствует <b>«ШКОЛА ПАЦИЕНТА»</b>

Недавно вы создали заказ на сайте

@component('mail::table')
    | Название       | Стоимость  |
    | :--------- | :------------- |
    @foreach($order->items as $item)
        @if($item->video)
            |Видео - {{ $item->video->title }}| @if($order->discount) <s>{{ $item->video->price }}</s> - <span>{{ $item->price }}</span> @else {{ $item->price }} @endif руб. |
        @endif
        @if($item->pdf)
            |PDF - {{ $item->pdf->title }}| @if($order->discount) <s>{{ $item->pdf->price }}</s> - <span>{{ $item->price }}</span> @else {{ $item->price }} @endif руб. |
        @endif
        @if($item->webinar)
            |Вебинар - {{ $item->webinar->title }}| @if($order->discount) <s>{{ $item->webinar->price }}</s> - <span>{{ $item->price }}</span> @else {{ $item->price }} @endif руб. |
        @endif
        @if($item->support)
            |Поддержка проекта| <span>{{ $item->price }}</span> руб. |
        @endif
        @if($item->telegram)
            |Оплата доступа в телеграм канал| <span>{{ $item->price }}</span> руб. |
        @endif
        @if($item->service)
            |Оплата услуги {{ $item->service->title }}| <span>{{ $item->price }}</span> руб. |
        @endif
    @endforeach
@endcomponent
@component('mail::button', ['url' => route('customer.pay.order', $order) , 'color' => 'primary'])
    Оплатить
@endcomponent
@component('mail::button', ['url' => route('customer.pay.cancel', $order) , 'color' => 'success'])
    Отменить
@endcomponent
    <hr>
    Что-бы отключить данную рассылку и больше не напоминать вам о неоплаченных платежах, нужно сделать отмену всех платежей<br>
    С уважением <b>«ШКОЛА ПАЦИЕНТА»</b> берегите себя, и ваше сердце.
@endcomponent
