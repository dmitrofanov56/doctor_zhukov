@component('mail::message')
Здравствуйте {{ $user->username }}, вас приветствует <b>«ШКОЛА ПАЦИЕНТА»</b>

Вам открыт доступ в платный <b>телеграм канал</b> и бонусом высылаем вам ежемесячный материал<br>

<hr>
@component('mail::button' , ['url' => 'https://t.me/+A8d21lGZkFswOTAy' , 'color' => 'success'])
    Перейти в платный канал
@endcomponent
<hr>

@if($material->type == 'pdf')
@component('mail::button' , ['url' => asset('storage/' . $material->pdfUrl )])
    Скачать ( {{ $material->title }} )
@endcomponent
@elseif($material->type == 'video')
@component('mail::button' , ['url' => $material->videoUrl])
    Смотреть ( {{ $material->title }} )
@endcomponent
@endif
@endcomponent
