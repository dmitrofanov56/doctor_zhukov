<x-customer-layout>
    <div class="page-inner mt--5">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">
                        <p class="text-center text-danger">Платеж не пройден</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-customer-layout>
