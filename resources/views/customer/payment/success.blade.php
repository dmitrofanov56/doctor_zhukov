<x-customer-layout>
    <div class="page-inner mt--5">

        <x-customer.card_help_btn />

        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-body">

                        @if($order->support)
                            <p class="text-center text-success">Спасибо за поддержку проекта, я очень благодарен вам!</p>
                        @else
                            <p class="text-center text-success">Платеж пройден</p>
                        @endif

                         <a class="d-flex justify-content-center" href="{{ route('customer') }}">вернуться на главную кабинета</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-customer-layout>
