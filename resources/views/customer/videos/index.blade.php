<x-customer-layout>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                @forelse($videos as $item)
                    <div class="card-body shadow-lg mb-3">
                        <h5 class="card-title mb-3">{{ $item->video->title }}</h5>
                        <p class="card-text">{{ $item->video->description ?: 'Нет описания' }}</p>

                        <div class="d-flex justify-content-center">
                            <div class="card p-5">
                                @if(! $item->order->status)
                                    <a href="#"><i class="fal fa-lock"></i></a>
                                @else
                                    <a id="showVideo" href="{{ $item->video->videoUrl}}">Смотреть</a>
                                @endif
                            </div>
                        </div>

                        @if(! $item->order->status)
                            <span class="text-danger">Не оплачен</span>
                        @else
                            <span class="text-success">Оплачен </span>
                        @endif
                    </div>
                @empty
                    <div class="card-body shadow-lg mb-3 pb-0">
                        <p class="text-center pb-3"><b>Нет купленных видео</b></p>
                    </div>
                @endforelse
            </div>
        </div>
    </div>

    @push('scripts')
        <script>

            $('#showVideo').magnificPopup({
                type : 'iframe',
            });
        </script>
    @endpush

</x-customer-layout>
