<x-customer-layout>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                @forelse($orderWebinars as $order)
                    <div class="card card-body">
                        <h3>{{ $order->webinar->title }}</h3>
                        <p class="text-muted">{!! $order->webinar->description !!}</p>

                        @if($order->webinar->status)
                            <div class="d-flex justify-content-center">
                                <div class="card p-5">
                                    <a id="showVideo" href="{{ $order->webinar->live_url }}">Смотреть</a>
                                </div>
                            </div>
                        @else
                            <p role="alert" class="alert alert-warning">
                                Дорогие друзья, <b>очень важно!</b> <br> иметь аккаунт на площадке YouTube и подписаться на основной канал Доктора Жукова, где будет проходить трансляция, если это не сделать вы не сможете задать вопрос в прямом эфире.

                                <a href="http://youtube.com/channel/UCLn5GtaFNy6Vgl3kl3b-eog?sub_confirmation=1" class="d-block btn btn-danger mt-3">Подписаться</a>
                            </p>
                        @endif
                        <p>Стоимость: <span>{{ $order->webinar->price }} руб. </span></p>
                    </div>
                @empty
                    <div class="card card-body">
                        <p class="text-center m-0">У вас нет подписки на платный вебинар</p>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
    @push('scripts')
        <script>

		$('#showVideo').magnificPopup({
			type : 'iframe',
		});
        </script>
    @endpush
</x-customer-layout>
