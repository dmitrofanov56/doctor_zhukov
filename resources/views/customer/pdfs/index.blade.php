<x-customer-layout>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">

                @forelse($pdfs as $item)

                    <div class="card-body shadow-lg mb-3">
                        <h5 class="card-title mb-3">{{ $item->pdf->title }}</h5>
                        <p class="card-text">{{ $item->pdf->description ?: 'Нет описания' }}</p>

                        <div class="d-flex justify-content-center">
                            <div class="card p-5">
                                @if(! $item->order->status)
                                    <a href="#"><i class="fal fa-lock"></i></a>
                                @else
                                    <a href="{{ asset('storage/' . $item->pdf->pdfUrl ) }}">Читать</a>
                                @endif
                            </div>
                        </div>

                        <p class="card-text">{{ $item->pdfUrl }}</p>
                        @if(! $item->order->status)
                            <span class="text-danger">Не оплачен</span>
                        @else
                            <span class="text-success">Оплачен </span>
                        @endif
                    </div>
                @empty
                    <div class="card-body shadow-lg mb-3 pb-0">
                        <p class="text-center pb-3"><b>Нет купленных PDF</b></p>
                    </div>
                @endforelse

            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            $('#showVideo').magnificPopup({
                items: {
                    src: "https://disk.yandex.ru/i/nRgUwD-LB1YdqQ"
                },
                type : 'iframe'
            });
        </script>
    @endpush

</x-customer-layout>
