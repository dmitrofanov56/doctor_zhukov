<x-customer-layout>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-5">
                @foreach($myAccess as $tgAccess)
                    <div class="card-body shadow-lg mb-3">
                        <div class="d-flex justify-content-center">
                            <a href="{{ env('INVITE_TELEGRAM') }}" class="btn btn-outline-primary">перейти в телеграм канал по ссылке доступа</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

</x-customer-layout>
