<x-customer-layout>
    <div class="page-inner">

        <div class="alert alert-warning" role="alert">
            <b>Уважаемый пациент!</b> <br> <br>
            После оплаты услуги напишите мне на мой <b>Whats`App</b> который указан в заказе, для голосовой связи.
        </div>

        <div class="d-flex justify-content-center">
            <div class="col-md-6">
                @forelse($orders as $order)
                    <div class="card">
                        <div class="card-header">{{ $order->service->title }}</div>
                        <div class="card-body">
                            {!! $order->service->description !!}
                            <ul class="mt-4">
                                <li>Whats'App: 8(911)829-07-40</li>
                                <li>Номер телефона: 8(911)829-07-4</li>
                                <li>Длительность: 25 мин</li>
                            </ul>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex">
                                <div class="p-2"><span>Стоимость: {{ $order->service->price }} руб. </span></div>
                                <div class="ml-auto p-2">
                                  <span class="badge badge-success">
                                    Оплачен
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="card card-body">
                        <p class="text-center m-0">На данный момент у вас нет не одной купленной услуги.</p>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
</x-customer-layout>
