<x-customer-layout>
    <div class="page-inner">
        <div class="d-flex justify-content-center">
            <div class="col-md-10">
                @forelse($orders as $order)

                    @if($order->items->count())
                        <div class="card-body shadow-lg mb-3">
                            <p># {{ $order->order }}</p>
                            <ul>
                                @foreach($order->items as $item)
                                    @if($item->video)
                                        <li>
                                            <p>
                                                <span class="badge badge-secondary">Видео</span> - <span>{{ $item->video->title }}</span> <br>
                                                @if($order->discount)
                                                    <s>{{ $item->video->price }}</s> - <span>{{ $item->price }}</span>
                                                @else
                                                    {{ $item->price }}
                                                @endif руб.
                                            </p>
                                        </li>
                                    @elseif($item->pdf)
                                        <li>
                                            <p>
                                                <span class="badge badge-primary">PDF</span> - <span>{{ $item->pdf->title }}</span><br>
                                                @if($order->discount)
                                                    <s>{{ $item->pdf->price }}</s> - <span>{{ $item->price }}</span>
                                                @else
                                                    {{ $item->price }}
                                                @endif руб.
                                            </p>
                                        </li>
                                    @elseif($item->webinar)
                                        <li>
                                            <p>
                                                <span class="badge badge-info">Вебинар</span> - <span>{{ $item->webinar->title }}</span> <br>
                                                @if($order->discount)
                                                    <s>{{ $item->webinar->price }}</s> - <span>{{ $item->price }}</span>
                                                @else
                                                    {{ $item->price }}
                                                @endif руб.
                                            </p>
                                        </li>
                                    @elseif($item->support)
                                        <li>
                                            <p><span class="badge badge-gray">Поддержка проекта</span></p>
                                        </li>
                                    @elseif($item->telegram)
                                        <li>
                                            <p><span class="badge badge-info">Доступ к телеграм каналу</span></p>
                                        </li>
                                    @elseif($item->service)
                                        <li>
                                            <p>
                                                <span class="badge badge-warning">Услуга</span> - <span>{{ $item->service->title }}</span> <br>
                                                @if($order->discount)
                                                    <s>{{ $item->service->price }}</s> - <span>{{ $item->price }}</span>
                                                @endif
                                            </p>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>

                            <hr>

                            @if($order->items->count())
                                <p>Стоимость: <b>{{ $order->items->sum('price') }}</b> руб. </p>
                            @endif

                            <hr>

                            @if($order->status == 1)
                                <span class="badge badge-success">оплачен</span>
                            @elseif($order->status == 0)
                                <span class="badge badge-danger">не оплачен</span>
                            @elseif($order->status == 2)
                                <span class="badge badge-danger">отменён</span>
                            @endif

                            @if( ! $order->status)
                                <hr>
                                <a class="btn btn-outline-success" href="{{ route('customer.pay.order', $order) }}">ОПЛАТИТЬ</a>
                                <a class="btn btn-outline-danger" href="{{ route('customer.pay.cancel', $order) }}">ОТМЕНИТЬ</a>
                            @endif

                        </div>
                    @endif
                @empty
                    <div class="card-body shadow-lg mb-3 pb-0">
                        <p class="text-center pb-3"><b>Нет счетов на оплату</b></p>
                    </div>
                @endforelse

                {{ $orders->links() }}

            </div>
        </div>
    </div>

</x-customer-layout>
